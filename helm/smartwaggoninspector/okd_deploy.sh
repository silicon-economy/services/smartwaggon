#!/bin/bash
# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=smartwaggoninspector}"
FULLNAME="smartwaggon-inspector"

# Switch to project if it exists or create a new one
oc project "$NAMESPACE"
# Upgrade or install
helm upgrade --namespace "$NAMESPACE" -i ${FULLNAME} . "$@"
# Ensure image stream picks up the new container image right away
oc -n "$NAMESPACE" import-image "${FULLNAME}-frontend"
oc -n "$NAMESPACE" import-image "${FULLNAME}-backend"
oc -n "$NAMESPACE" import-image "${FULLNAME}-api-gateway"
