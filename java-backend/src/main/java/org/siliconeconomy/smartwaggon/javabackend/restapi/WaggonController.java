/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.restapi;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.siliconeconomy.smartwaggon.javabackend.models.Train;
import org.siliconeconomy.smartwaggon.javabackend.models.Waggon;
import org.siliconeconomy.smartwaggon.javabackend.repository.*;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.TrainDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.WaggonDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.EntityMapper;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

/**
 * This Controller represents version {@value #VERSION} of the REST-api
 * this controller is for Waggon and Train Api calls only
 * @author M.w.Masri
 */
@RestController
@RequestMapping(WaggonController.VERSION)
public class WaggonController {
    public static final String VERSION = "/v1";
    @Autowired
    LoadingUnitRepository loadingUnitRepository;
    @Autowired
    WaggonRepository waggonRepository;
    @Autowired
    TrainRepository trainRepository;
    @Autowired
    EntityMapper entityMapper;
    @Autowired
    Mapper mapper;
    /**
     * @return all the trains  in the database as a json
     */
    @GetMapping("/trains")
    public List<TrainDto> getAllTrains() {
        return trainRepository.findAll().stream().map(mapper::mapTrain).collect(Collectors.toList());
    }

    /**
     * return a specific train by its I'd  as a Json Object
     *
     * @param id the required id of the specified train
     * @return a Response Entity that includes the body of the train as a json object
     * @throws NoSuchElementException when train id is not found in the database
     */    @GetMapping("/train/{id}")
    public ResponseEntity<TrainDto> getTrain(@PathVariable long id) {
        Optional<Train> train = trainRepository.findById(id);
        if (train.isPresent()) {
            return ResponseEntity.ok(mapper.mapTrain(train.get()));
        } else {
            throw new NoSuchElementException("This Train Id Doesn't exist.");
        }
    }
    //  returns all Waggons as a json
    /**
     * @return all Waggons  in the database as a json
     */
    @GetMapping("/waggons")
    public List<WaggonDto> getAllWaggons() {
        return waggonRepository.findAll().stream().map(mapper::mapWaggon).collect(Collectors.toList());
    }
    /**
     * return a specific waggon by its I'd  as a Json Object
     *
     * @param id the required id of the specified waggon
     * @return a Response Entity that included the body of the Waggon as a json object
     * @throws NoSuchElementException when waggon id is not found in the database
     */
    @GetMapping("/waggon/{id}")
    public ResponseEntity<WaggonDto> getTrainWaggon(@PathVariable long id) {
        Optional<Waggon> trainWaggon = waggonRepository.findById(id);
        if (trainWaggon.isPresent()) {
            return ResponseEntity.ok(mapper.mapWaggon(trainWaggon.get()));
        } else {
            throw new NoSuchElementException("This Waggon Id Doesn't exist.");
        }
    }
    /**
     *
     *  Updates or creates a waggon based on the provided waggon number and waggon data.
     *  @param waggonNumber The number of the waggon to update or create.
     *  @param waggonDto The data of the waggon to update or create, provided as a WaggonDto object.
     *  @return A ResponseEntity containing the updated or created waggon data as a JSON object.
     *  @throws URISyntaxException If there is an issue with creating the URI for the created waggon.
     */
    @PutMapping("/saveWaggon/{waggonNumber}")
    public ResponseEntity<WaggonDto> updateOrCreateWaggon(@PathVariable String waggonNumber, @RequestBody WaggonDto waggonDto) throws URISyntaxException {
        var existingWaggon = waggonRepository.findByWaggonNumber(waggonNumber);
        if (existingWaggon != null) {
            var updatedWaggon = entityMapper.mapWaggon(waggonDto);
            updatedWaggon.setId(existingWaggon.getId()); // set the ID of the updated waggon to the existing waggon's ID
            waggonRepository.save(updatedWaggon);
            var updatedWaggonDto = mapper.mapWaggon(updatedWaggon);
            return ResponseEntity.ok().body(updatedWaggonDto);
        } else {
            // The waggon does not exist in the database, create a new one
            var newWaggon = entityMapper.mapWaggon(waggonDto);
            newWaggon.setWaggonNumber(waggonNumber); // set the waggon number of the new waggon to the specified waggon number
            waggonRepository.save(newWaggon);
            var createdWaggonDto = mapper.mapWaggon(newWaggon);
            return ResponseEntity.created(new URI("/v1/waggon/" + createdWaggonDto.getId())).body(createdWaggonDto);
        }
    }
    /**
     *
     * Deletes a waggon by its waggon Number.
     *  @param waggonNumber The number of the waggon that will be deleted.
     *  @return A ResponseEntity indicating the success of the deletion.
     *  @throws EntityNotFoundException If the waggon with the specified waggonNumber  is not found.
     */
    @Transactional
    @DeleteMapping("/deleteWaggon/{waggonNumber}")
    public ResponseEntity<String> deleteWaggon(@PathVariable String waggonNumber) {
        if (waggonNumber == null || waggonNumber.equals("null")) {
            return ResponseEntity.badRequest().body("waggon Number is required.");
        }
        waggonRepository.deleteByWaggonNumber(waggonNumber);
        return ResponseEntity.noContent().build();
    }

    /**
     *  Adds a comment to a waggon.
     *
     *  @param waggonNumber The number(id) of the Waggon to which the comment will be added.
     *  @param comment The  damage comment to be reported.
     *  @return A ResponseEntity containing the updated Waggon as a JSON object.
     *  @throws EntityNotFoundException If the waggon with the specified waggon number is not found.
     */
    @PutMapping("/waggon/addComment/{waggonNumber}")
    public ResponseEntity<WaggonDto> addComment(@PathVariable String waggonNumber, @RequestBody String comment) {
        var existingWaggon = waggonRepository.findByWaggonNumber(waggonNumber);
        if (existingWaggon != null) {
            existingWaggon.setComment(comment);
            waggonRepository.save(existingWaggon);
            var updatedWaggonDto = mapper.mapWaggon(existingWaggon);
            return ResponseEntity.ok().body(updatedWaggonDto);
        } else {
            throw new EntityNotFoundException("This Waggon Doesn't exist.");
        }
    }
    /**
     *  Adds a comment to a waggon.
     *
     *  @param waggonNumber The number(id) of the Waggon to which the comment will be added.
     *  @param damageComment The  damage comment to be reported.
     *  @return A ResponseEntity containing the updated Waggon as a JSON object.
     *  @throws EntityNotFoundException If the waggon with the specified waggon number is not found.
     */
    @PutMapping("/waggon/addDamageComment/{waggonNumber}")
    public ResponseEntity<WaggonDto> addDamageComment(@PathVariable String waggonNumber, @RequestBody String damageComment) {
        var existingWaggon = waggonRepository.findByWaggonNumber(waggonNumber);
        if (existingWaggon != null) {
            existingWaggon.setDamageComment(damageComment);
            waggonRepository.save(existingWaggon);
            var updatedWaggon = mapper.mapWaggon(existingWaggon);
            return ResponseEntity.ok().body(updatedWaggon);
        } else {
            throw new EntityNotFoundException("This Waggon Doesn't exist.");
        }
    }
}
