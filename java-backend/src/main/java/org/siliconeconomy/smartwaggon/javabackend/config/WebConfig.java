/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Objects;

/**
 * Configuration of Spring MVC.
 * <p>
 * Enables CORS for the endpoints provided by the backend and configures the allowed origins.
 *
 * @author M. Grzenia
 */
@Configuration
public class WebConfig
    implements WebMvcConfigurer {

    private final BackendProperties backendProperties;

    /**
     * Creates a new instance.
     *
     * @param backendProperties The backend's configuration properties.
     */
    public WebConfig(BackendProperties backendProperties) {
        this.backendProperties = Objects.requireNonNull(backendProperties, "backendProperties");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
            .allowedOriginPatterns(
                backendProperties.getCorsAllowedOriginPatterns().toArray(new String[0])
            )
            .allowedMethods("*");
    }
}
