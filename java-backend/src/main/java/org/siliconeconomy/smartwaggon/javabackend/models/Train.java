/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.models;

import lombok.*;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

/**
 * holds Information about a Train
 *
 * @author M w.Masri
 */
@Entity
@Getter
@Setter
public class Train {
    @Id
    private long  trainId;
    private String source;
    private  String destination;
    private LocalDate dateDeparture;
    private LocalTime timeDeparture;
    private  LocalDate dateArrival;
    private LocalTime timeArrival;
    private int railId;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "train")
    Set<Waggon> waggons = new HashSet<>();
}
