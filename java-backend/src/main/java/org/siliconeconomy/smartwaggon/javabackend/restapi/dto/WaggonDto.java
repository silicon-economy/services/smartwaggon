/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.restapi.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class WaggonDto {
    private long id;
    @JsonBackReference
    private TrainDto train;
    private int waggonPosition;
    private String waggonNumber;
    private String damageComment;
    private String comment;
    private boolean correctCheckSum;
    private Set<LoadingUnitDto> unitLoads = new HashSet<>();
}
