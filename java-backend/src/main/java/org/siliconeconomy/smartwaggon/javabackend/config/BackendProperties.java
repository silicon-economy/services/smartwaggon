/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.util.List;
import java.util.Objects;

/**
 * Defines backend-relevant (configuration) properties (that are bound via environment variables).
 *
 * @author M. Grzenia
 */
@ConfigurationProperties(prefix = "backend")
@ConstructorBinding
@Getter
public class BackendProperties {

    private final List<String> corsAllowedOriginPatterns;

    /**
     * Creates a new instance.
     *
     * @param corsAllowedOriginPatterns The patterns of origins for which cross-origin requests to
     *                                  the backend's web API are allowed from a browser.
     */
    public BackendProperties(List<String> corsAllowedOriginPatterns) {
        this.corsAllowedOriginPatterns = Objects.requireNonNull(corsAllowedOriginPatterns,
            "corsAllowedOriginPatterns");
    }
}
