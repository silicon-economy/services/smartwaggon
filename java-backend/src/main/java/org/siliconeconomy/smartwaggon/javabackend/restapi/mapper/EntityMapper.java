/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.smartwaggon.javabackend.restapi.mapper;

import org.modelmapper.ModelMapper;
import org.siliconeconomy.smartwaggon.javabackend.models.*;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EntityMapper {
    @Autowired
    ModelMapper modelMapper;

    public Train mapTrain ( TrainDto trainDto){
        return modelMapper.map(trainDto,Train.class);
    }
    public Waggon mapWaggon(WaggonDto waggonDto) {
        return modelMapper.map(waggonDto, Waggon.class);
    }
    public LoadingUnit mapLoadingUnit(LoadingUnitDto loadingUnitDto) {
        return modelMapper.map(loadingUnitDto, LoadingUnit.class);
    }
    public Seal mapSeal(SealDto sealDto){
        return modelMapper.map(sealDto,Seal.class);
    }
    public DangerGoodLabels mapDangerGoodLabel(DangerGoodLabelsDto dangerGoodLabelsDto){
        return modelMapper.map(dangerGoodLabelsDto,DangerGoodLabels.class);
    }
}
