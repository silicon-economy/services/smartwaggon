/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.restapi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SealDto {
     private long sealId;
     private int sealNumber;
     private String sealType;
}
