/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.restapi;

import org.siliconeconomy.smartwaggon.javabackend.models.DangerGoodLabels;
import org.siliconeconomy.smartwaggon.javabackend.models.LoadingUnit;
import org.siliconeconomy.smartwaggon.javabackend.models.Waggon;
import org.siliconeconomy.smartwaggon.javabackend.repository.LoadingUnitRepository;
import org.siliconeconomy.smartwaggon.javabackend.repository.SealRepository;
import org.siliconeconomy.smartwaggon.javabackend.repository.WaggonRepository;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.DangerGoodLabelsDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.LoadingUnitDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.EntityMapper;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This Controller represents version {@value #VERSION} of the REST-api
 * This Controller is for loading Unit endpoints
 * @author M.w.Masri
 */
@RestController
@RequestMapping(LoadingUnitController.VERSION)
public class LoadingUnitController {
    String loadingUnitNumberText="Loading unit with number";
    String notFound= "Not found.";
    public static final String VERSION = "/v1";
    public static final String LOADING_UNIT_URL =VERSION+"loading-unit/";
    @Autowired
    LoadingUnitRepository loadingUnitRepository;
    @Autowired
    WaggonRepository waggonRepository;
    @Autowired
    SealRepository sealRepository;
    @Autowired
    EntityMapper entityMapper;
    @Autowired
    Mapper mapper;

    /**
     * returns all LoadingUnits as a json with their included data such as Danger-good labels
     *
     * @return the Loading units that are saved in the database
     */
    @GetMapping("/loading-units")
    public List<LoadingUnitDto> getAllLoadingUnits() {
        return loadingUnitRepository.findAll().stream().map(mapper::mapLoadingUnit).collect(Collectors.toList());
    }
    /**
     * return a specific LoadingUnit by its id  as a Json Object
     *
     * @param id the required id of the loading-unit
     * @return a loading unit with its data as a json Object
     * @throws NoSuchElementException if the Loading unit id is not found in the database
     */
    @GetMapping("/loading-unit/{id}")
    public ResponseEntity<LoadingUnitDto> getLoadingUnitsById(@PathVariable String id) {
        Optional<LoadingUnit> loadingUnit = loadingUnitRepository.findById(id);
        if (loadingUnit.isPresent()) {
            return ResponseEntity.ok(mapper.mapLoadingUnit(loadingUnit.get()));
        } else {
            throw new NoSuchElementException("This Loading unit  Id Doesn't exist.");
        }
    }
    /**
     *
     * Creates a loading unit and associates it with a specific waggon.
     * @param waggonNumber The number of the waggon to which the loading unit will be added.
     * @param loadingUnitDto The data of the loading unit to be created, provided as a JSON object.
     * @return A ResponseEntity containing the created loading unit data as a JSON object.
     * @throws NoSuchElementException If the waggon with the given waggonNumber is not found in the database.
     * @throws URISyntaxException If there is an issue with creating the URI for the newly created loading unit.
     */
    @PostMapping("/waggon/{waggonNumber}/saveLoading-unit")
    public ResponseEntity<LoadingUnitDto> createLoadingUnit (@PathVariable String waggonNumber,@RequestBody LoadingUnitDto loadingUnitDto) throws URISyntaxException {
        var waggon = waggonRepository.findByWaggonNumber(waggonNumber);
        if (waggon == null) {
        throw new EntityNotFoundException("this Waggon doesn't exist therefore the loading-unit cant be added");
        } else {
            var loadingUnit = entityMapper.mapLoadingUnit(loadingUnitDto);
            loadingUnit.getSeals().stream().forEach(seal -> seal.setLoadingUnit(loadingUnit));
            waggon.getUnitLoads().add(loadingUnit);
            loadingUnit.setWaggon(waggon);
            waggonRepository.save(waggon);
            var createdLoadingUnitDto = mapper.mapLoadingUnit(loadingUnit);
            return ResponseEntity.created(new URI(LOADING_UNIT_URL + URLEncoder.encode(createdLoadingUnitDto.getUnitLoadNumber(), StandardCharsets.UTF_8))).body(createdLoadingUnitDto);
        }
    }
    /**
     * Creates or updates the danger good labels for a specific loading unit.
     * @param loadingUnitNumber The number of the loading unit for which the danger good labels will be created or updated.
     * @param dangerGoodLabelsDtos An array of DangerGoodLabelsDto objects representing the new or updated danger good labels.
     * @return A ResponseEntity containing the updated loading unit data as a JSON object.
     * @throws URISyntaxException If there is an issue with creating the URI for the updated loading unit.
     */
    @PostMapping("loading-unit/{loadingUnitNumber}/saveDangerGoodLabels")
    public ResponseEntity<LoadingUnitDto> createOrUpdateDangerGoodLabels(@PathVariable String loadingUnitNumber, @RequestBody DangerGoodLabelsDto[] dangerGoodLabelsDtos) throws URISyntaxException {
        validateInputs(loadingUnitNumber, dangerGoodLabelsDtos);

        var loadingUnit = getLoadingUnitByUnitLoadNumber(loadingUnitNumber);
        Set<DangerGoodLabels> updatedLabels = updateDangerGoodLabels(loadingUnit, dangerGoodLabelsDtos);

        loadingUnit.setDangerGoodLabels(updatedLabels);
        saveLoadingUnit(loadingUnit);

        var createdLoadingUnitDto = mapper.mapLoadingUnit(loadingUnit);
        return ResponseEntity.created(new URI(LOADING_UNIT_URL + URLEncoder.encode(createdLoadingUnitDto.getUnitLoadNumber(), StandardCharsets.UTF_8))).body(createdLoadingUnitDto);
    }
    /**
     * this method is only used in the createOrUpdateDangerGoodLabels
     * its created to reduce a big method complexity
     *
     * @param loadingUnit the incoming L u
     */
    private void saveLoadingUnit(LoadingUnit loadingUnit) {
        waggonRepository.save(loadingUnit.getWaggon());
    }

    /**
     *
     Validates the inputs for creating or updating danger good labels.
     @param loadingUnitNumber The number of the loading unit for which the danger good labels will be created or updated.
     @param dangerGoodLabelsDtos An array of DangerGoodLabelsDto objects representing the new or updated danger good labels.
     @throws IllegalArgumentException If the loadingUnitNumber or dangerGoodLabelsDtos is null.
     */
    private void validateInputs(String loadingUnitNumber, DangerGoodLabelsDto[] dangerGoodLabelsDtos) {
        if (loadingUnitNumber == null || dangerGoodLabelsDtos == null) {
            throw new IllegalArgumentException("loadingUnitNumber and dangerGoodLabelsDtos must be provided.");
        }
    }
    /**
     *
     Retrieves the loading unit by its unit load number.
     @param loadingUnitNumber The number of the loading unit to retrieve.
     @return The LoadingUnit object corresponding to the provided unit load number.
     @throws EntityNotFoundException If no loading unit with the specified unit load number is found.
     */
    private LoadingUnit getLoadingUnitByUnitLoadNumber(String loadingUnitNumber) {
        var loadingUnit = loadingUnitRepository.findLoadingUnitByUnitLoadNumber(loadingUnitNumber);
        if (loadingUnit == null) {
            throw new EntityNotFoundException(loadingUnitNumberText + loadingUnitNumber + notFound);
        }
        return loadingUnit;
    }
    /**
     *
     * Updates the danger good labels for a loading unit based on the provided DangerGoodLabelsDto objects.
     * @param loadingUnit The loading unit for which the danger good labels will be updated.
     * @param dangerGoodLabelsDtos An array of DangerGoodLabelsDto objects representing the new or updated danger good labels.
     * @return A Set of DangerGoodLabels objects representing the updated danger good labels.
     */
    private Set<DangerGoodLabels> updateDangerGoodLabels(LoadingUnit loadingUnit, DangerGoodLabelsDto[] dangerGoodLabelsDtos) {
        Set<DangerGoodLabels> dangerGoodLabels = new HashSet<>();

        if (loadingUnit.getDangerGoodLabels().isEmpty()) {
            for (DangerGoodLabelsDto dangerGoodLabelsDto : dangerGoodLabelsDtos) {
                DangerGoodLabels dangerGoodLabel = entityMapper.mapDangerGoodLabel(dangerGoodLabelsDto);
                dangerGoodLabel.setLoadingUnit(loadingUnit);
                dangerGoodLabels.add(dangerGoodLabel);
            }
        } else {
            Set<DangerGoodLabels> existingLabels = loadingUnit.getDangerGoodLabels();
            for (DangerGoodLabels existingLabel : existingLabels) {
                for (DangerGoodLabelsDto dangerGoodLabelsDto : dangerGoodLabelsDtos) {
                    if (existingLabel.getPosition() == dangerGoodLabelsDto.getPosition() && (existingLabel.getStatusNumber() != dangerGoodLabelsDto.getStatusNumber())) {
                        existingLabel.setStatusNumber(dangerGoodLabelsDto.getStatusNumber());
                        dangerGoodLabels.add(existingLabel);
                    }
                }
            }
            if (dangerGoodLabels.isEmpty()) {
                dangerGoodLabels = existingLabels;
            }
        }
        return dangerGoodLabels;
    }
    /**
     *
     * Deletes a loading unit by its unit load number from a specific waggon.
     *  @param waggonNumber The number of the waggon from which the loading unit will be deleted.
     *  @param loadingUnitNumber The number of the loading unit to be deleted.
     *  @return A ResponseEntity indicating the success of the deletion.
     *  @throws EntityNotFoundException If the waggon with the specified waggonNumber or the loading unit with the specified loadingUnitNumber is not found.
     */
    @Transactional
    @DeleteMapping("/deleteLoading-unit/waggon/{waggonNumber}/loading-unit/{loadingUnitNumber}")
    public ResponseEntity<Waggon> deleteLoadingUnit(@PathVariable String waggonNumber, @PathVariable String loadingUnitNumber) {
        var waggon = waggonRepository.findByWaggonNumber(waggonNumber);
        if (waggon == null) {
            throw new EntityNotFoundException(loadingUnitNumber + waggonNumber + notFound);
        }

        Set<LoadingUnit> loadingUnits = waggon.getUnitLoads();
        boolean isRemoved = loadingUnits.removeIf(lu -> lu.getUnitLoadNumber().equals(loadingUnitNumber));

        if (!isRemoved) {
            throw new EntityNotFoundException( loadingUnitNumberText + loadingUnitNumber + " not found in the waggon " + waggonNumber + ".");
        }

        loadingUnitRepository.deleteById(loadingUnitNumber);
        waggonRepository.save(waggon);

        return ResponseEntity.noContent().build();
    }

    /**
     *
     *  Adds a comment to a loading unit identified by its unit load number.
     *  @param loadingUnitNumber The number(id) of the loading unit to which the comment will be added.
     *  @param comment The comment to be added.
     *  @return A ResponseEntity containing the updated loading unit data as a JSON object.
     * @throws EntityNotFoundException If the loading unit with the specified unit load number is not found.
     */
    @PutMapping("/loading-unit/addComment/{loadingUnitNumber}")
    public ResponseEntity<LoadingUnitDto> addComment(@PathVariable String loadingUnitNumber, @RequestBody String comment) {
        var existingLoadingUnit = loadingUnitRepository.findLoadingUnitByUnitLoadNumber(loadingUnitNumber);
        if (existingLoadingUnit == null) {
            throw new EntityNotFoundException(loadingUnitNumberText + loadingUnitNumber + notFound);
        }

        existingLoadingUnit.setComment(comment);
        loadingUnitRepository.save(existingLoadingUnit);

        var updatedLoadingUnitDto = mapper.mapLoadingUnit(existingLoadingUnit);
        return ResponseEntity.ok().body(updatedLoadingUnitDto);
    }
    /**
     *
     *  Adds a comment to a loading unit identified by its unit load number.
     *  @param loadingUnitNumber The number(id) of the loading unit to which the comment will be added.
     *  @param damageComment The  damage comment to be reported.
     *  @return A ResponseEntity containing the updated loading unit data as a JSON object.
     *  @throws EntityNotFoundException If the loading unit with the specified unit load number is not found.
     */
    @PutMapping("/loading-unit/addDamageComment/{loadingUnitNumber}")
    public ResponseEntity<LoadingUnitDto> addDamageComment(@PathVariable String loadingUnitNumber, @RequestBody String damageComment) {
        var existLoadingUnit = loadingUnitRepository.findLoadingUnitByUnitLoadNumber(loadingUnitNumber);
        if (existLoadingUnit != null) {
            existLoadingUnit.setDamageComment(damageComment);
            loadingUnitRepository.save(existLoadingUnit);
            var updateLoadingUnitDto = mapper.mapLoadingUnit(existLoadingUnit);
            return ResponseEntity.ok().body(updateLoadingUnitDto);
        } else {
            throw new EntityNotFoundException(loadingUnitNumberText + loadingUnitNumber + notFound);
        }
    }
}
