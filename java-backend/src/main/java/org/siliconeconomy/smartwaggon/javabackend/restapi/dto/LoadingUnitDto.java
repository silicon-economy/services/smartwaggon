/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.restapi.dto;


import lombok.Getter;
import lombok.Setter;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class LoadingUnitDto {
    private String unitLoadNumber;
    private String comment;
    private String damageComment;
    private boolean correctCheckSum;
    private  long waggonDto;
    private Set<SealDto> seals = new HashSet<>();
    private Set<DangerGoodLabelsDto> dangerGoodLabels = new HashSet<>();
}
