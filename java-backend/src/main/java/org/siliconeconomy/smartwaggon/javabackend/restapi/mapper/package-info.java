
/**
 * This package contains mapping functionality that reads or writes DTOs
 * Mapper class maps the entity data to Dto data
 * EntityMapper class maps Dto data to entity data
 *
 * @author M w.Masri
 */
package org.siliconeconomy.smartwaggon.javabackend.restapi.mapper;
