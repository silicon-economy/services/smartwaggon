
/**
 * Base package for the backend.
 * <p>
 * This package-info.java contains annotations that configure global null-handling
 *
 * @author S. Jankowski
 */
@NonNullApi
@NonNullFields
package org.siliconeconomy.smartwaggon.javabackend;

import org.springframework.lang.NonNullApi;
import org.springframework.lang.NonNullFields;
