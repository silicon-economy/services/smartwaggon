/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend;

import org.siliconeconomy.smartwaggon.javabackend.config.BackendProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * Contains the main method and thus the entry point of the java-backend application.
 *
 * @author R. Quensel
 * @version 1.0
 */
@SpringBootApplication
@EnableConfigurationProperties({BackendProperties.class})
public class JavaBackend {

    public static void main(final String[] args) {
        SpringApplication.run(JavaBackend.class, args);
    }

}
