/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.restapi.mapper;

import org.modelmapper.ModelMapper;
import org.siliconeconomy.smartwaggon.javabackend.models.*;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class Mapper {
    @Autowired
    ModelMapper modelMapper;
    public TrainDto mapTrain(Train train) {
        return  modelMapper.map(train, TrainDto.class);
    }

    public WaggonDto mapWaggon(Waggon waggon) {
        var waggonDto=  modelMapper.map(waggon, WaggonDto.class);

        for (LoadingUnitDto lu :waggonDto.getUnitLoads()){
            lu.setWaggonDto(waggon.getId());
        }
        return waggonDto;
    }
    public LoadingUnitDto mapLoadingUnit(LoadingUnit loadingUnit) {
        return modelMapper.map(loadingUnit, LoadingUnitDto.class);
    }
}
