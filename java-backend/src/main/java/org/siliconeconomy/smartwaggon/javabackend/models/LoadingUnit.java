/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * holds Information about a loading unit
 *
 * @author M w.Masri
 */
@Entity
@Getter
@Setter
public class LoadingUnit {
    @Id
    private  String unitLoadNumber;
    private String comment;
    private String damageComment;
    private boolean correctCheckSum;
    @ManyToOne
    @JoinColumn(name = "waggon")
    private Waggon waggon;
    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "loadingUnit")
    @Size(max = 4)
    private Set<DangerGoodLabels> dangerGoodLabels= new HashSet<>();
    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "loadingUnit")
    private Set<Seal> seals= new HashSet<>();
}
