/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.repository;

import org.siliconeconomy.smartwaggon.javabackend.models.Waggon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for Waggons of a train with its connection to the Train and the loadingUnits
 *
 * @author M w.Masri
 */
@Repository
public interface WaggonRepository extends JpaRepository<Waggon,Long> {

    void deleteByWaggonNumber(String waggonNumber);
    Waggon findByWaggonNumber(String waggonNumber);
}
