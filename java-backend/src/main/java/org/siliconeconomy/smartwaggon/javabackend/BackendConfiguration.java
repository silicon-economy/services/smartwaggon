/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Base configuration class
 * <p>
 * Global Spring configuration annotations and global constants should be here,
 * specialized annotations and constants in their respective classes.
 *
 * @author S. Jankowski
 */
@Configuration
@ComponentScan
@ConfigurationPropertiesScan
@EnableJpaRepositories
@EntityScan
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class BackendConfiguration {

 @Bean
    public ModelMapper modelMapper(){
            return new ModelMapper();
    }

}
