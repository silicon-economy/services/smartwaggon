/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.restapi.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class TrainDto {
     long  trainId;
     String source;
     String destination;
     LocalDate dateDeparture;
     LocalTime timeDeparture;
     LocalDate dateArrival;
     LocalTime timeArrival;
     int railId;
    @JsonManagedReference
    Set<WaggonDto> waggons = new HashSet<>();
}
