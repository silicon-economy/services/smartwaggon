/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * holds Information about the waggons with all their data
 *
 * @author M w.Masri
 */
@Entity
@Getter
@Setter
public class Waggon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "train_id")
    private Train  train;
    private  int waggonPosition;
    private String waggonNumber;
    private String comment;
    private  String damageComment;
    private boolean correctCheckSum;


    @OneToMany(cascade = CascadeType.ALL,mappedBy = "waggon")
    @Size(max = 4)
    Set<LoadingUnit> unitLoads = new HashSet<>();
}
