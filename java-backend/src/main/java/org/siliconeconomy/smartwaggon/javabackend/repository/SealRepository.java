/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.repository;

import org.siliconeconomy.smartwaggon.javabackend.models.Seal;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * Repository for Loading unit Seal
 *
 * @author M w.Masri
 */
public interface SealRepository extends JpaRepository<Seal,Long> {
}
