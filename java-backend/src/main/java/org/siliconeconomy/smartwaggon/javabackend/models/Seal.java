/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.models;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * holds Information about Loading unit Seals
 *
 * @author M w.Masri
 */
@Entity
@Setter
@Getter
public class Seal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long  sealId;
    private  int sealNumber;
    private String sealType;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "loading_unit_number")
    private LoadingUnit loadingUnit;
}
