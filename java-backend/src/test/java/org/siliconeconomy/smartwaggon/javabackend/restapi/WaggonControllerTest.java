/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.restapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.smartwaggon.javabackend.models.*;
import org.siliconeconomy.smartwaggon.javabackend.repository.*;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.WaggonDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.EntityMapper;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(value ={WaggonController.class}, excludeAutoConfiguration = SecurityAutoConfiguration.class)
class WaggonControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    LoadingUnitRepository loadingUnitRepository;
    @MockBean
    WaggonRepository waggonRepository;
    @MockBean
    TrainRepository trainRepository;
    @MockBean
    SealRepository sealRepository;
    @MockBean
    DangerGoodLabelRepository dangerGoodLabelRepository;
    @MockBean
    Mapper mapper;
    @MockBean
    EntityMapper entityMapper;
    @Autowired
    ObjectMapper objectMapper;


    @BeforeEach
    void setUp(){
        MapperTestConfiguration mapperTestConfiguration = new MapperTestConfiguration();
        mapperTestConfiguration.configureWaggonMapper(mapper);
        mapperTestConfiguration.configureLoadingUnitMapper(mapper);
        mapperTestConfiguration.configureTrainMapper(mapper);
        mapperTestConfiguration.configureEntityTrainWaggonMapper(entityMapper);
        mapperTestConfiguration.configureEntityTrainMapper(entityMapper);
        mapperTestConfiguration.configureEntityLoadingUnitMapper(entityMapper);
    }
    // init methods are just for initializing and preparing objects for tests
    public Train initTrain() {
        Train train = new Train();
        train.setTrainId(1L);
        train.setSource("TestSource");
        train.setDestination("testDestination");
        train.setTimeDeparture(LocalTime.of(12, 1));
        train.setDateDeparture(LocalDate.now());
        train.setDateArrival(LocalDate.now());
        train.setTimeArrival(LocalTime.of(12, 1));
        train.setRailId(1);

        Waggon waggon = initWaggon();
        train.getWaggons().add(waggon);

        return train;
    }
    public Waggon initWaggon(){
        Waggon waggon = new Waggon();
        waggon.setId(1);
        waggon.setWaggonNumber("testNumber");
        waggon.setComment("testComment");
        waggon.setDamageComment("testDamageComment");
        waggon.setCorrectCheckSum(true);

        LoadingUnit loadingUnit = initLoadingUnit();

        waggon.getUnitLoads().add(loadingUnit);

        return waggon;
    }
    public LoadingUnit initLoadingUnit(){
        // creating neccessary Objects for the test
        LoadingUnit loadingUnit = new LoadingUnit();
        loadingUnit.setUnitLoadNumber("test");
        loadingUnit.setCorrectCheckSum(true);
        loadingUnit.setComment("testComment");
        loadingUnit.setDamageComment("testDamageComment");
        Seal seal = new Seal();
        seal.setSealId(1);
        seal.setSealNumber(1);
        seal.setSealType("testType");


        Waggon waggon = new Waggon();
        waggon.setId(1);
        loadingUnit.setWaggon(waggon);
        DangerGoodLabels dangerGoodLabels = new DangerGoodLabels();
        dangerGoodLabels.setLoadingUnit(loadingUnit);
        dangerGoodLabels.setStatusNumber(1);
        dangerGoodLabels.setPosition(1);


        loadingUnit.getDangerGoodLabels().add(dangerGoodLabels);
        loadingUnit.getSeals().add(seal);

        return loadingUnit;
    }

    // test  to check if all trains are called
    @Test
    void getAllTrainsTest() throws Exception {
        Train train = initTrain();

        List<Train> allTrains = Collections.singletonList(train);
        given(trainRepository.findAll()).willReturn(allTrains);

        mvc.perform(get("/v1/trains")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].trainId", is((int)train.getTrainId())))
            .andExpect(jsonPath("$[0].source", is(train.getSource())))
            .andExpect(jsonPath("$[0].destination", is(train.getDestination())))
            .andExpect(jsonPath("$[0].dateDeparture", is(train.getDateDeparture().toString())))
            .andExpect(jsonPath("$[0].timeDeparture", is(train.getTimeDeparture().format(DateTimeFormatter.ofPattern("HH:mm:ss")))))
            .andExpect(jsonPath("$[0].dateArrival", is(train.getDateArrival().toString())))
            .andExpect(jsonPath("$[0].timeArrival", is(train.getTimeArrival().format(DateTimeFormatter.ofPattern("HH:mm:ss")))))
            .andExpect(jsonPath("$[0].railId", is(train.getRailId())));
    }

    // test  to check if one train is called
    @Test
    void getTrain() throws Exception {
        Train train = initTrain();

        Optional<Train> optionalTrain = Optional.of(train);

        given(trainRepository.findById(1L)).willReturn(optionalTrain);

        mvc.perform(get("/v1/train/1")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.trainId", is((int)train.getTrainId())))
            .andExpect(jsonPath("$.source", is(train.getSource())))
            .andExpect(jsonPath("$.destination", is(train.getDestination())))
            .andExpect(jsonPath("$.dateDeparture", is(train.getDateDeparture().toString())))
            .andExpect(jsonPath("$.timeDeparture", is(train.getTimeDeparture().format(DateTimeFormatter.ofPattern("HH:mm:ss")))))
            .andExpect(jsonPath("$.dateArrival", is(train.getDateArrival().toString())))
            .andExpect(jsonPath("$.timeArrival", is(train.getTimeArrival().format(DateTimeFormatter.ofPattern("HH:mm:ss")))))
            .andExpect(jsonPath("$.railId", is(1)));
    }
    // test to check if Exception is called correctly
    @Test
    void getTrain_TrainNotFound() throws Exception {
        long trainId = 123;

        // Mock the repository to return an empty Optional
        when(trainRepository.findById(trainId)).thenReturn(Optional.empty());

        // Assert that the API endpoint call throws an exception
        NestedServletException nestedServletException = assertThrows(NestedServletException.class, () -> {
            String url = "/v1/train/" + trainId;
            mvc.perform(get(url));
        }, "Expected NestedServletException was not thrown.");

        // Extract the actual exception
        Throwable rootCause = nestedServletException.getRootCause();

        // Assert that the exception is of type NoSuchElementException
        Assertions.assertEquals(NoSuchElementException.class, rootCause.getClass());

        // Verify that the method has been called
        verify(trainRepository, times(1)).findById(trainId);
    }

    // test to calling all waggons
    @Test
    void getAllWaggons() throws Exception {
        Waggon waggon = initWaggon();

        List<Waggon> allWaggons = Collections.singletonList(waggon);

        given(waggonRepository.findAll()).willReturn(allWaggons);

        mvc.perform(get("/v1/waggons")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].id", is((int) waggon.getId())))
            .andExpect(jsonPath("$[0].comment", is((waggon.getComment()))))
            .andExpect(jsonPath("$[0].damageComment", is(waggon.getDamageComment())))
            .andExpect(jsonPath("$[0].waggonPosition", is(waggon.getWaggonPosition())));
    }
    // test to call one Waggon by Id
    @Test
    void getWaggon() throws Exception {
        Waggon waggon = initWaggon();

        Optional<Waggon> optionalWaggon = Optional.of(waggon);

        given(waggonRepository.findById(1L)).willReturn(optionalWaggon);

        mvc.perform(get("/v1/waggon/1")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is((int) waggon.getId())))
            .andExpect(jsonPath("$.comment", is((waggon.getComment()))))
            .andExpect(jsonPath("$.damageComment", is(waggon.getDamageComment())))
            .andExpect(jsonPath("$.waggonPosition", is(waggon.getWaggonPosition())));
    }

    @Test
    void getWaggon_waggonNotFound() throws Exception {
        long waggonId = 123;

        // Mock the repository to return an empty Optional
        when(waggonRepository.findById(waggonId)).thenReturn(Optional.empty());

        // Assert that the API endpoint call throws an exception
        NestedServletException nestedServletException = assertThrows(NestedServletException.class, () -> {
            String url = "/v1/waggon/" + waggonId;
            mvc.perform(get(url));
        }, "Expected NestedServletException was not thrown.");

        // Extract the actual exception
        Throwable rootCause = nestedServletException.getRootCause();

        // Assert that the exception is of type NoSuchElementException
        Assertions.assertEquals(NoSuchElementException.class, rootCause.getClass());

        // Verify that the method has been called
        verify(waggonRepository, times(1)).findById(waggonId);
    }

    //Test Post Methods
    @Test
    void testUpdateOrCreateWaggon() throws Exception {
        // Prepare test data
        String waggonNumber = "1234";
        Waggon waggon = initWaggon();
        waggon.setWaggonNumber(waggonNumber);
        WaggonDto waggonDto = mapper.mapWaggon(waggon);

        // Mock the repository to return an existing waggon
        when(waggonRepository.findByWaggonNumber(waggonNumber)).thenReturn(waggon);
        when(waggonRepository.save(any(Waggon.class))).thenReturn(waggon);

        // Call the API endpoint
        String url = "/v1/saveWaggon/" + waggonNumber;
        mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(waggonDto)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is((int) waggon.getId())))
            .andExpect(jsonPath("$.waggonNumber", is(waggon.getWaggonNumber())))
            .andExpect(jsonPath("$.comment", is(waggon.getComment())))
            .andExpect(jsonPath("$.damageComment", is(waggon.getDamageComment())));

        // Verify that the repository methods were called with the expected parameters
        verify(waggonRepository, times(1)).findByWaggonNumber(waggonNumber);
        verify(waggonRepository, times(1)).save(any(Waggon.class));

        // Mock the repository to return a null waggon
        when(waggonRepository.findByWaggonNumber(waggonNumber)).thenReturn(null);
        when(waggonRepository.save(any(Waggon.class))).thenReturn(waggon);

        // Call the API endpoint again with a new waggon
        WaggonDto newWaggonDto = mapper.mapWaggon(initWaggon());
        mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newWaggonDto)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").exists())
            .andExpect(jsonPath("$.waggonNumber", is(waggonNumber)))
            .andExpect(jsonPath("$.comment", is(newWaggonDto.getComment())))
            .andExpect(jsonPath("$.damageComment", is(newWaggonDto.getDamageComment())));

        // Verify that the repository methods were called with the expected parameters
        verify(waggonRepository, times(2)).findByWaggonNumber(waggonNumber);
        verify(waggonRepository, times(2)).save(any(Waggon.class));
    }
    @Test
    void testDeleteWaggon() throws Exception {
        // Prepare test data
        String waggonNumber = "1234";
        Waggon waggon = initWaggon();
        waggon.setWaggonNumber(waggonNumber);

        when(waggonRepository.findByWaggonNumber(waggonNumber)).thenReturn(waggon);

        // Call the API endpoint to delete the waggon
        String url = "/v1/deleteWaggon/" + waggonNumber;
        mvc.perform(delete(url))
            .andExpect(status().isNoContent());

        // Verify that the waggon has been deleted from the repository
        verify(waggonRepository, times(1)).deleteByWaggonNumber(waggonNumber);
    }
    @Test
    void testDeleteWaggon_WaggonNumberIsNull() throws Exception {
        // Call the API endpoint with null waggonNumber and expect a 400 Bad Request
        String url = "/v1/deleteWaggon/" + null;

        mvc.perform(delete(url))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("waggon Number is required."));
    }

    @Test
    void testAddComment() throws Exception {
        // Prepare test data
        String waggonNumber = "1234";
        String comment = "Test comment";
        Waggon waggon = initWaggon();
        waggon.setWaggonNumber(waggonNumber);

        when(waggonRepository.findByWaggonNumber(waggonNumber)).thenReturn(waggon);
        when(waggonRepository.save(any(Waggon.class))).thenReturn(waggon);

        //Call the API endpoint to change comment the waggon
        String url = "/v1/waggon/addComment/" + waggonNumber;
        mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(comment))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.comment", is(comment)));

        // Verify that the methods has been called
        verify(waggonRepository, times(1)).findByWaggonNumber(waggonNumber);
        verify(waggonRepository, times(1)).save(waggon);
    }
    @Test
    void testAddCommentWaggonNotFound() {
        // Prepare test data
        String waggonNumber = "1234";
        String comment = "Test comment";

        when(waggonRepository.findByWaggonNumber(waggonNumber)).thenReturn(null);

        // Assert that the API endpoint call throws an exception
        NestedServletException nestedServletException = assertThrows(NestedServletException.class, () -> {
            String url = "/v1/waggon/addComment/" + waggonNumber;
            mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(comment));
        }, "test ,Expected NestedServletException was not thrown.");

        // Extract the actual cause of the exception
        Throwable rootCause = nestedServletException.getRootCause();

        // Assert that the cause of the exception is of type EntityNotFoundException
        assertEquals(EntityNotFoundException.class, rootCause.getClass());

        // Verify that the method has been called
        verify(waggonRepository, times(1)).findByWaggonNumber(waggonNumber);
    }

    @Test
    void testAddDamageComment() throws Exception {
        // Prepare test data
        String waggonNumber = "1234";
        String damageComment = "Test damage comment";
        Waggon waggon = initWaggon();
        waggon.setWaggonNumber(waggonNumber);

        when(waggonRepository.findByWaggonNumber(waggonNumber)).thenReturn(waggon);
        when(waggonRepository.save(any(Waggon.class))).thenReturn(waggon);

        // Call the API endpoint to create or change Damage Comment
        String url = "/v1/waggon/addDamageComment/" + waggonNumber;
        mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(damageComment))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.damageComment", is(damageComment)));

        // Verify that the methods has been called
        verify(waggonRepository, times(1)).findByWaggonNumber(waggonNumber);
        verify(waggonRepository, times(1)).save(waggon);
    }

    @Test
    void testAddDamageCommentWaggonNotFound() {
        // Prepare test data
        String waggonNumber = "1234";
        String damageComment = "Test  Damage comment";

        when(waggonRepository.findByWaggonNumber(waggonNumber)).thenReturn(null);

        // Assert that the API endpoint call throws an exception
        NestedServletException nestedServletException = assertThrows(NestedServletException.class, () -> {
            String url = "/v1/waggon/addDamageComment/" + waggonNumber;
            mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(damageComment));
        }, "test ,Expected NestedServletException was not thrown.");

        // Extract the actual cause of the exception
        Throwable rootCause = nestedServletException.getRootCause();

        // Assert that the cause of the exception is of type EntityNotFoundException
        assertEquals(EntityNotFoundException.class, rootCause.getClass());

        // Verify that the method has been called
        verify(waggonRepository, times(1)).findByWaggonNumber(waggonNumber);
    }
}
