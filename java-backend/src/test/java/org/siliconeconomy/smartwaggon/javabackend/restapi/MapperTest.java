/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.restapi;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.smartwaggon.javabackend.models.*;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.LoadingUnitDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.TrainDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.WaggonDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class MapperTest {
    @Autowired
    private Mapper mapper;
    @Test
     void testMapTrain() {
        Train train = new Train();
        train.setTrainId(1L);
        train.setSource("Express");


        Waggon waggon = new Waggon();
        waggon.setId(1L);

        train.getWaggons().add(waggon);
        TrainDto trainDto = mapper.mapTrain(train);

        assertEquals(train.getTrainId(), trainDto.getTrainId());
        assertEquals(train.getRailId(), trainDto.getRailId());
        assertEquals(train.getSource(), trainDto.getSource());
        assertEquals(train.getWaggons().size(), trainDto.getWaggons().size());
        assertEquals(train.getWaggons().isEmpty(), trainDto.getWaggons().isEmpty());
        assertEquals(train.getWaggons().contains(waggon.getId()), trainDto.getWaggons().contains(waggon.getId()));
    }

    @Test
     void testMapWaggon() {
        Waggon waggon = new Waggon();
        waggon.setId(1L);
        waggon.setWaggonNumber("testNumber");
        waggon.setWaggonPosition(1);
        waggon.setComment("testComment");
        waggon.setDamageComment("testComment");
        waggon.setCorrectCheckSum(true);

        LoadingUnit lu = new LoadingUnit();
        lu.setUnitLoadNumber("testNumber");

        waggon.getUnitLoads().add(lu);
        Train train = new Train();
        train.setTrainId(1L);

        waggon.setTrain(train);

        WaggonDto waggonDto = mapper.mapWaggon(waggon);

        assertEquals(waggon.getId(), waggonDto.getId());
        assertEquals(waggon.getWaggonPosition(), waggonDto.getWaggonPosition());
        assertEquals(waggon.getComment(), waggonDto.getComment());
        assertEquals(waggon.getDamageComment(), waggonDto.getDamageComment());
        assertEquals(waggon.getUnitLoads().size(), waggonDto.getUnitLoads().size());
    }
    @Test
     void testMapLoadingUnit() {

        LoadingUnit loadingUnit = new LoadingUnit();

        DangerGoodLabels dangerGoodLabels = new DangerGoodLabels();
        dangerGoodLabels.setPosition(1);
        dangerGoodLabels.setStatusNumber(1);
        dangerGoodLabels.setUNId(1);

        Seal seal = new Seal();
        seal.setSealType("testStatus");
        seal.setSealId(1);

        Waggon waggon = new Waggon();
        waggon.setId(1);

        loadingUnit.setUnitLoadNumber("test ID");
        loadingUnit.getSeals().add(seal);
        loadingUnit.getDangerGoodLabels().add(dangerGoodLabels);
        loadingUnit.setWaggon(waggon);

        LoadingUnitDto loadingUnitDto = mapper.mapLoadingUnit(loadingUnit);

        assertEquals(loadingUnit.getUnitLoadNumber(), loadingUnitDto.getUnitLoadNumber());
        assertEquals(loadingUnit.getComment(), loadingUnitDto.getComment());
        assertEquals(loadingUnit.getDamageComment(), loadingUnitDto.getDamageComment());

        assertEquals(loadingUnit.getSeals().size(), loadingUnitDto.getSeals().size());
        assertEquals(loadingUnit.getDangerGoodLabels().size(), loadingUnitDto.getDangerGoodLabels().size());
    }
}
