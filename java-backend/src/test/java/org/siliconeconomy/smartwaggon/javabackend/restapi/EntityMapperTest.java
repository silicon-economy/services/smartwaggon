/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.restapi;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.smartwaggon.javabackend.models.*;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.*;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class EntityMapperTest {
    @Autowired
    private EntityMapper mapper;
    @Test
    void testMapTrain() {
        TrainDto trainDto = new TrainDto();
        trainDto.setTrainId(1L);
        trainDto.setSource("Express");


        WaggonDto waggonDto = new WaggonDto();
        waggonDto.setId(1L);

        trainDto.getWaggons().add(waggonDto);
        Train train = mapper.mapTrain(trainDto);

        assertEquals(train.getTrainId(), trainDto.getTrainId());
        assertEquals(train.getRailId(), trainDto.getRailId());
        assertEquals(train.getSource(), trainDto.getSource());
        assertEquals(train.getWaggons().size(), trainDto.getWaggons().size());
        assertEquals(train.getWaggons().isEmpty(), trainDto.getWaggons().isEmpty());
        assertEquals(train.getWaggons().contains(waggonDto.getId()), train.getWaggons().contains(waggonDto.getId()));
    }

    @Test
    void testMapWaggon() {
        WaggonDto waggonDto = new WaggonDto();
        waggonDto.setId(1L);

        TrainDto trainDto = new TrainDto();
        trainDto.setTrainId(1L);
        waggonDto.setWaggonNumber("testNumber");
        waggonDto.setWaggonPosition(1);
        waggonDto.setDamageComment("testComment");
        waggonDto.setCorrectCheckSum(true);
        waggonDto.setComment("testComment");

        LoadingUnitDto loadingUnitDto = new LoadingUnitDto();
        loadingUnitDto.setUnitLoadNumber("test");

        waggonDto.getUnitLoads().add(loadingUnitDto);
        Waggon waggon = mapper.mapWaggon(waggonDto);

        assertEquals(waggon.getId(), waggonDto.getId());
        assertEquals(waggon.getWaggonNumber(), waggonDto.getWaggonNumber());
        assertEquals(waggon.getWaggonPosition(), waggonDto.getWaggonPosition());
        assertEquals(waggon.getUnitLoads().size(), waggonDto.getUnitLoads().size());
    }

    @Test
    void testMapLoadingUnit() {
        // Given
        LoadingUnitDto loadingUnitDto = new LoadingUnitDto();
        loadingUnitDto.setUnitLoadNumber("test ID");
        loadingUnitDto.setComment("comment");
        loadingUnitDto.setDamageComment("damageComment");

        DangerGoodLabelsDto dangerGoodLabelsDto = new DangerGoodLabelsDto();
        dangerGoodLabelsDto.setPosition(1);
        dangerGoodLabelsDto.setLoadingUnit("test ID");
        dangerGoodLabelsDto.setUNId(1);


        SealDto sealDto = new SealDto();
        sealDto.setSealType("testType");
        sealDto.setSealId(1);

        loadingUnitDto.getSeals().add(sealDto);
        loadingUnitDto.getDangerGoodLabels().add(dangerGoodLabelsDto);

        LoadingUnit loadingUnit = mapper.mapLoadingUnit(loadingUnitDto);


        assertEquals(loadingUnit.getUnitLoadNumber(), loadingUnitDto.getUnitLoadNumber());
        assertEquals(loadingUnit.getDamageComment(), loadingUnitDto.getDamageComment());
        assertEquals(loadingUnitDto.getComment(),loadingUnit.getComment());
        assertEquals(loadingUnit.getDangerGoodLabels().size(), loadingUnitDto.getDangerGoodLabels().size());
        assertEquals(loadingUnit.getSeals().size(), loadingUnitDto.getSeals().size());
    }

    @Test
     void testMapSeal() {
        // create a sample SealDto
        SealDto sealDto = new SealDto();
        sealDto.setSealNumber(12345);
        sealDto.setSealType("TestType");

        // call the mapSeal method
        Seal seal = mapper.mapSeal(sealDto);

        // compare the properties
        assertEquals(sealDto.getSealNumber(), seal.getSealNumber());
        assertEquals(sealDto.getSealType(), seal.getSealType());
    }
    @Test
     void testMapDangerGoodLabel() {
        // create a sample DangerGoodLabelsDto
        DangerGoodLabelsDto dangerGoodLabelsDto = new DangerGoodLabelsDto();
        dangerGoodLabelsDto.setPosition(3);
        dangerGoodLabelsDto.setUNId(1);
        dangerGoodLabelsDto.setLoadingUnit("Test");

        // call the mapDangerGoodLabel method
        DangerGoodLabels dangerGoodLabels = mapper.mapDangerGoodLabel(dangerGoodLabelsDto);

        // compare the properties
        assertEquals(dangerGoodLabelsDto.getStatusNumber(), dangerGoodLabels.getStatusNumber());
        assertEquals(dangerGoodLabelsDto.getPosition(), dangerGoodLabels.getPosition());
        assertEquals(dangerGoodLabelsDto.getUNId(), dangerGoodLabels.getUNId());
    }
}
