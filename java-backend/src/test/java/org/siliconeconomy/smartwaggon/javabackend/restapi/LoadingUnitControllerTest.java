/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.smartwaggon.javabackend.restapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.siliconeconomy.smartwaggon.javabackend.models.DangerGoodLabels;
import org.siliconeconomy.smartwaggon.javabackend.models.LoadingUnit;
import org.siliconeconomy.smartwaggon.javabackend.models.Seal;
import org.siliconeconomy.smartwaggon.javabackend.models.Waggon;
import org.siliconeconomy.smartwaggon.javabackend.repository.*;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.DangerGoodLabelsDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.LoadingUnitDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.EntityMapper;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import javax.persistence.EntityNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(value ={LoadingUnitController.class}, excludeAutoConfiguration = SecurityAutoConfiguration.class)
 class LoadingUnitControllerTest {

    @Autowired
    private LoadingUnitController loadingUnitController;

    @Autowired
    private MockMvc mvc;
    @MockBean
    LoadingUnitRepository loadingUnitRepository;
    @MockBean
    WaggonRepository waggonRepository;
    @MockBean
    TrainRepository trainRepository;
    @MockBean
    SealRepository sealRepository;
    @MockBean
    DangerGoodLabelRepository dangerGoodLabelRepository;
    @MockBean
    Mapper mapper;
    @MockBean
    EntityMapper entityMapper;
    @Autowired
    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        MapperTestConfiguration mapperTestConfiguration = new MapperTestConfiguration();
        mapperTestConfiguration.configureWaggonMapper(mapper);
        mapperTestConfiguration.configureLoadingUnitMapper(mapper);
        mapperTestConfiguration.configureTrainMapper(mapper);
        mapperTestConfiguration.configureEntityTrainWaggonMapper(entityMapper);
        mapperTestConfiguration.configureEntityTrainMapper(entityMapper);
        mapperTestConfiguration.configureEntityLoadingUnitMapper(entityMapper);
    }

    public LoadingUnit initLoadingUnit() {
        // creating neccessary Objects for the test
        LoadingUnit loadingUnit = new LoadingUnit();
        loadingUnit.setUnitLoadNumber("test");
        loadingUnit.setCorrectCheckSum(true);
        loadingUnit.setComment("testComment");
        loadingUnit.setDamageComment("testDamageComment");
        Seal seal = new Seal();
        seal.setSealId(1);
        seal.setSealNumber(1);
        seal.setSealType("testType");


        Waggon waggon = new Waggon();
        waggon.setId(1);
        loadingUnit.setWaggon(waggon);
        DangerGoodLabels dangerGoodLabels = new DangerGoodLabels();
        dangerGoodLabels.setLoadingUnit(loadingUnit);
        dangerGoodLabels.setStatusNumber(1);
        dangerGoodLabels.setPosition(1);


        loadingUnit.getDangerGoodLabels().add(dangerGoodLabels);
        loadingUnit.getSeals().add(seal);

        return loadingUnit;
    }

    public Waggon initWaggon() {
        Waggon waggon = new Waggon();
        waggon.setId(1);
        waggon.setWaggonNumber("testNumber");
        waggon.setComment("testComment");
        waggon.setDamageComment("testDamageComment");
        waggon.setCorrectCheckSum(true);

        LoadingUnit loadingUnit = initLoadingUnit();

        waggon.getUnitLoads().add(loadingUnit);

        return waggon;
    }

    @Test
    void getAllLoadingUnits() throws Exception {
        // creating necessary Objects for the test
        LoadingUnit loadingUnit = initLoadingUnit();

        List<LoadingUnit> allLoadingUnits = Collections.singletonList(loadingUnit);

        given(loadingUnitRepository.findAll()).willReturn(allLoadingUnits);

        mvc.perform(get("/v1/loading-units")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].unitLoadNumber", is(loadingUnit.getUnitLoadNumber())))
            .andExpect(jsonPath("$[0].comment", is(loadingUnit.getComment())))
            .andExpect(jsonPath("$[0].damageComment", is(loadingUnit.getDamageComment())));
    }

    // test for one Loading unit
    @Test
    void getLoadingUnit() throws Exception {
        // creating necessary Objects for the test
    LoadingUnit loadingUnit=initLoadingUnit();

        Optional<LoadingUnit> optionalLoadingUnit = Optional.of(loadingUnit);

        given(loadingUnitRepository.findById("test")).willReturn(optionalLoadingUnit);

        mvc.perform(get("/v1/loading-unit/"+loadingUnit.getUnitLoadNumber())
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.unitLoadNumber", is(loadingUnit.getUnitLoadNumber())))
            .andExpect(jsonPath("$.comment", is(loadingUnit.getComment())))
            .andExpect(jsonPath("$.damageComment", is(loadingUnit.getDamageComment())));
    }

    @Test
    void getLoadingUnit_LuNotFound() {
        String LoadingUnitId = "123";

        // Mock the repository to return an empty Optional
        when(loadingUnitRepository.findById(LoadingUnitId)).thenReturn(Optional.empty());

        // Assert that the API endpoint call throws an exception
        NestedServletException nestedServletException = assertThrows(NestedServletException.class, () -> {
            String url = "/v1/loading-unit/" + LoadingUnitId;
            mvc.perform(get(url));
        }, "Expected NestedServletException was not thrown.");

        // Extract the actual exception
        Throwable rootCause = nestedServletException.getRootCause();

        // Assert that the exception is of type NoSuchElementException
        Assertions.assertEquals(NoSuchElementException.class, rootCause.getClass());

        // Verify that the method has been called
        verify(loadingUnitRepository, times(1)).findById(LoadingUnitId);
    }

    @Test
    void testCreateLoadingUnit() throws Exception {
        // creating necessary Objects for the test
        LoadingUnit loadingUnit = initLoadingUnit();
        Waggon waggon = new Waggon();
        waggon.setId(1);
        waggon.setWaggonNumber("test");
        loadingUnit.setWaggon(waggon);


        LoadingUnitDto loadingUnitDto = mapper.mapLoadingUnit(loadingUnit);

        when(waggonRepository.findByWaggonNumber("test")).thenReturn(waggon);
        when(loadingUnitRepository.save(any(LoadingUnit.class))).thenReturn(loadingUnit);


        mvc.perform(post(String.format("/v1/waggon/%s/saveLoading-unit", waggon.getWaggonNumber()))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loadingUnitDto)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.unitLoadNumber", is(loadingUnit.getUnitLoadNumber())))
            .andExpect(jsonPath("$.comment", is(loadingUnit.getComment())))
            .andExpect(jsonPath("$.damageComment", is(loadingUnit.getDamageComment())));
        verify(waggonRepository, times(1)).save(waggon);
    }

    @Test
    void testCreateLoadingUnit_WaggonNotFound() {
        // Test case for waggon not found
        String waggonNumber = "test";

        when(waggonRepository.findByWaggonNumber(waggonNumber)).thenReturn(null);

        NestedServletException nestedServletException = assertThrows(NestedServletException.class, () -> mvc.perform(post("/v1/waggon/{waggonNumber}/saveLoading-unit", waggonNumber)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(new LoadingUnitDto()))));

        Throwable rootCause = nestedServletException.getRootCause();

        Assertions.assertEquals(EntityNotFoundException.class, rootCause.getClass());

        // Verify that the necessary methods have been called
        verify(waggonRepository, times(1)).findByWaggonNumber(waggonNumber);
        verify(loadingUnitRepository, never()).save(any(LoadingUnit.class));
    }
    @Test
    void testCreateOrUpdateDangerGoodLabels_ValidInputs() throws UnsupportedEncodingException, URISyntaxException {
        // Mock data
        String loadingUnitNumber = "123";
        LoadingUnit loadingUnit = initLoadingUnit();

        ModelMapper mapper1 = new ModelMapper();
        DangerGoodLabelsDto[] dangerGoodLabelsDtos = {mapper1.map(loadingUnit.getDangerGoodLabels(), DangerGoodLabelsDto.class)};

        // Mock repository methods
        when(loadingUnitRepository.findLoadingUnitByUnitLoadNumber(loadingUnitNumber)).thenReturn(loadingUnit);
        when(entityMapper.mapDangerGoodLabel(any(DangerGoodLabelsDto.class))).thenReturn(new DangerGoodLabels());

        // Create a valid LoadingUnitDto with a non-null unitLoadNumber
        LoadingUnitDto loadingUnitDto = new LoadingUnitDto();
        loadingUnitDto.setUnitLoadNumber("12345");
        when(mapper.mapLoadingUnit(loadingUnit)).thenReturn(loadingUnitDto);

        // Call the method
        ResponseEntity<LoadingUnitDto> response = loadingUnitController.createOrUpdateDangerGoodLabels(loadingUnitNumber, dangerGoodLabelsDtos);

        // Assertions
        Assertions.assertEquals(ResponseEntity.created(new URI("/v1loading-unit/12345")).body(loadingUnitDto), response);
        verify(waggonRepository, times(1)).save(any(Waggon.class));
    }

    @Test
    void testCreateOrUpdateDangerGoodLabels_InvalidInputs() {
        // Mock data
        String loadingUnitNumber = null;
        DangerGoodLabelsDto[] dangerGoodLabelsDtos = null;

        // Call the method and assert for exception
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> loadingUnitController.createOrUpdateDangerGoodLabels(loadingUnitNumber, dangerGoodLabelsDtos));

        Assertions.assertEquals("loadingUnitNumber and dangerGoodLabelsDtos must be provided.", exception.getMessage());
        verifyNoInteractions(loadingUnitRepository);
        verifyNoInteractions(waggonRepository);
    }


    @Test
    void testDeleteLoadingUnit() throws Exception {
        // Prepare test data
        String waggonNumber = "1234";
        String loadingUnitNumber = "5678";
        Waggon waggon = initWaggon();
        waggon.setWaggonNumber(waggonNumber);
        LoadingUnit loadingUnit = initLoadingUnit();
        loadingUnit.setUnitLoadNumber(loadingUnitNumber);
        waggon.getUnitLoads().add(loadingUnit);

        when(waggonRepository.findByWaggonNumber(waggonNumber)).thenReturn(waggon);
        doNothing().when(loadingUnitRepository).deleteById(loadingUnitNumber);
        when(waggonRepository.save(any(Waggon.class))).thenReturn(waggon);

        // Call the Api method
        String url = "/v1/deleteLoading-unit/waggon/" + waggonNumber + "/loading-unit/" + loadingUnitNumber;
        mvc.perform(delete(url))
            .andExpect(status().isNoContent());

        // Verify that the methods has been called
        verify(waggonRepository, times(1)).findByWaggonNumber(waggonNumber);
        verify(loadingUnitRepository, times(1)).deleteById(loadingUnitNumber);
        verify(waggonRepository, times(1)).save(waggon);
    }



//    @Test
//    void testDeleteLoadingUnit_LoadingUnitNotFound() throws Exception {
//        // Mock the repository methods
//        when(waggonRepository.findByWaggonNumber(anyString())).thenReturn(null);
//
//        String waggonNumber = "123";
//        String loadingUnitNumber = "456";
//
//        NestedServletException nestedServletException = assertThrows(NestedServletException.class, () -> {
//            String url = "v1/deleteLoading-unit/waggon/" + waggonNumber + "/loading-unit/" + loadingUnitNumber;
//            mvc.perform(delete(url)
//                .contentType(MediaType.APPLICATION_JSON));
//        }, "test ,Expected NestedServletException was not thrown.");
//
//        // Extract the actual cause of the exception
//        Throwable rootCause = nestedServletException.getRootCause();
//
//        // Assert that the cause of the exception is of type EntityNotFoundException
//        assertEquals(EntityNotFoundException.class, rootCause.getClass());
//
//    }
    @Test
    void testAddCommentToLoadingUnit() throws Exception {
        // Prepare test data
        String loadingUnitNumber = "LU1234";
        String comment = "This is a test comment.";

        LoadingUnit loadingUnit = new LoadingUnit();
        loadingUnit.setUnitLoadNumber(loadingUnitNumber);
        Waggon waggon=new Waggon();
        waggon.setId(1);
        loadingUnit.setWaggon(waggon);
        when(loadingUnitRepository.findLoadingUnitByUnitLoadNumber(loadingUnitNumber)).thenReturn(loadingUnit);

        //Call the API endpoint to  add or change comment the Loading unit
        String url = "/v1/loading-unit/addComment/" + loadingUnitNumber;

        mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(comment))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.comment", is(comment)));
        // Verify that the methods has been called
        verify(loadingUnitRepository, times(1)).save(any(LoadingUnit.class));
    }

    @Test
    void testAddCommentLoadingUnit_NotFound() {
        // Prepare test data
        String loadingUnitNumber = "1234";
        String comment = "Test Damage comment";

        when(loadingUnitRepository.findLoadingUnitByUnitLoadNumber(loadingUnitNumber)).thenReturn(null);

        // Assert that the API endpoint call throws an exception
        NestedServletException nestedServletException = assertThrows(NestedServletException.class, () -> {
            String url = "/v1/loading-unit/addComment/" + loadingUnitNumber;
            mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(comment));
        }, "test ,Expected NestedServletException was not thrown.");

        // Extract the actual cause of the exception
        Throwable rootCause = nestedServletException.getRootCause();

        // Assert that the cause of the exception is of type EntityNotFoundException
        Assertions.assertEquals(EntityNotFoundException.class, rootCause.getClass());

        // Verify that the method has been called
        verify(loadingUnitRepository, times(1)).findLoadingUnitByUnitLoadNumber(loadingUnitNumber);
    }
    @Test
    void testAddDamageCommentToLoadingUnit() throws Exception {
        String loadingUnitNumber = "LU1234";
        String damageComment = "This is a test damage comment.";

        LoadingUnit loadingUnit = new LoadingUnit();
        loadingUnit.setUnitLoadNumber(loadingUnitNumber);

        Waggon waggon=new Waggon();
        waggon.setId(1);
        loadingUnit.setWaggon(waggon);

        //Call the API endpoint to  add or change Damage comment the Loading unit
        String url = "/v1/loading-unit/addDamageComment/" + loadingUnitNumber;

        when(loadingUnitRepository.findLoadingUnitByUnitLoadNumber(loadingUnitNumber)).thenReturn(loadingUnit);
        when(loadingUnitRepository.save(any(LoadingUnit.class))).thenReturn(loadingUnit);

        mvc.perform(put(url)
                .content(damageComment)
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.damageComment", is(damageComment)));

        // Verify that the methods has been called
        verify(loadingUnitRepository, times(1)).save(any(LoadingUnit.class));
    }

    @Test
    void testAddDamageCommentLoadingUnit_NotFound() {
        // Prepare test data
        String loadingUnitNumber = "1234";
        String damageComment = "Test Damage comment";

        when(loadingUnitRepository.findLoadingUnitByUnitLoadNumber(loadingUnitNumber)).thenReturn(null);

        // Assert that the API endpoint call throws an exception
        NestedServletException nestedServletException = assertThrows(NestedServletException.class, () -> {
            String url = "/v1/loading-unit/addDamageComment/" + loadingUnitNumber;
            mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(damageComment));
        }, "test ,Expected NestedServletException was not thrown.");

        // Extract the actual cause of the exception
        Throwable rootCause = nestedServletException.getRootCause();

        // Assert that the cause of the exception is of type EntityNotFoundException
        Assertions.assertEquals(EntityNotFoundException.class, rootCause.getClass());

        // Verify that the method has been called
        verify(loadingUnitRepository, times(1)).findLoadingUnitByUnitLoadNumber(loadingUnitNumber);
    }
}
