/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.smartwaggon.javabackend.restapi;

import org.siliconeconomy.smartwaggon.javabackend.models.LoadingUnit;
import org.siliconeconomy.smartwaggon.javabackend.models.Train;
import org.siliconeconomy.smartwaggon.javabackend.models.Waggon;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.LoadingUnitDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.TrainDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.dto.WaggonDto;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.EntityMapper;
import org.siliconeconomy.smartwaggon.javabackend.restapi.mapper.Mapper;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

public class MapperTestConfiguration {
    public  void configureWaggonMapper(Mapper mapper){
        given(mapper.mapWaggon(any(Waggon.class))).willAnswer(invocation -> {
            Waggon waggon = invocation.getArgument(0);
            WaggonDto waggonDto = new WaggonDto();
            waggonDto.setId(waggon.getId());
            waggonDto.setWaggonPosition(waggon.getWaggonPosition());
            waggonDto.setDamageComment(waggon.getDamageComment());
            waggonDto.setWaggonNumber(waggon.getWaggonNumber());
            waggonDto.setComment(waggon.getComment());
            waggonDto.setCorrectCheckSum(waggon.isCorrectCheckSum());
            return waggonDto;
        });
    }
    public  void configureEntityTrainWaggonMapper(EntityMapper entityMapper){
        given(entityMapper.mapWaggon(any(WaggonDto.class))).willAnswer(invocation -> {
            WaggonDto waggonDto = invocation.getArgument(0);
            Waggon waggon = new Waggon();
            waggon.setId(waggonDto.getId());
            waggon.setWaggonNumber(waggonDto.getWaggonNumber());
            waggon.setCorrectCheckSum(waggonDto.isCorrectCheckSum());
            waggon.setDamageComment(waggonDto.getDamageComment());
            waggon.setComment(waggonDto.getComment());
            return waggon;
        });
    }
    public  void configureTrainMapper(Mapper mapper){
        given(mapper.mapTrain(any(Train.class))).willAnswer(invocation -> {
            Train train = invocation.getArgument(0);
            TrainDto trainDto = new TrainDto();
            trainDto.setTrainId(train.getTrainId());
            trainDto.setSource(train.getSource());
            trainDto.setDestination(train.getDestination());
            trainDto.setTimeDeparture(train.getTimeDeparture());
            trainDto.setDateDeparture(train.getDateDeparture());
            trainDto.setDateArrival(train.getDateArrival());
            trainDto.setTimeArrival(train.getTimeArrival());
            trainDto.setRailId(train.getRailId());
            return trainDto;
        });
    }
    public  void configureEntityTrainMapper(EntityMapper entityMapper){
        given(entityMapper.mapTrain(any(TrainDto.class))).willAnswer(invocation -> {
            TrainDto trainDto = invocation.getArgument(0);
            Train train = new Train();
            train.setTrainId(trainDto.getTrainId());
            train.setSource(trainDto.getSource());
            train.setDestination(trainDto.getDestination());
            train.setTimeDeparture(trainDto.getTimeDeparture());
            train.setDateDeparture(trainDto.getDateDeparture());
            train.setDateArrival(trainDto.getDateArrival());
            train.setTimeArrival(trainDto.getTimeArrival());
            train.setRailId(trainDto.getRailId());
            return train;
        });
    }
    public  void configureLoadingUnitMapper(Mapper mapper){
        given(mapper.mapLoadingUnit(any(LoadingUnit.class))).willAnswer(invocation -> {
            LoadingUnit loadingUnit = invocation.getArgument(0);
            LoadingUnitDto loadingUnitDto = new LoadingUnitDto();
            loadingUnitDto.setUnitLoadNumber(loadingUnit.getUnitLoadNumber());
            loadingUnitDto.setDamageComment(loadingUnit.getDamageComment());
            loadingUnitDto.setComment(loadingUnit.getComment());
            loadingUnitDto.setCorrectCheckSum(loadingUnit.isCorrectCheckSum());
            loadingUnitDto.setWaggonDto(loadingUnit.getWaggon().getId());
            return loadingUnitDto;
        });
    }
    public  void configureEntityLoadingUnitMapper(EntityMapper entityMapper){
        given(entityMapper.mapLoadingUnit(any(LoadingUnitDto.class))).willAnswer(invocation -> {
            LoadingUnitDto loadingUnitDto = invocation.getArgument(0);
            LoadingUnit loadingUnit = new LoadingUnit();
            loadingUnit.setUnitLoadNumber(loadingUnitDto.getUnitLoadNumber());
            loadingUnit.setComment(loadingUnitDto.getComment());
            loadingUnit.setDamageComment(loadingUnitDto.getDamageComment());
            loadingUnit.setCorrectCheckSum(loadingUnitDto.isCorrectCheckSum());
            Waggon waggon= new Waggon();
            waggon.setId(loadingUnitDto.getWaggonDto());
            loadingUnit.setWaggon(waggon);

            return loadingUnit;
        });
    }


}
