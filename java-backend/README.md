# Java application for storing and managing waggon and loading unit informations

This is a simple Java application that manages the captured waggons and loading units.
It provides a Rest API that allows receiving information from the front-end.
The data is saved to a PostgreSQL database.

### Technology stack

* Java
* SpringBoot
* Eclipse Paho
* PostgreSQL

### Build and run

__All following commands should be executed in the directory 'smartwaggon/java-backend'.__

Run tests: `mvn test`

After running the tests a protocol of the results will be written to 'smartwaggon.java-backend.test-results.log'.

Build: `mvn clean package`

Before starting the application ensure that a PostgreSQL database.
The connection parameters have to be adjusted in 'src/main/resources/application.properties'.

For the database please specify:
* __spring.datasource.url__: URL (jdbc:postgres://_ip_:_port_/_databasename_)
* __spring.datasource.username__: username
* __spring.datasource.password__: password

##### Build and run with Docker

To build and run the application in docker, execute:

Build: `docker build -t java-backend .`

Run: `docker run -p 8080:8080 java-backend`

To specify database and MQTT parameters when running with docker use an environment file and specify it in the docker run command as follows:

`docker run -p 8080:8080 java-backend -env-file /path/to/env-file`

### Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.txt` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.txt` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
  The content of this file is maintained manually.

#### Generating third-party license reports

This project uses the [license-maven-plugin](https://github.com/mojohaus/license-maven-plugin) to generate a file containing the licenses used by the third-party dependencies.
The content of the `mvn license:add-third-party` Maven goal's output (`target/generated-sources/license/THIRD-PARTY.txt`) can be copied into `third-party-licenses/third-party-licenses.txt`.

Third-party dependencies for which the licenses cannot be determined automatically by the license-maven-plugin have to be documented manually in `third-party-licenses/third-party-licenses-complementary.txt`.
In the `third-party-licenses/third-party-licenses.txt` file these third-party dependencies have an "Unknown license" license.


### Troubleshooting

To start the program locally
all the ports in the frontend must set to the port of the Api gateway
