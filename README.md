> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.


# Smart Waggon Inspector

## Project Overview
The Smart Waggon Inspector is a web application that enables a waggon inspector to record the waggons and loading units of a train using any device with an internet connection. This is done in a guided and convenient manner, significantly reducing errors, duplicate inspections and forgotten steps.

The Smart Waggon Inspector Service is decomposed into several main parts:
* The **angular-frontend** (The web-interface for the user to interact with the service)
<!--- * The **python-backend** (The Logic for the OCR function) --->
* The **java-backend** (The logic of dispatching and storing given data)
* The **api-gateway** (The logic for the backend communication)

A Python backend for the OCR functions is also under development.
This is not available yet.
However, it is planned in the overall architecture and therefore interfaces for communication with the planned Python backend can be found in the source code.

## Setting up the project
* You need to set up the database (we use postgreSQL as an example implementation). If you are using another database,
  configure Spring Boot Data accordingly.
* We recommend using continuous integration, e.g. gitlab-ci. For this, set up a corresponding gitlab-ci.yml
* Configure the config files (/config/ and /helm/<service>/values.yml) in the services according to your technical
  requirements. Pay attention to the URLs to the required services and set them to your needs.

## Running the project
To run and test all services together (integration testing), please run the services in the following order:

* database (optional, see the backend's [README](java-backend/README.md#build-and-run))
* java-backend (see the backend's [README](java-backend/README.md#build-and-run))
* angular-frontend (see frontend [README](angular-frontend/README.md))
* api-gateway (see api-gateway [README](smartwaggoninspector-api-gateway/README.md))
<!--- * python-backend (see python-backend [README](ai-engine/README.md)) --->

Or when docker is installed on your current system, then only run `docker compose up --build`

Please follow the instructions of the services in their respective readme file!

## Documentation

For more details, please read the [documentation](documentation).

## License
Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the [LICENSE](LICENSE) file.


## Contact information
* Product Owner:
  * Lara Hövener  ([Mail](mailto:lara.hoevener@iml.fraunhofer.de))
* Development Team Maintainer:
  * Sandra Jankowski ([Mail](mailto:sandra.jankowski@iml.fraunhofer.de))
  * Jonas Stilling ([Mail](mailto:jonas.stilling@isst.fraunhofer.de))
* Development Team:
  * Rami Aldrea
  * Ebrahim Ehsanfar
  * Mohamad Wasim Masri
  * Ulrich Franzke
