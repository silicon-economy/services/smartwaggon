/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterService } from './services/state/footer.service';
import { SidenavService } from './services/state/sidenav.service';
import { ThemeService } from './services/state/theme.service';
import { CookieDialogComponent } from './dialogs/cookie-dialog/cookie-dialog.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { FlexModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { MatTabsModule } from '@angular/material/tabs';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
  declarations: [CookieDialogComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    FlexModule,
    RouterModule,
    MatTabsModule,
    TranslateModule,
    MatCardModule,
    MatDividerModule,
  ],
  exports: [],
  providers: [FooterService, SidenavService, ThemeService],
})
export class SharedModule {}
