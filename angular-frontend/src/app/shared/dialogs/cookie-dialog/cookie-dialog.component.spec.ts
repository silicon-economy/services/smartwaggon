/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CookieDialogComponent } from './cookie-dialog.component';
import { MatDialogRef } from '@angular/material/dialog';

describe('CookieDialogComponent', () => {
  let component: CookieDialogComponent;
  let fixture: ComponentFixture<CookieDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CookieDialogComponent],
      providers: [{ provide: MatDialogRef, useValue: {} }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CookieDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close dialog with true value when the button is clicked', () => {
    const dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);
    component.dialogRef = dialogRefSpy;
    component.onClick();
    expect(dialogRefSpy.close).toHaveBeenCalledWith(true);
  });
});
