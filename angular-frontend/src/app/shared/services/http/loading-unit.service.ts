/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { unitLoadDTO } from '../state/DTO/unitLoadDTO';
import { sealDTO } from '../state/DTO/sealDTO';
import { dangerGoodLabelDTO } from '../state/DTO/dangerGoodLabelDTO';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LoadingUnitService {
  private readonly deleteLuUrl: string;
  readonly getAllLuUrl: string;
  readonly Url: string;

  private static readonly API_GATEWAY_URL = `${environment.smwiBackendUrl}`;
  private static readonly JAVA_BACKEND_URL = `${LoadingUnitService.API_GATEWAY_URL}/java-backend`;

  constructor(private http: HttpClient) {
    this.getAllLuUrl = `${LoadingUnitService.JAVA_BACKEND_URL}/v1/loading-units`;
    this.deleteLuUrl = `${LoadingUnitService.JAVA_BACKEND_URL}/v1//deleteLoading-unit/`;
    this.Url = `${LoadingUnitService.JAVA_BACKEND_URL}/v1/`;
  }

  public getAllLoadingUnits(): Observable<unitLoadDTO[]> {
    return this.http.get<unitLoadDTO[]>(this.getAllLuUrl);
  }
  public saveLoadingUnit(waggonNumber: string, loadingunit: unitLoadDTO) {
    return this.http.post<unitLoadDTO>(
      `${this.Url}/waggon/${waggonNumber}/saveLoading-unit/`,
      loadingunit,
    );
  }
  public saveSeal(loadingUnitNumber: string, seals: sealDTO[]) {
    return this.http.post<unitLoadDTO>(
      `${this.Url}/loading-unit/${loadingUnitNumber}/saveSeals/`,
      seals,
    );
  }
  public saveDangerGoodLabel(
    loadingUnitNumber: string,
    dangerGoodLabels: dangerGoodLabelDTO[],
  ) {
    return this.http.post<unitLoadDTO>(
      `${this.Url}/loading-unit/${loadingUnitNumber}/saveDangerGoodLabels/`,
      dangerGoodLabels,
    );
  }
  public deleteLoadingUnit(
    loadingUnitNumber: string,
    waggonNumber: string,
  ): Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.deleteLuUrl}/waggon/${waggonNumber}/loading-unit/${loadingUnitNumber}`,
    );
  }
  public addComment(loadingUnit: string, comment: string) {
    return this.http.put<unitLoadDTO>(
      `${this.Url}/loading-unit/addComment/${loadingUnit}`,
      comment,
    );
  }
  public addDamageComment(loadingUnit: string, damageComment: string) {
    return this.http.put<unitLoadDTO>(
      `${this.Url}/loading-unit/addDamageComment/${loadingUnit}`,
      damageComment,
    );
  }
}
