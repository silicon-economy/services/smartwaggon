/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { waggonDTO } from '../state/DTO/waggonDTO';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class WaggonService {
  readonly saveWaggonUrl: string;
  readonly deleteWaggonUrl: string;
  readonly getAllWaggonsUrl: string;
  readonly putUrl: string;

  private static readonly API_GATEWAY_URL = `${environment.smwiBackendUrl}`;
  private static readonly JAVA_BACKEND_URL = `${WaggonService.API_GATEWAY_URL}/java-backend`;

  constructor(private http: HttpClient) {
    this.saveWaggonUrl = `${WaggonService.JAVA_BACKEND_URL}/v1/saveWaggon`;
    this.getAllWaggonsUrl = `${WaggonService.JAVA_BACKEND_URL}/v1/waggons`;
    this.deleteWaggonUrl = `${WaggonService.JAVA_BACKEND_URL}/v1/deleteWaggon`;
    this.putUrl = `${WaggonService.JAVA_BACKEND_URL}/v1/`;
  }
  public getAllWaggons(): Observable<waggonDTO[]> {
    return this.http.get<waggonDTO[]>(this.getAllWaggonsUrl);
  }
  public save(waggon: waggonDTO, waggonNumber: string) {
    return this.http.put<waggonDTO>(
      `${this.saveWaggonUrl}/${waggonNumber}`,
      waggon,
    );
  }

  public addComment(waggonNumber: string, comment: string) {
    return this.http.put<waggonDTO>(
      `${this.putUrl}/waggon/addComment/${waggonNumber}`,
      comment,
    );
  }

  public addDamageComment(waggonNumber: string, damageComment: string) {
    return this.http.put<waggonDTO>(
      `${this.putUrl}/waggon/addDamageComment/${waggonNumber}`,
      damageComment,
    );
  }
  public deleteWaggon(waggonNumber: string): Observable<HttpResponse<never>> {
    return this.http.delete<any>(`${this.deleteWaggonUrl}/${waggonNumber}`);
  }
}
