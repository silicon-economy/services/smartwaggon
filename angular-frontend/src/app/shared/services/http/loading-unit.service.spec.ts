/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { LoadingUnitService } from './loading-unit.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { unitLoadDTO } from '../state/DTO/unitLoadDTO';
import { sealDTO } from '../state/DTO/sealDTO';
import { dangerGoodLabelDTO } from '../state/DTO/dangerGoodLabelDTO';

describe('LoadingUnitService', () => {
  let service: LoadingUnitService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LoadingUnitService],
    });
    service = TestBed.inject(LoadingUnitService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all loading units', () => {
    const mockLoadingUnits: unitLoadDTO[] = [
      {
        unitLoadNumber: '12345',
        creationDate: new Date(),
        modificationDate: new Date(),
        dangerGoodLabels: [],
        seals: [],
        comment: 'Test comment',
        damageComment: 'Test damage comment',
        correctCheckSum: true,
      },
    ];
    service.getAllLoadingUnits().subscribe((loadingUnits) => {
      expect(loadingUnits.length).toBe(1);
      expect(loadingUnits).toEqual(mockLoadingUnits);
    });
    const request = httpMock.expectOne(service.getAllLuUrl);
    expect(request.request.method).toBe('GET');
    request.flush(mockLoadingUnits);
  });

  it('should save a loading unit', () => {
    const waggonNumber = 'test';
    const mockSeals: sealDTO[] = [
      { sealNumber: '1', sealTypus: 'testType' },
      { sealNumber: '2', sealTypus: 'sealType' },
    ];
    const mockLoadingUnit: unitLoadDTO = {
      unitLoadNumber: '12345',
      creationDate: new Date(),
      modificationDate: new Date(),
      dangerGoodLabels: [],
      seals: mockSeals,
      comment: 'Test comment',
      damageComment: 'Test damage comment',
      correctCheckSum: true,
    };
    service
      .saveLoadingUnit(waggonNumber, mockLoadingUnit)
      .subscribe((response) => {
        expect(response).toEqual(mockLoadingUnit);
      });
    const request = httpMock.expectOne(
      `${service.Url}/waggon/${waggonNumber}/saveLoading-unit/`,
    );
    expect(request.request.method).toBe('POST');
    request.flush(mockLoadingUnit);
  });

  it('should save seals', () => {
    const loadingUnitNumber = '1';
    const mockSeals: sealDTO[] = [
      { sealNumber: '1', sealTypus: 'testType' },
      { sealNumber: '2', sealTypus: 'sealType' },
    ];
    const mockLoadingUnit: unitLoadDTO = {
      unitLoadNumber: '12345',
      creationDate: new Date(),
      modificationDate: new Date(),
      dangerGoodLabels: [],
      seals: mockSeals,
      comment: 'Test comment',
      damageComment: 'Test damage comment',
      correctCheckSum: true,
    };
    service.saveSeal(loadingUnitNumber, mockSeals).subscribe((response) => {
      expect(response).toEqual(mockLoadingUnit);
    });
    const request = httpMock.expectOne(
      `${service.Url}/loading-unit/${loadingUnitNumber}/saveSeals/`,
    );
    expect(request.request.method).toBe('POST');
    request.flush(mockLoadingUnit);
  });
  it('should save danger good labels', () => {
    const loadingUnitNumber = '1';
    const mockLabels: dangerGoodLabelDTO[] = [
      {
        statusNumber: 1,
        statusStr: 'stringtest',
        position: 1,
        creationDate: new Date(),
        modificationDate: new Date(),
      },
      {
        statusNumber: 2,
        statusStr: 'stringtest2',
        position: 2,
        creationDate: new Date(),
        modificationDate: new Date(),
      },
    ];
    const mockLoadingUnit: unitLoadDTO = {
      unitLoadNumber: '12345',
      creationDate: new Date(),
      modificationDate: new Date(),
      dangerGoodLabels: mockLabels,
      seals: [],
      comment: 'Test comment',
      damageComment: 'Test damage comment',
      correctCheckSum: true,
    };
    service
      .saveDangerGoodLabel(loadingUnitNumber, mockLabels)
      .subscribe((response) => {
        expect(response).toEqual(mockLoadingUnit);
      });
    const request = httpMock.expectOne(
      `${service.Url}/loading-unit/${loadingUnitNumber}/saveDangerGoodLabels/`,
    );
    expect(request.request.method).toBe('POST');
    request.flush(mockLoadingUnit);
  });
});
