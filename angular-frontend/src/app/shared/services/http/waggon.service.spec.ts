/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { WaggonService } from './waggon.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { waggonDTO } from '../state/DTO/waggonDTO';

describe('WaggonService', () => {
  let service: WaggonService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WaggonService],
    });
    service = TestBed.inject(WaggonService);
    httpMock = TestBed.inject(HttpTestingController);
  });
  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve All waggons', () => {
    const creationDate: Date = new Date('2023-05-01');
    const modificationDate: Date = new Date('2023-05-02');

    const expectedWaggon: waggonDTO[] = [
      {
        waggonNumber: '11',
        waggonPosition: 1,
        comment: 'This is a comment.',
        correctCheckSum: false,
        unitLoads: [],
        creationDate: creationDate,
        modificationDate: modificationDate,
        damageComment: 'No damages found.',
      },
    ];
    service.getAllWaggons().subscribe((waggons: waggonDTO[]) => {
      expect(waggons.length).toBe(1);
      expect(waggons).toEqual(expectedWaggon);
    });
    const req = httpMock.expectOne(service.getAllWaggonsUrl);
    expect(req.request.method).toBe('GET');
    req.flush(expectedWaggon);
  });
  it('should save a Waggon', () => {
    const creationDate: Date = new Date('2023-05-01');
    const modificationDate: Date = new Date('2023-05-02');
    const waggon: waggonDTO = {
      waggonNumber: '11',
      waggonPosition: 1,
      comment: 'This is a comment.',
      correctCheckSum: false,
      unitLoads: [],
      creationDate: creationDate,
      modificationDate: modificationDate,
      damageComment: 'No damages found.',
    };
    service.save(waggon, waggon.waggonNumber).subscribe((response: waggonDTO) => {
      expect(response).toEqual(waggon);
    });
    const request = httpMock.expectOne(
      `${service.saveWaggonUrl}/${waggon.waggonNumber}`,
    );
    expect(request.request.method).toBe('PUT');
    request.flush(waggon);
  });
  it('should add a comment to a waggon', () => {
    const waggonNumber = '11';
    const comment = 'This is a comment.';
    service.addComment(waggonNumber, comment).subscribe((response) => {
      expect(response).toBeTruthy();
    });
    const request = httpMock.expectOne(
      `${service.putUrl}/waggon/addComment/${waggonNumber}`,
    );
    expect(request.request.method).toBe('PUT');
    expect(request.request.body).toEqual(comment);
    request.flush({});
  });

  it('should add a damage comment to a waggon', () => {
    const waggonNumber = '11';
    const damageComment = 'No damages found.';
    service
      .addDamageComment(waggonNumber, damageComment)
      .subscribe((response) => {
        expect(response).toBeTruthy();
      });
    const request = httpMock.expectOne(
      `${service.putUrl}/waggon/addDamageComment/${waggonNumber}`,
    );
    expect(request.request.method).toBe('PUT');
    expect(request.request.body).toEqual(damageComment);
    request.flush({});
  });
});
