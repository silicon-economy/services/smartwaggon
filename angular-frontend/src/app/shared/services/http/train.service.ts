/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { trainDTO } from '../state/DTO/trainDTO';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TrainService {
  readonly postTrainUrl: string;
  readonly getTrainUrl: string;
  readonly getAllTrainsUrl: string;

  private static readonly API_GATEWAY_URL = `${environment.smwiBackendUrl}`;
  private static readonly JAVA_BACKEND_URL = `${TrainService.API_GATEWAY_URL}/java-backend`;

  constructor(private http: HttpClient) {
    this.postTrainUrl = `${TrainService.JAVA_BACKEND_URL}/v1/saveTrain`;
    this.getAllTrainsUrl = `${TrainService.JAVA_BACKEND_URL}v1/Trains`;
    this.getTrainUrl = `${TrainService.JAVA_BACKEND_URL}v1/train`;
  }
  public getAllTrains(): Observable<trainDTO[]> {
    return this.http.get<trainDTO[]>(this.getAllTrainsUrl);
  }
  public getTrain(): Observable<trainDTO> {
    return this.http.get<trainDTO>(this.getTrainUrl);
  }
  public save(train: trainDTO) {
    return this.http.post<trainDTO>(this.postTrainUrl, train);
  }
}
