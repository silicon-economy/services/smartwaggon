/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { TrainService } from './train.service';
import { trainDTO } from '../state/DTO/trainDTO';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

describe('trainService', () => {
  let service: TrainService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TrainService],
    });
    service = TestBed.inject(TrainService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should retrieve all trains', () => {
    const mockTrains: trainDTO[] = [
      {
        rail_id: '123',
        date_arrival: new Date('2023-05-01'),
        date_departure: new Date('2023-05-02'),
        source: 'sourceTest',
        destination: 'destinationTest',
        creationDate: new Date(),
        modificationDate: new Date(),
        waggon: [],
      },
    ];
    service.getAllTrains().subscribe((trains: trainDTO[]) => {
      expect(trains.length).toBe(1);
      expect(trains).toEqual(mockTrains);
    });
    const request = httpMock.expectOne(service.getAllTrainsUrl);
    expect(request.request.method).toBe('GET');
    request.flush(mockTrains);
  });

  it('should retrieve a train', () => {
    const mockTrain: trainDTO = {
      rail_id: '123',
      date_arrival: new Date('2023-05-01'),
      date_departure: new Date('2023-05-02'),
      source: 'sourceTest',
      destination: 'destinationTest',
      creationDate: new Date(),
      modificationDate: new Date(),
      waggon: [],
    };
    service.getTrain().subscribe((train: trainDTO) => {
      expect(train).toEqual(mockTrain);
    });
    const request = httpMock.expectOne(service.getTrainUrl);
    expect(request.request.method).toBe('GET');
    request.flush(mockTrain);
  });

  it('should save a train', () => {
    const mockTrain: trainDTO = {
      rail_id: '123',
      date_arrival: new Date('2023-05-01'),
      date_departure: new Date('2023-05-02'),
      source: 'sourceTest',
      destination: 'destinationTest',
      creationDate: new Date(),
      modificationDate: new Date(),
      waggon: [],
    };
    service.save(mockTrain).subscribe((train: trainDTO) => {
      expect(train).toEqual(mockTrain);
    });
    const request = httpMock.expectOne(service.postTrainUrl);
    expect(request.request.method).toBe('POST');
    request.flush(mockTrain);
  });
});
