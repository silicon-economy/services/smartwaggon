/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { dangerGoodLabelDTO } from './DTO/dangerGoodLabelDTO';
import { trainDTO } from './DTO/trainDTO';
import { waggonDTO } from './DTO/waggonDTO';

import { TrainStorageService } from './train-storage.service';
import { expect } from '@angular/flex-layout/_private-utils/testing';
import { unitLoadDTO } from './DTO/unitLoadDTO';

describe('TrainStorageService', () => {
  let service: TrainStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrainStorageService);
  });

  afterAll(() => {
    service.removeAllEntries();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should test removeAllEntries', () => {
    service.removeAllEntries();
    const testString = localStorage.getItem('trainStorage');
    expect(testString).toBe(null);
  });

  it('should test getTrain with empty Storage', () => {
    const train: trainDTO = service.getTrain();
    const testString = train.rail_id;
    service.removeAllEntries();
    expect(testString).toBe('Phantom');
  });

  it('should test getTrain with filled Storage', () => {
    service.createTrain('Emma');
    const train: trainDTO = service.getTrain();
    const testString = train.rail_id;
    service.removeAllEntries();
    expect(testString).toBe('Emma');
  });

  it('should test getDangergoodStatesFromUnitLoadAsString()', () => {
    const waggon: waggonDTO = service.addWaggon('einWagen');
    service.addUnitLoadToWaggon('12345', waggon);
    service.setDangergoodLabelToUnitLoad(
      '12345',
      dangerGoodLabelDTO.positionFront,
      1,
    );
    service.setDangergoodLabelToUnitLoad(
      '12345',
      dangerGoodLabelDTO.positionRear,
      2,
    );
    service.setDangergoodLabelToUnitLoad(
      '12345',
      dangerGoodLabelDTO.positionRight,
      3,
    );
    const testString: string =
      service.getDangergoodStatesFromUnitLoadAsString('12345');
    service.removeAllEntries();
    expect(testString).toBe('1203');
  });

  it('should test train-source and destination on empty localStorage', () => {
    let isGood = true;
    service.removeAllEntries();
    service.setTrainSource('Lummerland');
    service.setTrainDestination('Kummerland');
    const source = service.getTrainSource();
    const destination = service.getTrainDestination();
    if (source.charAt(0) != 'L' || destination.charAt(0) != 'K') {
      isGood = false;
    }
    if (source.substring(1) != destination.substring(1)) {
      isGood = false;
    }
    service.removeAllEntries();
    expect(isGood).toBeTrue();
  });

  it('should test the waggonfunctions', () => {
    let isGood = true;
    service.addWaggon('Wagen 1');
    service.addWaggon('Wagen 2');
    service.addWaggon('Wagen 3');
    service.addWaggon('Wagen 4');
    service.addWaggon('Wagen 5');
    service.addWaggon('Wagen 3');
    if (service.getWaggon('Wagen 3').waggonNumber != 'Wagen 3') {
      isGood = false;
    }
    expect(isGood).toBeTruthy();
  });

  describe('createTrain and save train to local storage', () => {
    it('should create a new train if no train exists in localStorage', () => {
      const train = service.createTrain('123');
      expect(train.rail_id).toBe('123');
      expect(train.creationDate instanceof Date).toBeTrue();
      expect(train.modificationDate instanceof Date).toBeTrue();
      expect(train.waggon).toEqual([]);
    });

    it('should create a new train if rail_id does not match existing train in localStorage', () => {
      const existingTrain = new trainDTO();
      existingTrain.rail_id = '456';
      localStorage.setItem('trainStorage', JSON.stringify(existingTrain));
      const train = service.createTrain('123');
      expect(train.rail_id).toBe('123');
      expect(train.creationDate instanceof Date).toBeTrue();
      expect(train.modificationDate instanceof Date).toBeTrue();
      expect(train.waggon).toEqual([]);
    });
  });

  it('should create a train with the name "Phantom"', () => {
    const phantomTrain = service.createPhantomTrain();
    expect(phantomTrain.rail_id).toBe('Phantom');
  });

  describe('TrainService', () => {
    describe('saveTrainToLocalStorage', () => {
      it('should save trainDTO to localStorage', () => {
        const train = service.createPhantomTrain();
        service.saveTrainToLocalStorage(train);
        const trainString: string = localStorage.getItem('trainStorage');
        expect(trainString).toBeDefined();
        expect(trainString).toBe(JSON.stringify(train));
      });
    });

    describe('Waggon functions', () => {
      it('should return null if train is not present in localStorage', () => {
        spyOn(service, 'getTrain').and.returnValue(null);
        const result: waggonDTO = service.getWaggon('W001');
        expect(result).toBeNull();
      });

      it('should return a trainDTO from localStorage', () => {
        const train = service.createPhantomTrain();
        const trainString: string = JSON.stringify(train);
        localStorage.setItem('trainStorage', trainString);
        const result: trainDTO = service.getTrain();
        expect(result).toBeDefined();
        expect(result.rail_id).toBe(train.rail_id);
      });

      it('should return null if waggon is not found in train', () => {
        spyOn(service, 'getTrain');
        const result: waggonDTO = service.getWaggon('W001');
        expect(result).toBeNull();
      });

      it('should return the waggon if found in train', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        const result: waggonDTO = service.getWaggon('W001');
        expect(result).toBeDefined();
        expect(result.waggonNumber).toBe('W001');
        service.removeAllEntries();
      });

      it('should return the index of the waggon if found in train', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.addWaggon('W002');
        service.setWaggonPosition(1, 'W001');
        service.setWaggonPosition(2, 'W002');
        const waggon: waggonDTO = service.getWaggon('W002');
        const result: number = service.getCoordinateOfWaggonInArray('W002');
        const result2 = service.getWaggonByPosition(2);
        const result3 = service.getWaggonNameByPosition(2);
        expect(result).toBe(1);
        expect(result2.waggonNumber).toBe(waggon.waggonNumber);
        expect(result3).toBe('W002');
        service.removeAllEntries();
      });

      it('should test the getTotalWaggonCount function', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        expect(service.getTotalWaggonCount()).toBe(1);
        service.addWaggon('W002');
        expect(service.getTotalWaggonCount()).toBe(2);
        service.removeAllEntries();
        expect(service.getTotalWaggonCount()).toBe(0);
      });

      it('should test the add and get comment functions for a waggon', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.addCommentToWaggon('W001', 'comment');
        expect(service.getWaggon('W001').comment).toBe('comment');
        const result = service.getCommentByWaggonName('W001');
        expect(result).toBe('comment');
        service.removeAllEntries();
      });

      it('should test the add and get damage comment functions for a waggon', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.addDamageCommentToWaggon('W001', 'comment');
        expect(service.getWaggon('W001').damageComment).toBe('comment');
        const result = service.getDamageCommentByWaggonName('W001');
        expect(result).toBe('comment');
        service.removeAllEntries();
      });

      it('should test the remove function for a waggon with decreasing of waggon position', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.addWaggon('W002');
        service.addWaggon('W003');
        service.setWaggonPosition(1, 'W001');
        service.setWaggonPosition(2, 'W002');
        service.setWaggonPosition(3, 'W003');
        expect(service.getWaggon('W001').waggonPosition).toBe(1);
        expect(service.getWaggon('W003').waggonPosition).toBe(3);
        service.removeWaggonByName('W002');
        expect(service.getWaggon('W001').waggonPosition).toBe(1);
        expect(service.getWaggon('W003').waggonPosition).toBe(2);
        expect(service.getWaggon('W002')).toBe(null);
        service.removeAllEntries();
      });

      it('should set the check sum status to a waggon', function () {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.setChecksumToWaggon(true, 'W001');
        expect(service.getWaggon('W001').correctCheckSum).toBeTruthy();
        service.removeAllEntries();
      });

      it('should test the checkIfWaggonNumberIsAlreadyUsed function', function () {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.addWaggon('W002');
        service.setWaggonPosition(2, 'W002');
        service.addWaggon('W002');
        service.setWaggonPosition(3, 'W003');
        expect(service.checkIfWaggonNumberIsAlreadyUsed('W002')).toBeTrue();
        expect(service.checkIfWaggonNumberIsAlreadyUsed('W005')).toBeFalse();
        service.removeAllEntries();
      });
    });

    describe('unitLoad functions', () => {
      it('should test the get and add functions of unit load to a TrainDTO and covers addUnitLoadToWaggon and addUnitLoad', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.addUnitLoad('W001', 'ABCD123');
        service.addUnitLoad('W001', 'ABCD321');
        let unitLoadList = [
          service.getWaggon('W001').unitLoads[0].unitLoadNumber,
          service.getWaggon('W001').unitLoads[1].unitLoadNumber,
        ];
        const unitLoad1 = service.getWaggon('W001').unitLoads[0];
        const unitLoad1FromFunction =
          service.getUnitloadByUnitloadNumber('ABCD123');
        expect(unitLoadList).toEqual(['ABCD123', 'ABCD321']);
        expect(unitLoad1).toEqual(unitLoad1FromFunction);
        service.addUnitLoad('W001', 'ABCD321');
        unitLoadList = service.getUnitLoadNumbersOnWaggonWithName('W001');
        expect(unitLoadList).toEqual(['ABCD123', 'ABCD321']);
        service.removeAllEntries();
      });

      it('should test the getWaggonOfUnitLoadByUnitLoadNumber function ', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.addUnitLoad('W001', 'ABCD123');
        const waggonResult =
          service.getWaggonOfUnitLoadByUnitLoadNumber('ABCD123');
        expect(waggonResult).toEqual(service.getWaggon('W001'));
        service.removeAllEntries();
      });

      it('should test the function getCoordinateOfUnitLoadInArray', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.addUnitLoad('W001', 'ABCD123');
        service.addUnitLoad('W001', 'ABCD321');
        expect(
          service.getCoordinateOfUnitLoadInArray('ABCD321').unitLoadPos,
        ).toBe(1);
        expect(
          service.getCoordinateOfUnitLoadInArray('ABCD123').unitLoadPos,
        ).toBe(0);
        expect(service.getCoordinateOfUnitLoadInArray('ABCD321').waggonPos).toBe(
          0,
        );
        service.removeAllEntries();
      });

      it('should test the comment functionality for unit loads', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.addUnitLoad('W001', 'ABCD123');
        service.addCommentToUnitLoad('ABCD123', 'comment');
        const commentResult = service.getCommentByUnitLoadNumber('ABCD123');
        expect(service.getUnitloadByUnitloadNumber('ABCD123').comment).toBe(
          'comment',
        );
        expect(commentResult).toBe('comment');
        service.removeAllEntries();
      });

      it('should test the damage comment functionality for unit loads', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.addUnitLoad('W001', 'ABCD123');
        service.addDamageCommentToUnitLoad('ABCD123', 'comment');
        const commentResult =
          service.getDamageCommentByUnitLoadNumber('ABCD123');
        expect(
          service.getUnitloadByUnitloadNumber('ABCD123').damageComment,
        ).toBe('comment');
        expect(commentResult).toBe('comment');
        service.removeAllEntries();
      });

      it('should test the remove function for unit loads', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.addUnitLoad('W001', 'ABCD123');
        service.addUnitLoad('W001', 'ABCD321');
        service.addUnitLoad('W001', 'ABCD111');
        expect(service.getUnitLoadNumbersOnWaggonWithName('W001').length).toBe(
          3,
        );
        expect(
          service.getCoordinateOfUnitLoadInArray('ABCD111').unitLoadPos,
        ).toBe(2);
        service.removeUnitLoadByName('ABCD321');
        expect(service.getUnitLoadNumbersOnWaggonWithName('W001').length).toBe(
          2,
        );
        expect(
          service.getCoordinateOfUnitLoadInArray('ABCD111').unitLoadPos,
        ).toBe(1);
        service.removeAllEntries();
      });

      it('should test the checkIfUnitLoadNumberIsAlreadyUsed function', () => {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.addUnitLoad('W001', 'ABCD123');
        service.addUnitLoad('W001', 'ABCD321');
        service.addUnitLoad('W001', 'ABCD111');
        service.addWaggon('W002');
        service.setWaggonPosition(2, 'W002');
        service.addUnitLoad('W002', 'BCD123');
        service.addUnitLoad('W002', 'BCD321');
        service.addUnitLoad('W002', 'BCD111');
        expect(service.checkIfUnitLoadNumberIsAlreadyUsed('BCD123')).toBeTrue();
        expect(service.checkIfUnitLoadNumberIsAlreadyUsed('333')).toBeFalse();
        service.removeAllEntries();
      });

      it('should set the check sum status to a unit load', function () {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.addUnitLoad('W001', 'ABCD123');
        service.setCheckSumToUnitLoad(true, 'ABCD123');
        expect(
          service.getUnitloadByUnitloadNumber('ABCD123').correctCheckSum,
        ).toBeTruthy();
        service.removeAllEntries();
      });

      it('should test the creation of a seal', function () {
        const testSeal = service.createSigillum('typeA', '1234');
        expect(testSeal.sealTypus).toBe('typeA');
        expect(testSeal.sealNumber).toBe('1234');
        service.removeAllEntries();
      });

      it('should test the adding of a seal to loading unit and the get seal function', function () {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.addUnitLoad('W001', 'ABCD123');
        const testSeal1 = service.createSigillum('typeA', '1234');
        const testSeal2 = service.createSigillum('typeB', '1111');
        service.addSigillumToUnitLoad('ABCD123', testSeal1);
        service.addSigillumToUnitLoad('ABCD123', testSeal2);
        const actualSeals = service.getSigillumsFromUnitLoad('ABCD123');
        expect(actualSeals[0].sealTypus).toBe('typeA');
        expect(actualSeals[1].sealTypus).toBe('typeB');
        expect(actualSeals[0].sealNumber).toBe('1234');
        expect(actualSeals[1].sealNumber).toBe('1111');
        service.addSigillumToUnitLoad('ABCD123', testSeal2);
        expect(actualSeals[2]).toBeUndefined();
        service.removeAllEntries();
      });

      it('should test the adding of multiple seals to an unit load', function () {
        service.createPhantomTrain();
        service.addWaggon('W001');
        service.setWaggonPosition(1, 'W001');
        service.addUnitLoad('W001', 'ABCD123');
        const testSeal1 = service.createSigillum('typeA', '1234');
        const testSeal2 = service.createSigillum('typeB', '1111');
        const actualSeals = [testSeal1, testSeal2];
        service.setSigillumsToUnitLoad('ABCD123', actualSeals);
        expect(
          service.getUnitloadByUnitloadNumber('ABCD123').seals[0].sealTypus,
        ).toBe('typeA');
        expect(
          service.getUnitloadByUnitloadNumber('ABCD123').seals[1].sealTypus,
        ).toBe('typeB');
        expect(
          service.getUnitloadByUnitloadNumber('ABCD123').seals[0].sealNumber,
        ).toBe('1234');
        expect(
          service.getUnitloadByUnitloadNumber('ABCD123').seals[1].sealNumber,
        ).toBe('1111');
        service.removeAllEntries();
      });
    });
  });

  it('should return the dangerGoodLabels of a unit load', () => {
    const unitLoadNumber = '123';
    const mockUnitLoad: unitLoadDTO = {
      unitLoadNumber: '12345',
      creationDate: new Date(),
      modificationDate: new Date(),
      dangerGoodLabels: [
        {
          statusNumber: 1,
          statusStr: 'stringtest',
          position: 1,
          creationDate: new Date(),
          modificationDate: new Date(),
        },
        {
          statusNumber: 2,
          statusStr: 'stringtest2',
          position: 2,
          creationDate: new Date(),
          modificationDate: new Date(),
        },
      ],
      seals: [],
      comment: 'Test comment',
      damageComment: 'Test damage comment',
      correctCheckSum: true,
    };
    spyOn(service, 'getUnitloadByUnitloadNumber').and.returnValue(mockUnitLoad);

    const result = service.getDangerGoodLabelsFromUnitLoad(unitLoadNumber);

    expect(result.length).toBe(2);
    expect(result[0].statusNumber).toBe(1);
    expect(result[0].statusStr).toBe('stringtest');
    expect(result[1].statusNumber).toBe(2);
    expect(result[1].statusStr).toBe('stringtest2');
  });
});
