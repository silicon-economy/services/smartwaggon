/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { CoordinateTupel } from './DTO/CoordinateTupel';
import { dangerGoodLabelDTO } from './DTO/dangerGoodLabelDTO';
import { sealDTO } from './DTO/sealDTO';
import { trainDTO } from './DTO/trainDTO';
import { unitLoadDTO } from './DTO/unitLoadDTO';
import { waggonDTO } from './DTO/waggonDTO';

@Injectable({
  providedIn: 'root',
})
export class TrainStorageService {
  constructor() {
    //    this.playALittle();
    this.removeAllEntries();
  }

  /********************************************************************** clear the localStorage-List */
  removeAllEntries() {
    localStorage.removeItem('trainStorage');
  }

  /********************************************************************** working with the train */
  createTrain(number: string): trainDTO {
    const trainString: string = localStorage.getItem('trainStorage');
    let train: trainDTO = null;
    if (trainString != '') {
      train = JSON.parse(trainString);
      if (train != null && train.rail_id != number) {
        train = null;
        localStorage.removeItem('trainStorage');
      }
    }
    if (train == null) {
      train = new trainDTO();
      train.rail_id = number;
      train.creationDate = new Date(Date.now());
      train.modificationDate = train.creationDate;
      train.waggon = new Array<waggonDTO>();
    }
    this.saveTrainToLocalStorage(train);
    return train;
  }

  createPhantomTrain() {
    return this.createTrain('Phantom');
  }

  saveTrainToLocalStorage(train: trainDTO) {
    const trainString = JSON.stringify(train);
    localStorage.setItem('trainStorage', trainString);
  }

  getTrain(): trainDTO {
    const trainString: string = localStorage.getItem('trainStorage');
    let train: trainDTO = null;
    if (trainString != '') {
      train = JSON.parse(trainString);
    }
    if (train == null) {
      train = this.createPhantomTrain();
    }
    return train;
  }

  setTrainSource(location: string) {
    const train: trainDTO = this.getTrain();
    train.source = location;
    train.modificationDate = new Date(Date.now());
    this.saveTrainToLocalStorage(train);
  }
  getTrainSource(): string {
    let retVal: string = null;
    const train: trainDTO = this.getTrain();
    if (train != null) {
      retVal = train.source;
    }
    return retVal;
  }

  setTrainDestination(location: string) {
    const train: trainDTO = this.getTrain();
    train.modificationDate = new Date(Date.now());
    train.destination = location;
    this.saveTrainToLocalStorage(train);
  }
  getTrainDestination(): string {
    let retVal: string = null;
    const train: trainDTO = this.getTrain();
    if (train != null) {
      retVal = train.destination;
    }
    return retVal;
  }

  /********************************************************************** working with the waggons */
  getWaggon(waggonNumber: string): waggonDTO {
    let train: trainDTO = this.getTrain();
    let waggon: waggonDTO = null;
    if (train == null) {
      train = this.createPhantomTrain();
    }
    if (train != null) {
      let count: number;
      for (count = 0; count < train.waggon.length; count++) {
        if (train.waggon[count].waggonNumber == waggonNumber) {
          waggon = train.waggon[count];
          break;
        }
      }
    }
    return waggon;
  }

  addWaggonToTrain(waggonNumber: string, train: trainDTO): waggonDTO {
    let waggon: waggonDTO = null;
    if (train != null) {
      let count;
      for (count = 0; count < train.waggon.length; count++) {
        if (train.waggon[count].waggonNumber == waggonNumber) {
          waggon = train.waggon[count];
          break;
        }
      }
      if (waggon == null) {
        waggon = new waggonDTO();
        waggon.waggonNumber = waggonNumber;
        waggon.creationDate = new Date(Date.now());
        waggon.modificationDate = waggon.creationDate;
        waggon.unitLoads = new Array<unitLoadDTO>();
        waggon.comment = '';
        waggon.damageComment = '';
        waggon.correctCheckSum = false;
        train.waggon.push(waggon);
        this.saveTrainToLocalStorage(train);
      }
    }
    return waggon;
  }

  addWaggon(waggonNumber: string): waggonDTO {
    return this.addWaggonToTrain(waggonNumber, this.getTrain());
  }

  setWaggonPosition(position: number, waggonNumber: string): waggonDTO {
    let waggon: waggonDTO;
    const train: trainDTO = this.getTrain();
    let count: number;
    for (count = 0; count < train.waggon.length; count++) {
      if (train.waggon[count].waggonNumber == waggonNumber) {
        waggon = train.waggon[count];
        break;
      }
    }
    if (waggon != null) {
      waggon.waggonPosition = position;
      waggon.modificationDate = new Date(Date.now());
      this.saveTrainToLocalStorage(train);
    }
    return waggon;
  }

  getCoordinateOfWaggonInArray(waggonNumber: string): number {
    let retVal = -1;
    const train = this.getTrain();
    const waggon: waggonDTO = this.getWaggon(waggonNumber);
    if (waggon != null) {
      let count;
      for (count = 0; count < train.waggon.length; count++) {
        if (train.waggon[count].waggonNumber == waggonNumber) {
          retVal = count;
          break;
        }
      }
    }
    return retVal;
  }

  getTotalWaggonCount(): number {
    return this.getTrain().waggon.length;
  }

  getWaggonByPosition(position: number): waggonDTO {
    let retVal: waggonDTO = null;
    const train: trainDTO = this.getTrain();
    let cnt: number;
    for (cnt = 0; cnt < train.waggon.length; cnt++) {
      if (train.waggon[cnt].waggonPosition == position) {
        retVal = train.waggon[cnt];
        break;
      }
    }
    return retVal;
  }

  getWaggonNameByPosition(position: number): string {
    const waggon: waggonDTO = this.getWaggonByPosition(position);
    return waggon.waggonNumber;
  }

  addCommentToWaggon(waggonNumber: string, comment: string) {
    const posWaggon = this.getCoordinateOfWaggonInArray(waggonNumber);
    if (posWaggon >= 0) {
      const train = this.getTrain();
      const waggon: waggonDTO = train.waggon[posWaggon];
      if (waggon != null) {
        waggon.comment = comment;
        this.saveTrainToLocalStorage(train);
      }
    }
  }

  getCommentByWaggonName(waggonName: string): string {
    const waggon: waggonDTO = this.getWaggon(waggonName);
    return waggon.comment;
  }

  addDamageCommentToWaggon(waggonNumber: string, comment: string) {
    const posWaggon = this.getCoordinateOfWaggonInArray(waggonNumber);
    if (posWaggon >= 0) {
      const train = this.getTrain();
      const waggon: waggonDTO = train.waggon[posWaggon];
      if (waggon != null) {
        waggon.damageComment = comment;
        this.saveTrainToLocalStorage(train);
      }
    }
  }

  getDamageCommentByWaggonName(waggonName: string): string {
    const waggon: waggonDTO = this.getWaggon(waggonName);
    return waggon.damageComment;
  }

  setChecksumToWaggon(checksumStatus: boolean, waggonNumber: string) {
    const posWaggon = this.getCoordinateOfWaggonInArray(waggonNumber);
    if (posWaggon >= 0) {
      const train = this.getTrain();
      const waggon: waggonDTO = train.waggon[posWaggon];
      if (waggon != null) {
        waggon.correctCheckSum = checksumStatus;
        this.saveTrainToLocalStorage(train);
      }
    }
  }

  removeWaggonByName(waggonName: string) {
    const train: trainDTO = this.getTrain();
    const pos = this.getCoordinateOfWaggonInArray(waggonName);
    train.waggon.splice(pos, 1);
    let a: number;
    for (a = pos; a < train.waggon.length; a++) {
      train.waggon[a].waggonPosition = train.waggon[a].waggonPosition - 1;
    }
    this.saveTrainToLocalStorage(train);
  }

  checkIfWaggonNumberIsAlreadyUsed(number: string): boolean {
    const train: trainDTO = this.getTrain();
    for (let count = 0; count < train.waggon.length; count++) {
      if (train.waggon[count].waggonNumber == number) {
        return true;
      }
    }
    return false;
  }

  /********************************************************************** working with the unitloads */
  addUnitLoadToWaggon(unitLoadNumber: string, waggon: waggonDTO): unitLoadDTO {
    let unitLoad: unitLoadDTO = null;
    const train: trainDTO = this.getTrain();
    let cnt1: number,
      cnt2 = 0;
    for (cnt1 = 0; cnt1 < train.waggon.length; cnt1++) {
      if (cnt2 == -42) {
        break;
      }
      for (cnt2 = 0; cnt2 < train.waggon[cnt1].unitLoads.length; cnt2++) {
        if (
          unitLoadNumber == train.waggon[cnt1].unitLoads[cnt2].unitLoadNumber
        ) {
          unitLoad = train.waggon[cnt1].unitLoads[cnt2];
          cnt2 = -42;
          break;
        }
      }
    }
    if (unitLoad == null) {
      unitLoad = new unitLoadDTO();
      unitLoad.unitLoadNumber = unitLoadNumber;
      unitLoad.creationDate = new Date(Date.now());
      unitLoad.comment = '';
      unitLoad.damageComment = '';
      unitLoad.correctCheckSum = false;
      if (waggon != null && waggon.unitLoads != null) {
        waggon.unitLoads.push(unitLoad);
      }
    }
    unitLoad.modificationDate = unitLoad.creationDate;
    unitLoad.dangerGoodLabels = new Array<dangerGoodLabelDTO>();
    unitLoad.seals = new Array<sealDTO>();
    let a: number;
    for (a = 0; a < train.waggon.length; a++) {
      if (train.waggon[a].waggonNumber == waggon.waggonNumber) {
        train.waggon[a] = waggon;
      }
    }
    this.saveTrainToLocalStorage(train);
    return unitLoad;
  }

  checkIfUnitLoadNumberIsAlreadyUsed(loadingUnitNumber: string): boolean {
    const train: trainDTO = this.getTrain();
    for (let cnt1 = 0; cnt1 < train.waggon.length; cnt1++) {
      for (let cnt2 = 0; cnt2 < train.waggon[cnt1].unitLoads.length; cnt2++) {
        if (
          loadingUnitNumber == train.waggon[cnt1].unitLoads[cnt2].unitLoadNumber
        ) {
          return true;
        }
      }
    }
    return false;
  }

  addUnitLoad(waggonNumber: string, unitLoad: string): unitLoadDTO {
    const waggon: waggonDTO = this.getWaggon(waggonNumber);
    return this.addUnitLoadToWaggon(unitLoad, waggon);
  }

  getUnitloadByUnitloadNumber(unitLoadNumber: string): unitLoadDTO {
    let retVal: unitLoadDTO = null;
    let waggon: waggonDTO;
    const train: trainDTO = this.getTrain();
    let cnt1: number, cnt2: number;
    for (cnt1 = 0; cnt1 < train.waggon.length; cnt1++) {
      waggon = train.waggon[cnt1];
      for (cnt2 = 0; cnt2 < waggon.unitLoads.length; cnt2++) {
        if (waggon.unitLoads[cnt2].unitLoadNumber == unitLoadNumber) {
          retVal = waggon.unitLoads[cnt2];
          cnt1 = train.waggon.length;
          break;
        }
      }
    }
    return retVal;
  }

  getWaggonOfUnitLoadByUnitLoadNumber(unitLoadNumber: string): waggonDTO {
    let retVal: waggonDTO = null;
    let waggon: waggonDTO = null;
    const train: trainDTO = this.getTrain();
    let cnt1: number, cnt2: number;
    for (cnt1 = 0; cnt1 < train.waggon.length; cnt1++) {
      waggon = train.waggon[cnt1];
      for (cnt2 = 0; cnt2 < waggon.unitLoads.length; cnt2++) {
        if (waggon.unitLoads[cnt2].unitLoadNumber == unitLoadNumber) {
          retVal = waggon;
          break;
        }
      }
      if (retVal != null) {
        break;
      }
    }
    return retVal;
  }

  setCheckSumToUnitLoad(checksumStatus: boolean, unitLoadNumber: string) {
    const train = this.getTrain();
    const cTupel: CoordinateTupel =
      this.getCoordinateOfUnitLoadInArray(unitLoadNumber);
    const unitLoad: unitLoadDTO =
      train.waggon[cTupel.waggonPos].unitLoads[cTupel.unitLoadPos];
    if (unitLoad != null) {
      unitLoad.correctCheckSum = checksumStatus;
      this.saveTrainToLocalStorage(train);
    }
  }

  getCoordinateOfUnitLoadInArray(unitLoadNumber: string): CoordinateTupel {
    let cnt1: number,
      cnt2 = 0;
    let tupel: CoordinateTupel = null;
    const train: trainDTO = this.getTrain();
    for (cnt1 = 0; cnt1 < train.waggon.length; cnt1++) {
      if (cnt2 == -42) {
        break;
      }
      for (cnt2 = 0; cnt2 < train.waggon[cnt1].unitLoads.length; cnt2++) {
        if (
          unitLoadNumber == train.waggon[cnt1].unitLoads[cnt2].unitLoadNumber
        ) {
          tupel = new CoordinateTupel();
          tupel.waggonPos = cnt1;
          tupel.unitLoadPos = cnt2;
          cnt2 = -42;
          break;
        }
      }
    }
    return tupel;
  }

  getUnitLoadNumbersOnWaggonWithName(waggonName: string): string[] {
    const waggon: waggonDTO = this.getWaggon(waggonName);
    const count = waggon.unitLoads.length;
    let cnt: number;
    const retVal = new Array<string>(length);
    for (cnt = 0; cnt < count; cnt++) {
      retVal[cnt] = waggon.unitLoads[cnt].unitLoadNumber;
    }
    return retVal;
  }

  addCommentToUnitLoad(unitLoadNumber: string, comment: string) {
    const train = this.getTrain();
    const cTupel: CoordinateTupel =
      this.getCoordinateOfUnitLoadInArray(unitLoadNumber);
    const unitLoad: unitLoadDTO =
      train.waggon[cTupel.waggonPos].unitLoads[cTupel.unitLoadPos];
    if (unitLoad != null) {
      unitLoad.comment = comment;
      this.saveTrainToLocalStorage(train);
    }
  }

  getCommentByUnitLoadNumber(unitLoadName: string): string {
    let retVal: string = null;
    const unitLoad: unitLoadDTO =
      this.getUnitloadByUnitloadNumber(unitLoadName);
    if (unitLoad != null) {
      retVal = unitLoad.comment;
    }
    return retVal;
  }

  addDamageCommentToUnitLoad(unitLoadNumber: string, comment: string) {
    const train = this.getTrain();
    const cTupel: CoordinateTupel =
      this.getCoordinateOfUnitLoadInArray(unitLoadNumber);
    const unitLoad: unitLoadDTO =
      train.waggon[cTupel.waggonPos].unitLoads[cTupel.unitLoadPos];
    if (unitLoad != null) {
      unitLoad.damageComment = comment;
      this.saveTrainToLocalStorage(train);
    }
  }

  getDamageCommentByUnitLoadNumber(unitLoadName: string): string {
    let retVal: string = null;
    const unitLoad: unitLoadDTO =
      this.getUnitloadByUnitloadNumber(unitLoadName);
    if (unitLoad != null) {
      retVal = unitLoad.damageComment;
    }
    return retVal;
  }

  removeUnitLoadByName(unitLoadNumber: string) {
    const train: trainDTO = this.getTrain();
    const posTupel: CoordinateTupel =
      this.getCoordinateOfUnitLoadInArray(unitLoadNumber);
    train.waggon[posTupel.waggonPos].unitLoads.splice(posTupel.unitLoadPos, 1);
    this.saveTrainToLocalStorage(train);
  }

  /********************************************************************** working with the danger-good-labels */
  setDangergoodLabelToUnitLoad(
    unitLoadNumber: string,
    position: number,
    state: number,
  ): dangerGoodLabelDTO {
    const train = this.getTrain();
    const unitLoad: unitLoadDTO =
      this.getUnitloadByUnitloadNumber(unitLoadNumber);
    let dangerGoodLabel: dangerGoodLabelDTO = null;
    let cnt: number, cnt2: number;
    if (unitLoad.dangerGoodLabels != null) {
      for (cnt = 0; cnt < unitLoad.dangerGoodLabels.length; cnt++) {
        if (unitLoad.dangerGoodLabels[cnt].position == position) {
          dangerGoodLabel = unitLoad.dangerGoodLabels[cnt];
          break;
        }
      }
      if (dangerGoodLabel == null) {
        dangerGoodLabel = new dangerGoodLabelDTO();
        dangerGoodLabel.position = position;
        unitLoad.dangerGoodLabels.push(dangerGoodLabel);
      }
      dangerGoodLabel.statusNumber = state;
      const waggon: waggonDTO =
        this.getWaggonOfUnitLoadByUnitLoadNumber(unitLoadNumber);
      for (cnt = 0; cnt < waggon.unitLoads.length; cnt++) {
        if (waggon.unitLoads[cnt].unitLoadNumber == unitLoadNumber) {
          waggon.unitLoads[cnt] = unitLoad;
          for (cnt2 = 0; cnt2 < train.waggon.length; cnt2++) {
            if (train.waggon[cnt2].waggonNumber == waggon.waggonNumber) {
              train.waggon[cnt2] = waggon;
            }
          }
        }
      }
      this.saveTrainToLocalStorage(train);
    }
    return dangerGoodLabel;
  }

  /*ToDo Muss getestet und wieder eingefügt werden
  setDangergoodStatesToUnitLoad(unitLoadName: string, status: string) {
    if (status.length == 4) {
      const front = parseInt(status.charAt(0));
      const rear = parseInt(status.charAt(1));
      const left = parseInt(status.charAt(2));
      const right = parseInt(status.charAt(3));
      const coord = this.getCoordinateOfUnitLoadInArray(unitLoadName);
      if (coord != null) {
        this.setDangergoodLabelToUnitLoad(unitLoadName, dangerGoodLabelDTO.positionRight, right);
        this.setDangergoodLabelToUnitLoad(unitLoadName, dangerGoodLabelDTO.positionFront, front);
        this.setDangergoodLabelToUnitLoad(unitLoadName, dangerGoodLabelDTO.positionRear, rear);
        this.setDangergoodLabelToUnitLoad(unitLoadName, dangerGoodLabelDTO.positionLeft, left);
      }
    }
  }*/

  getDangergoodStatesFromUnitLoadAsString(unitLoadName: string): string {
    let retVal = null;
    let front = 0,
      rear = 0,
      left = 0,
      right = 0;
    const coord = this.getCoordinateOfUnitLoadInArray(unitLoadName);
    if (coord != null) {
      const unitLoad: unitLoadDTO =
        this.getTrain().waggon[coord.waggonPos].unitLoads[coord.unitLoadPos];
      if (unitLoad.dangerGoodLabels != null) {
        let cnt: number;
        for (cnt = 0; cnt < unitLoad.dangerGoodLabels.length; cnt++) {
          const dangerGood: dangerGoodLabelDTO = unitLoad.dangerGoodLabels[cnt];
          if (dangerGood.position == dangerGoodLabelDTO.positionFront)
            front = dangerGood.statusNumber;
          if (dangerGood.position == dangerGoodLabelDTO.positionLeft)
            left = dangerGood.statusNumber;
          if (dangerGood.position == dangerGoodLabelDTO.positionRear)
            rear = dangerGood.statusNumber;
          if (dangerGood.position == dangerGoodLabelDTO.positionRight)
            right = dangerGood.statusNumber;
        }
      }
      retVal = '' + front + '' + rear + '' + left + '' + right + '';
    }
    return retVal;
  }

  /********************************************************************** playing with seals */
  createSigillum(sigillumTypus: string, sigillumNumber: string): sealDTO {
    const retVal = new sealDTO();
    retVal.sealNumber = sigillumNumber;
    retVal.sealTypus = sigillumTypus;
    return retVal;
  }

  addSigillumToUnitLoad(unitLoadNumber: string, seal: sealDTO) {
    if (seal != null) {
      const train = this.getTrain();
      let testFlag = true;
      const coord: CoordinateTupel =
        this.getCoordinateOfUnitLoadInArray(unitLoadNumber);
      const unitLoad = train.waggon[coord.waggonPos].unitLoads[coord.unitLoadPos];
      if (unitLoad.seals == null) {
        unitLoad.seals = new Array<sealDTO>();
      }
      const seals: sealDTO[] = unitLoad.seals;
      let i: number;
      for (i = 0; i < seals.length; i++) {
        if (
          seals[i].sealNumber == seal.sealNumber &&
          seals[i].sealTypus == seal.sealTypus
        ) {
          testFlag = false;
          break;
        }
      }
      if (testFlag == true) {
        unitLoad.seals.push(seal);
        this.saveTrainToLocalStorage(train);
      }
    }
  }
  getDangerGoodLabelsFromUnitLoad(
    unitLoadNumber: string,
  ): dangerGoodLabelDTO[] {
    return this.getUnitloadByUnitloadNumber(unitLoadNumber).dangerGoodLabels;
  }
  getSigillumsFromUnitLoad(unitLoadNumber: string): sealDTO[] {
    return this.getUnitloadByUnitloadNumber(unitLoadNumber).seals;
  }

  /* ToDo: insert and test when used
  countSigillumsAtUnitLoad(unitLoadNumber: string): number {
    let retVal = 0;
    const seals: sealDTO[] = this.getSigillumsFromUnitLoad(unitLoadNumber);
    if (seals != null) {
      retVal = seals.length;
    }
    return retVal;
  }

  getSigillumAtPosition(unitLoadNumber: string,pos : number) : sealDTO {
    const train: trainDTO = this.getTrain();
    const coord: CoordinateTupel = this.getCoordinateOfUnitLoadInArray(unitLoadNumber);
    const unitLoad: unitLoadDTO = train.waggon[coord.waggonPos].unitLoads[coord.unitLoadPos];
    return unitLoad.seals[pos];
  }

  existSealInSealsAtUnitLoad(unitLoadNumber: string,seal : sealDTO) : boolean {
    let retVal = false;
    const train = this.getTrain();
    const coord = this.getCoordinateOfUnitLoadInArray(unitLoadNumber);
    const unitLoad = train.waggon[coord.waggonPos].unitLoads[coord.unitLoadPos];
    const seals = unitLoad.seals;
    let i: number;
    for (i = 0; i < seals.length; i++) {
      if (seals[i].sealNumber == seal.sealNumber && seals[i].sealTypus == seal.sealTypus) {
        retVal = true;
        break;
      }
    }
    return retVal;
  }*/

  setSigillumsToUnitLoad(unitLoadNumber: string, seals: sealDTO[]) {
    const train: trainDTO = this.getTrain();
    const coord = this.getCoordinateOfUnitLoadInArray(unitLoadNumber);
    const unitLoad = train.waggon[coord.waggonPos].unitLoads[coord.unitLoadPos];
    unitLoad.seals = seals;
    this.saveTrainToLocalStorage(train);
  }

  /********************************************************************** have other helpfull utilities
  printInfo(text: string) {
    console.log('trainStorage: ' + text);
  }*/
}
