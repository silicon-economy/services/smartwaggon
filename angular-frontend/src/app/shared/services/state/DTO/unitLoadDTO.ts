/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { dangerGoodLabelDTO } from './dangerGoodLabelDTO';
import { sealDTO } from './sealDTO';

export class unitLoadDTO {
  unitLoadNumber: string;
  creationDate: Date;
  modificationDate: Date;
  dangerGoodLabels: dangerGoodLabelDTO[];
  seals: sealDTO[];
  comment: string;
  damageComment: string;
  correctCheckSum: boolean;
}
