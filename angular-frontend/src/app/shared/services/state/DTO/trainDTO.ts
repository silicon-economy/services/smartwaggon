/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { waggonDTO } from './waggonDTO';

export class trainDTO {
  rail_id: string;
  date_arrival: Date;
  date_departure: Date;
  source: string;
  destination: string;
  creationDate: Date;
  modificationDate: Date;
  waggon: waggonDTO[];
}
