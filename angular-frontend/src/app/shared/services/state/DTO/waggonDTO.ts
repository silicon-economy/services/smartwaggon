/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { unitLoadDTO } from './unitLoadDTO';

export class waggonDTO {
  waggonNumber: string;
  waggonPosition: number;
  unitLoads: unitLoadDTO[];
  creationDate: Date;
  modificationDate: Date;
  comment: string;
  damageComment: string;
  correctCheckSum: boolean;
}
