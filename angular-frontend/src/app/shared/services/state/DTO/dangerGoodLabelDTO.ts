/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class dangerGoodLabelDTO {
  static readonly positionLeft = 1;
  static readonly positionRight = 2;
  static readonly positionFront = 4;
  static readonly positionRear = 8;

  static readonly statusUndefined = 0;
  static readonly statusReadable = 1;
  static readonly statusBlock = 2;
  static readonly statusUnvisible = 3;

  creationDate: Date;
  modificationDate: Date;
  position: number;
  statusNumber: number;
  statusStr: string;
}
