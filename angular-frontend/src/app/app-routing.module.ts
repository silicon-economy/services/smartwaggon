/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingsPageComponent } from './pages/settings-page/settings-page.component';
import { ErrorPageComponent } from './pages/error-page/error-page.component';
import { ImprintLegalPageComponent } from './pages/imprint-legal-page/imprint-legal-page.component';
import { PrivacyPageComponent } from './pages/privacy-page/privacy-page.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { WaggonOverviewComponent } from './pages/waggon-overview/waggon-overview.component';
import { WaggonCaptureComponent } from './pages/waggon-capture/waggon-capture.component';
import { DangergoodlabelCaptureComponent } from './pages/dangergoodlabel-capture/dangergoodlabel-capture.component';
import { LoadingUnitComponent } from './pages/loadingUnit-capture/loading-unit.component';
import { CommentPageComponent } from './pages/comment-page/comment-page.component';
import { DamageCapturePageComponent } from './pages/damage-capture-page/damage-capture-page.component';
import { CheckSigillumComponent } from './pages/check-sigillum/check-sigillum.component';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'start' },
  { path: 'start', component: LandingPageComponent },
  { path: 'privacy', component: PrivacyPageComponent },
  { path: 'imprint-legal', component: ImprintLegalPageComponent },
  { path: 'settings', component: SettingsPageComponent },
  { path: 'waggon-overview', component: WaggonOverviewComponent },
  { path: 'waggon-capture', component: WaggonCaptureComponent },
  { path: 'loadingUnit-capture', component: LoadingUnitComponent },
  {
    path: 'dangergoodlabel-capture',
    component: DangergoodlabelCaptureComponent,
  },
  { path: 'comment', component: CommentPageComponent },
  { path: 'damage-capture', component: DamageCapturePageComponent },
  { path: 'check-sigillum', component: CheckSigillumComponent },
  { path: '**', component: ErrorPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
