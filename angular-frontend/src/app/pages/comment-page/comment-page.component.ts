/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { TrainStorageService } from '../../shared/services/state/train-storage.service';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { WaggonService } from '../../shared/services/http/waggon.service';
import { LoadingUnitService } from '../../shared/services/http/loading-unit.service';

@Component({
  selector: 'app-comment-page',
  templateUrl: './comment-page.component.html',
  styleUrls: ['./comment-page.component.scss'],
})
export class CommentPageComponent implements OnInit {
  @Input() number: string;
  @Input() showWaggonNumber: boolean; // True if the waggon-icon should be shown. False if the loading-unit-icon should be shown
  @Input() waggonPositionNumber: number;

  commentInput: string;
  showSaveButton = false;

  constructor(
    private loadingUnitService: LoadingUnitService,
    private waggonService: WaggonService,
    private iconRegistry: MatIconRegistry,
    private domSanitzer: DomSanitizer,
    private router: Router,
    private route: ActivatedRoute,
    private trainStorage: TrainStorageService,
  ) {
    this.iconRegistry.addSvgIcon(
      'loading-Unit-Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/loading-Unit-Icon.svg',
      ),
    );
    this.iconRegistry.addSvgIcon(
      'waggon-Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/waggon_icon.svg',
      ),
    );
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.number = params['number'];
      this.showWaggonNumber = coerceBooleanProperty(params['showWaggonNumber']);
      this.waggonPositionNumber = Number(params['waggonPositionNumber']);
    });
    if (this.showWaggonNumber) {
      this.commentInput = this.trainStorage.getCommentByWaggonName(this.number);
    } else {
      this.commentInput = this.trainStorage.getCommentByUnitLoadNumber(
        this.number,
      );
    }
    this.onKey();
  }

  commit() {
    this.setNewTextToTrainStorage(this.commentInput);
    this.router.navigate(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: this.waggonPositionNumber },
    });
  }

  discard() {
    this.setNewTextToTrainStorage('');
    this.router.navigate(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: this.waggonPositionNumber },
    });
  }

  // Sets the text of the InputField depending on whether it is a loading unit comment or a waggon comment.
  setNewTextToTrainStorage(text: string) {
    if (this.showWaggonNumber) {
      this.trainStorage.addCommentToWaggon(this.number, text);
      this.saveWaggonComment(this.number, text);
    } else {
      this.trainStorage.addCommentToUnitLoad(this.number, text);
      this.saveLoadingUnitComment(this.number, text);
    }
  }

  onKey() {
    if (this.commentInput) {
      this.showSaveButton = this.commentInput.length > 0;
    }
  }
  saveLoadingUnitComment(loadingUnitNumber: string, comment: string) {
    this.loadingUnitService.addComment(loadingUnitNumber, comment).subscribe({
      next: () => {
        console.log('Waggon comment saved successfully');
      },
      error: (error) => {
        console.error('Error saving waggon comment:', error);
      },
    });
  }
  saveWaggonComment(waggonNumber: string, comment: string) {
    this.waggonService.addComment(waggonNumber, comment).subscribe({
      next: () => {
        console.log('Waggon comment saved successfully');
      },
      error: (error) => {
        console.error('Error saving waggon comment:', error);
      },
    });
  }
}
