/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentPageComponent } from './comment-page.component';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { WaggonOverviewComponent } from '../waggon-overview/waggon-overview.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';
import { TrainStorageService } from '../../shared/services/state/train-storage.service';
import { expect } from '@angular/flex-layout/_private-utils/testing';
import { LoadingUnitService } from '../../shared/services/http/loading-unit.service';
import { WaggonService } from '../../shared/services/http/waggon.service';

describe('CommentPageComponent', () => {
  let component: CommentPageComponent;
  let fixture: ComponentFixture<CommentPageComponent>;
  let router: Router;
  let trainStorageServiceSpy: jasmine.SpyObj<TrainStorageService>;
  let loadingUnitServiceSpy: jasmine.SpyObj<LoadingUnitService>;
  let waggonServiceSpy: jasmine.SpyObj<WaggonService>;

  const paramsSubject = new BehaviorSubject({
    number: '23 12 312 3 123 - 5',
    showWaggonNumber: 'false',
    waggonPositionNumber: '2',
  });

  beforeEach(async () => {
    loadingUnitServiceSpy = jasmine.createSpyObj('LoadingUnitService', [
      'addComment',
    ]);
    loadingUnitServiceSpy.addComment.and.returnValue(of(null));
    waggonServiceSpy = jasmine.createSpyObj('WaggonService', ['addComment']);
    waggonServiceSpy.addComment.and.returnValue(of(null));
    trainStorageServiceSpy = jasmine.createSpyObj('TrainStorageService', [
      'getCommentByWaggonName',
      'getCommentByUnitLoadNumber',
      'addCommentToUnitLoad',
      'addCommentToWaggon',
    ]);
    await TestBed.configureTestingModule({
      declarations: [CommentPageComponent],
      imports: [
        BrowserModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        HttpClientModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes([
          { path: 'waggon-overview', component: WaggonOverviewComponent },
        ]),
        TranslateTestingModule.withTranslations('de', {}),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            queryParams: of(paramsSubject),
          },
        },
        { provide: TrainStorageService, useValue: trainStorageServiceSpy },
        { provide: LoadingUnitService, useValue: loadingUnitServiceSpy },
        { provide: WaggonService, useValue: waggonServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CommentPageComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('discard', () => {
    it('should clear the comment for a waggon number and navigate to the waggon overview', () => {
      spyOn(router, 'navigate');
      trainStorageServiceSpy.addCommentToWaggon.and.callThrough();
      component.commentInput = 'Test comment';
      component.waggonPositionNumber = 1;
      component.number = '23 12 312 3 123 - 5';
      component.showWaggonNumber = true;
      component.discard();
      expect(trainStorageServiceSpy.addCommentToWaggon).toHaveBeenCalledWith(
        component.number,
        '',
      );
      expect(router.navigate).toHaveBeenCalledWith(['/waggon-overview'], {
        queryParams: { waggonPositionNumber: 1 },
      });
    });

    it('should clear the comment for a loading unit and navigate to the waggon overview', () => {
      spyOn(router, 'navigate');
      trainStorageServiceSpy.addCommentToUnitLoad.and.callThrough();
      component.commentInput = 'Test comment';
      component.waggonPositionNumber = 1;
      component.number = 'ABCD1234560';
      component.showWaggonNumber = false;
      component.discard();
      expect(trainStorageServiceSpy.addCommentToUnitLoad).toHaveBeenCalledWith(
        component.number,
        '',
      );
      expect(router.navigate).toHaveBeenCalledWith(['/waggon-overview'], {
        queryParams: { waggonPositionNumber: 1 },
      });
    });
  });

  describe('setNewTextToTrainStorage', () => {
    it('should add the comment to the waggon if showWaggonNumber is true', () => {
      trainStorageServiceSpy.addCommentToWaggon.and.callThrough();
      component.number = '23 12 312 3 123 - 5';
      component.showWaggonNumber = true;
      component.setNewTextToTrainStorage('Test comment');
      expect(trainStorageServiceSpy.addCommentToWaggon).toHaveBeenCalledWith(
        component.number,
        'Test comment',
      );
    });

    it('should add the comment to the unit load if showWaggonNumber is false', () => {
      trainStorageServiceSpy.addCommentToUnitLoad.and.callThrough();
      component.number = 'ABCD1234560';
      component.showWaggonNumber = false;
      component.setNewTextToTrainStorage('Test comment');
      expect(trainStorageServiceSpy.addCommentToUnitLoad).toHaveBeenCalledWith(
        component.number,
        'Test comment',
      );
    });
  });

  it('should test the enabling of the save-button when something is inserted in the textarea', () => {
    component.showSaveButton = false;
    expect(component.showSaveButton).toBeFalse();
    component.commentInput = '';
    component.onKey();
    expect(component.showSaveButton).toBeFalse();
    component.commentInput = 'Test comment';
    component.onKey();
    expect(component.showSaveButton).toBeTruthy();
  });

  it('should test the commit functionality', () => {
    spyOn(component, 'setNewTextToTrainStorage');
    spyOn(router, 'navigate');
    component.waggonPositionNumber = 1;
    component.commentInput = 'Test';
    component.commit();
    expect(component.setNewTextToTrainStorage).toHaveBeenCalledWith(
      component.commentInput,
    );
    expect(router.navigate).toHaveBeenCalledWith(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: 1 },
    });
  });

  it('should test other query params', function () {
    paramsSubject.next({
      number: '23 12 312 3 123 - 5',
      showWaggonNumber: 'true',
      waggonPositionNumber: '2',
    });
    component.ngOnInit();
    expect(component.showWaggonNumber).toBeFalse();
  });

  it('should call addComment for loading units', () => {
    const loadingUnitNumber = '123';
    const comment = 'test comment';

    component.saveLoadingUnitComment(loadingUnitNumber, comment);

    expect(loadingUnitServiceSpy.addComment).toHaveBeenCalledWith(
      loadingUnitNumber,
      comment,
    );
  });

  it('should call addComment for Waggon', () => {
    const waggonNumber = '456';
    const comment = 'test comment';

    component.saveWaggonComment(waggonNumber, comment);

    expect(waggonServiceSpy.addComment).toHaveBeenCalledWith(
      waggonNumber,
      comment,
    );
  });
});
