/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { BrowserModule, By } from '@angular/platform-browser';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { WaggonOverviewComponent } from './waggon-overview.component';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { expect } from '@angular/flex-layout/_private-utils/testing';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { waggonDTO } from '../../shared/services/state/DTO/waggonDTO';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { DeleteWaggonWithLoadingUnitExceptionDialogComponent } from '../delete-waggon-with-loading-unit-exception-dialog/delete-waggon-with-loading-unit-exception-dialog.component';
import { WaggonCaptureComponent } from '../waggon-capture/waggon-capture.component';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { unitLoadDTO } from '../../shared/services/state/DTO/unitLoadDTO';

describe('WaggonOverviewComponent', () => {
  let component: WaggonOverviewComponent;
  let fixture: ComponentFixture<WaggonOverviewComponent>;
  let snackService: SnackbarService;
  let dialog: MatDialog;

  const paramsSubject = new BehaviorSubject({
    waggonPositionNumber: null,
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WaggonOverviewComponent, WaggonCaptureComponent],

      imports: [
        FormsModule,
        TextMaskModule,
        BrowserModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        HttpClientModule,
        MatCardModule,
        RouterTestingModule,
        NoopAnimationsModule,
        MatDialogModule,
        TranslateTestingModule.withTranslations('de', {}),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            queryParams: paramsSubject.asObservable(),
          },
        },
        MatDialog,
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(WaggonOverviewComponent);
    snackService = TestBed.get(SnackbarService);
    component = fixture.componentInstance;
    dialog = TestBed.inject(MatDialog);
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // test to check if waggon number is right
  it('should have a waggon position number', () => {
    expect(component.waggonPositionNumber).toBe(1);
  });

  describe('dialog on deleting waggon with loading units', () => {
    it('should open delete waggon dialog and delete waggon on confirmation', () => {
      component.loadingUnitCounter = 2;
      spyOn(dialog, 'open').and.returnValue({
        afterClosed: () => of(true),
      } as MatDialogRef<DeleteWaggonWithLoadingUnitExceptionDialogComponent>);
      spyOn(component, 'deleteWaggon');
      component.openDeleteWaggonDialog();
      expect(dialog.open).toHaveBeenCalled();
      expect(component.deleteWaggon).toHaveBeenCalled();
    });

    it('should open the delete waggon dialog and abort the delete waggon process', () => {
      component.loadingUnitCounter = 2;
      spyOn(dialog, 'open').and.returnValue({
        afterClosed: () => of(false),
      } as MatDialogRef<DeleteWaggonWithLoadingUnitExceptionDialogComponent>);
      spyOn(component, 'deleteWaggon');
      component.openDeleteWaggonDialog();
      expect(dialog.open).toHaveBeenCalled();
      expect(component.deleteWaggon).not.toHaveBeenCalled();
    });

    it('should not open delete waggon dialog and delete the waggon', () => {
      component.loadingUnitCounter = 0;
      component.formattedWaggonNumber = 'W001';
      spyOn(dialog, 'open').and.returnValue({
        afterClosed: () => of(true),
      } as MatDialogRef<DeleteWaggonWithLoadingUnitExceptionDialogComponent>);
      spyOn(component, 'deleteWaggon');
      component.openDeleteWaggonDialog();
      expect(dialog.open).not.toHaveBeenCalled();
      expect(component.deleteWaggon).toHaveBeenCalled();
    });
  });

  // test for the title
  it('should have a title', () => {
    expect(component.title).toBe('waggon.overview');
    // we are accessing the "h1" by its css
    const title = fixture.debugElement.query(By.css('h1')).nativeElement;
    expect(title.innerHTML).toBe('waggon.overview');
  });

  // test for the title
  it('should change title', () => {
    expect(component.title).toBe('waggon.overview');
    component.changeTitle('test');
    expect(component.title).toBe('test');
  });

  // Test for waggon capture
  it('should capture the waggon number', () => {
    expect(component.title).toBe('waggon.overview');
  });

  it('should test the change of the WaggonCheckSumFlag', function () {
    component.waggonCheckSumIsOkay = true;
    component.changeWaggonCheckSumFlag(false);
    expect(component.waggonCheckSumIsOkay).toBeFalsy();
    component.changeWaggonCheckSumFlag(true);
    expect(component.waggonCheckSumIsOkay).toBeTruthy();
  });

  it('should test the change of the UnitLoadCheckSumFlag', function () {
    component.actualUnitLoadNumberCheckSumIsOkay = true;
    component.changeLoadigUnitCheckSumFlag(false);
    expect(component.actualUnitLoadNumberCheckSumIsOkay).toBeFalsy();
    component.changeLoadigUnitCheckSumFlag(true);
    expect(component.actualUnitLoadNumberCheckSumIsOkay).toBeTruthy();
  });

  // Test for adding Loading unit component
  it('should show the load unit component', () => {
    component.loadingUnitCounter = 0;
    spyOn(component.trainStorage, 'setCheckSumToUnitLoad');
    component.addLoadingUnit('DSCS1234562');
    expect(component.loadingUnitCounter).toBe(1);
    component.addLoadingUnit('ACDA1234566');
    expect(component.loadingUnitCounter).toBe(2);
    component.addLoadingUnit('ABCD1234560');
    expect(component.loadingUnitCounter).toBe(3);
    component.addLoadingUnit('QWER1234564');
    expect(component.loadingUnitCounter).toBe(4);
    expect(component.listOfLoadingUnitNumbers).toEqual([
      'DSCS1234562',
      'ACDA1234566',
      'ABCD1234560',
      'QWER1234564',
    ]);
    expect(component.trainStorage.setCheckSumToUnitLoad).toHaveBeenCalled();
  });

  // Tests if the waggon capture function keys are working
  it('should enable the waggon capture function keys', () => {
    expect(component.hasValidWaggonNumber).toBeFalsy();
    expect(component.formattedWaggonNumber).toBe('');
    component.showValidWaggonNumber('12 39 812 3 785 - 5');
    expect(component.hasValidWaggonNumber).toBeTruthy();
    expect(component.formattedWaggonNumber).toBe('12 39 812 3 785 - 5');
  });

  it(
    'should set showLoadingUnits[loadingUnitCounter]' +
      ' to true and waitForLoadingUnitNumber to true when hasValidWaggonNumber is true',
    () => {
      component.hasValidWaggonNumber = true;
      component.waitForLoadingUnitNumber = false;
      component.newLoadingUnitCapture();
      expect(
        component.showLoadingUnits[component.loadingUnitCounter],
      ).toBeTruthy();
      expect(component.waitForLoadingUnitNumber).toBeTruthy();
    },
  );

  it(
    'should call snackService.handleDefaultErrorsWithText with ' +
      '"loadingUnit.captureLoadingUnitWarning" when loadingUnitCounter is less than 4',
    () => {
      spyOn(snackService, 'handleDefaultErrorsWithText');
      component.hasValidWaggonNumber = true;
      component.waitForLoadingUnitNumber = true;
      component.loadingUnitCounter = 3;
      component.newLoadingUnitCapture();
      component.newLoadingUnitCapture();
      expect(snackService.handleDefaultErrorsWithText).toHaveBeenCalledWith(
        'loadingUnit.captureLoadingUnitWarning',
        'error',
      );
    },
  );

  it('should call snackService.handleDefaultErrorsWithText with "loadingUnit.enoughLoadingUnits" when loadingUnitCounter is 4', () => {
    spyOn(snackService, 'handleDefaultErrorsWithText');
    component.hasValidWaggonNumber = true;
    component.waitForLoadingUnitNumber = true;
    component.loadingUnitCounter = 4;
    component.newLoadingUnitCapture();
    expect(snackService.handleDefaultErrorsWithText).toHaveBeenCalledWith(
      'loadingUnit.enoughLoadingUnits',
      'error',
    );
  });

  it('should call snackService.handleDefaultErrorsWithText with "loadingUnit.buttonDisabled" when hasValidWaggonNumber is false', () => {
    spyOn(snackService, 'handleDefaultErrorsWithText');
    component.hasValidWaggonNumber = false;
    component.newLoadingUnitCapture();
    expect(snackService.handleDefaultErrorsWithText).toHaveBeenCalledWith(
      'loadingUnit.buttonDisabled',
      'error',
    );
  });

  // Tests if the add waggon  function key is working
  it('should add a new waggon but no waggon number is captured', () => {
    spyOn(snackService, 'handleDefaultErrorsWithText');
    component.hasValidWaggonNumber = false;
    component.addWaggon();
    expect(snackService.handleDefaultErrorsWithText).toHaveBeenCalledWith(
      'loadingUnit.buttonDisabled',
      'error',
    );
  });

  // Tests if the add waggon  function key is working
  it('should add a new waggon', () => {
    component.waggonPositionNumber = 1;
    const a: number = component.waggonPositionNumber;
    component.hasValidWaggonNumber = true;
    component.addWaggon();
    expect(component.listOfLoadingUnitNumbers).toEqual([]);
    expect(component.showLoadingUnits).toEqual([false, false, false, false]);
    expect(component.loadingUnitCounter).toEqual(0);
    expect(component.waggonPositionNumber).toEqual(a + 1);
  });

  it('should show valid waggon number', () => {
    spyOn(component, 'showValidWaggonNumber');
    component.buildOverviewWithInformations('1234', []);
    expect(component.showValidWaggonNumber).toHaveBeenCalledWith('1234');
  });

  it('should show loading units', () => {
    const loadingUnits = ['LU1', 'LU2'];
    component.buildOverviewWithInformations('1234', loadingUnits);
    expect(component.showLoadingUnits).toEqual([true, true, false, false]);
    expect(component.listOfLoadingUnitNumbers).toEqual(loadingUnits);
    expect(component.loadingUnitCounter).toBe(2);
  });

  it('should not show loading units when null', () => {
    component.buildOverviewWithInformations('1234', null);
    expect(component.showLoadingUnits).toEqual([false, false, false, false]);
    expect(component.listOfLoadingUnitNumbers).toEqual([]);
    expect(component.loadingUnitCounter).toBe(0);
  });

  it('should test the removeLoadingUnit function', () => {
    const mockUnitLoad: unitLoadDTO = {
      unitLoadNumber: 'ABCD1235',
      creationDate: new Date(),
      modificationDate: new Date(),
      dangerGoodLabels: [],
      seals: [],
      comment: 'Test comment',
      damageComment: 'Test damage comment',
      correctCheckSum: true,
    };
    component.formattedWaggonNumber = '123';
    component.waggonPositionNumber = 1;
    component.listOfLoadingUnitNumbers = ['ABCD1234', 'ABCD1235', 'ABCD1236'];
    spyOn(component.loadingUnitService, 'deleteLoadingUnit').and.returnValue(
      of(null),
    );

    spyOn(component.trainStorage, 'removeUnitLoadByName');
    spyOn(component, 'buildOverviewWithInformations');

    spyOn(
      component.trainStorage,
      'getUnitloadByUnitloadNumber',
    ).and.returnValue(mockUnitLoad);

    component.removeLoadingUnit('ABCD1235');

    expect(component.trainStorage.removeUnitLoadByName).toHaveBeenCalledWith(
      'ABCD1235',
    );
    expect(component.listOfLoadingUnitNumbers).toEqual([
      'ABCD1234',
      'ABCD1236',
    ]);
    expect(component.buildOverviewWithInformations).toHaveBeenCalledWith(
      '123',
      ['ABCD1234', 'ABCD1236'],
    );
  });

  it('should test the onInitFunction without routin parameters', function () {
    component.ngOnInit();
    expect(component.waggonPositionNumber).toEqual(1);
  });

  it('should test the buildOverviewWithActualWaggonPosition()', function () {
    this.waggonPositionNumber = 1;
    const mockWaggonDTO: waggonDTO = {
      waggonNumber: '123',
      waggonPosition: 1,
      unitLoads: null,
      creationDate: null,
      modificationDate: null,
      comment: null,
      correctCheckSum: null,
      damageComment: null,
    };
    spyOn(component.trainStorage, 'getWaggonByPosition').and.returnValue(
      mockWaggonDTO,
    );
    spyOn(
      component.trainStorage,
      'getUnitLoadNumbersOnWaggonWithName',
    ).and.returnValue(['ABCD1234']);
    spyOn(component, 'buildOverviewWithInformations');
    component.buildOverviewWithActualWaggonPosition();
    expect(component.trainStorage.getWaggonByPosition).toHaveBeenCalledWith(1);
    expect(
      component.trainStorage.getUnitLoadNumbersOnWaggonWithName,
    ).toHaveBeenCalledWith('123');
    expect(component.buildOverviewWithInformations).toHaveBeenCalledWith(
      '123',
      ['ABCD1234'],
    );
  });

  it('should test the delete waggon function for waggon number >1', function (done) {
    const mockWaggonDTO: waggonDTO = {
      waggonNumber: '123',
      waggonPosition: null,
      unitLoads: null,
      creationDate: null,
      modificationDate: null,
      comment: null,
      correctCheckSum: null,
      damageComment: null,
    };
    spyOn(component.trainStorage, 'removeWaggonByName');
    spyOn(component.trainStorage, 'getWaggonByPosition').and.returnValue(
      mockWaggonDTO,
    );
    spyOn(
      component.trainStorage,
      'getUnitLoadNumbersOnWaggonWithName',
    ).and.returnValue(['ABCD1234']);
    spyOn(component, 'buildOverviewWithInformations');
    spyOn(component.waggonService, 'deleteWaggon').and.returnValue(of(null));
    component.waggonPositionNumber = 2;
    component.deleteWaggon();
    setTimeout(() => {
      expect(component.waggonPositionNumber).toBe(1);
      expect(component.trainStorage.getWaggonByPosition).toHaveBeenCalledWith(1);
      expect(
        component.trainStorage.getUnitLoadNumbersOnWaggonWithName,
      ).toHaveBeenCalledWith('123');
      expect(component.trainStorage.removeWaggonByName).toHaveBeenCalled();
      expect(component.buildOverviewWithInformations).toHaveBeenCalled();
      done();
    });
  });

  it('should test the delete waggon function for waggon number = 1', function () {
    const mockWaggonDTO: waggonDTO = {
      waggonNumber: '123',
      waggonPosition: null,
      unitLoads: null,
      creationDate: null,
      modificationDate: null,
      comment: null,
      correctCheckSum: null,
      damageComment: null,
    };

    spyOn(component.trainStorage, 'removeWaggonByName');
    spyOn(component.trainStorage, 'getWaggonByPosition').and.returnValue(
      mockWaggonDTO,
    );
    spyOn(component, 'resetLoadingUnits');
    component.hasValidWaggonNumber = true;
    component.formattedWaggonNumber = '123123-5';
    component.deleteWaggon();
    expect(component.trainStorage.removeWaggonByName).toHaveBeenCalledWith(
      '123123-5',
    );
    expect(component.hasValidWaggonNumber).toBeFalsy();
    expect(component.formattedWaggonNumber).toEqual('');
    expect(component.resetLoadingUnits).toHaveBeenCalled();
  });

  it('should test the right click and left click', function () {
    spyOn(component, 'buildOverviewWithActualWaggonPosition');
    component.waggonPositionNumber = 2;
    component.rightClick();
    expect(component.buildOverviewWithActualWaggonPosition).toHaveBeenCalled();
    expect(component.waggonPositionNumber).toBe(3);
    component.leftClick();
    expect(component.buildOverviewWithActualWaggonPosition).toHaveBeenCalled();
    expect(component.waggonPositionNumber).toBe(2);
  });

  it('should test the onInitFunction with routing parameter', function () {
    paramsSubject.next({ waggonPositionNumber: 2 });
    const mockWaggonDTO: waggonDTO = {
      waggonNumber: '123',
      waggonPosition: null,
      unitLoads: null,
      creationDate: null,
      modificationDate: null,
      comment: null,
      correctCheckSum: null,
      damageComment: null,
    };
    spyOn(component.trainStorage, 'getWaggonByPosition').and.returnValue(
      mockWaggonDTO,
    );
    spyOn(
      component.trainStorage,
      'getUnitLoadNumbersOnWaggonWithName',
    ).and.returnValue(['ABCD1234']);
    spyOn(component, 'buildOverviewWithInformations');
    component.ngOnInit();
    expect(component.waggonPositionNumber).toEqual(2);
    expect(component.trainStorage.getWaggonByPosition).toHaveBeenCalledWith(2);
    expect(
      component.trainStorage.getUnitLoadNumbersOnWaggonWithName,
    ).toHaveBeenCalled();
    expect(component.buildOverviewWithInformations).toHaveBeenCalled();
  });
});
