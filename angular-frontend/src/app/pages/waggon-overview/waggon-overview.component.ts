/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { SnackbarService } from 'src/app/shared/services/state/snackbar.service';
import { ActivatedRoute } from '@angular/router';
import { TrainStorageService } from '../../shared/services/state/train-storage.service';
import { waggonDTO } from '../../shared/services/state/DTO/waggonDTO';
import { MatDialog } from '@angular/material/dialog';
import { DeleteWaggonWithLoadingUnitExceptionDialogComponent } from '../delete-waggon-with-loading-unit-exception-dialog/delete-waggon-with-loading-unit-exception-dialog.component';
import { WaggonService } from '../../shared/services/http/waggon.service';
import { LoadingUnitService } from '../../shared/services/http/loading-unit.service';
import { unitLoadDTO } from '../../shared/services/state/DTO/unitLoadDTO';

@Component({
  selector: 'app-waggon-overview',
  templateUrl: './waggon-overview.component.html',
  styleUrls: ['./waggon-overview.component.scss'],
})
export class WaggonOverviewComponent implements OnInit {
  @Input() waggonPositionNumber; //contains the current position of the waggon to be detected

  title = 'waggon.overview';
  hasValidWaggonNumber = false; //True when a valid waggon number is captured in the waggon-capture component
  formattedWaggonNumber = '';
  listOfLoadingUnitNumbers: string[] = []; //Contains the captured loading unit numbers
  showLoadingUnits: boolean[] = [false, false, false, false]; //Contains the information about which load unit component should be rendered
  loadingUnitCounter = 0; // Counts the already captured loading unit numbers
  waitForLoadingUnitNumber = true; //True when waiting for the entering of a loading unit number
  firstTime = true; //True when accessing the disabled button error code the first time in order to prevent it to occur randomly
  waggonCheckSumIsOkay = false;
  actualUnitLoadNumberCheckSumIsOkay = false;

  constructor(
    private iconRegistry: MatIconRegistry,
    private domSanitzer: DomSanitizer,
    private snackService: SnackbarService,
    private route: ActivatedRoute,
    public trainStorage: TrainStorageService,
    private dialog: MatDialog,
    public waggonService: WaggonService,
    public loadingUnitService: LoadingUnitService,
  ) {
    this.registerIcon('add_Waggon_Icon',
      'assets/icons/add_Waggon.svg');
    this.registerIcon('add_LE_Icon',
      'assets/icons/add_LE.svg');
    this.registerIcon('back_Icon',
      'assets/icons/third-party-material-icons/Icon-back.svg');
    this.registerIcon('waggon-icon',
      'assets/icons/waggon_icon.svg');
    this.registerIcon('delete_Icon',
      'assets/icons/third-party-material-icons/delete.svg');
    this.registerIcon('forward_Icon',
      'assets/icons/third-party-material-icons/forward.svg');
    this.registerIcon('backward_Icon',
      'assets/icons/third-party-material-icons/backward.svg');
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.waggonPositionNumber = Number(params['waggonPositionNumber']);
    });

    if (!this.waggonPositionNumber) {
      this.waggonPositionNumber = 1;
    } else {
      this.buildOverviewWithActualWaggonPosition();
    }
  }

  //Register icons
  private registerIcon(iconName:string, value:string):void{
    this.iconRegistry.addSvgIcon(
      iconName,
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        value,
      ),
    );
  }

  //Changes the title of the current page
  changeTitle(newTitle: string) {
    this.title = newTitle;
  }

  //Recreates the waggon overview page by using the waggon number and a loadingUnit list
  buildOverviewWithInformations(waggonNumber: string, loadingUnits: string[]) {
    this.showValidWaggonNumber(waggonNumber);
    if (loadingUnits != null) {
      this.resetLoadingUnits();
      loadingUnits.forEach((element, i) => {
        this.showLoadingUnits[i] = true;
        this.listOfLoadingUnitNumbers.push(element); //this will call the ngOnChanges in the loading-unit component
        this.loadingUnitCounter = i + 1;
      });
    }
  }

  //This function loads the waggon information for the
  buildOverviewWithActualWaggonPosition() {
    const waggon: waggonDTO = this.trainStorage.getWaggonByPosition(
      this.waggonPositionNumber,
    );
    const unitLoads: string[] =
      this.trainStorage.getUnitLoadNumbersOnWaggonWithName(waggon.waggonNumber);
    this.buildOverviewWithInformations(waggon.waggonNumber, unitLoads);
  }

  /*Enables the Field with the Waggon number and the waggon capture function keys.
    This Function should only be called after a valid waggon number is captured.*/
  showValidWaggonNumber(waggonNumber: string) {
    this.formattedWaggonNumber = waggonNumber;
    this.trainStorage.addWaggon(this.formattedWaggonNumber);
    this.trainStorage.setWaggonPosition(
      this.waggonPositionNumber,
      this.formattedWaggonNumber,
    );
    this.trainStorage.setChecksumToWaggon(
      this.waggonCheckSumIsOkay,
      this.formattedWaggonNumber,
    );
    this.hasValidWaggonNumber = true;
    this.waitForLoadingUnitNumber = false;
  }
  saveLoadingUnit(loadingunit: unitLoadDTO) {
    this.loadingUnitService
      .saveLoadingUnit(this.formattedWaggonNumber, loadingunit)
      .subscribe({
        next: () => {
          console.log('loading unit saved successfully');
        },
        error: (error) => {
          console.error('Error saving loading unit:', error);
        },
      });
  }

  /*The addLoadingUnit function is usually called from the loadingUnit component, when a correct loading unit has been captured.
    This Function will add the captured loading unit number to the listOfLoadingUnitNumbers.*/
  addLoadingUnit(loadingUnitNumber) {
    if (this.loadingUnitCounter == 3) {
      this.changeTitle('waggon.overview');
      this.waitForLoadingUnitNumber = true;
    } else {
      this.waitForLoadingUnitNumber = false;
    }
    this.listOfLoadingUnitNumbers.push(loadingUnitNumber);
    this.trainStorage.addUnitLoad(this.formattedWaggonNumber, loadingUnitNumber);
    this.trainStorage.setCheckSumToUnitLoad(
      this.actualUnitLoadNumberCheckSumIsOkay,
      loadingUnitNumber,
    );
    this.loadingUnitCounter++;
    this.saveLoadingUnit(
      this.trainStorage.getUnitloadByUnitloadNumber(loadingUnitNumber),
    );
  }

  /*On click function, when add loading unit button is clicked and a valid waggon number is given,
    then it will show the addLoadingUnit interface. This function will handle the default
    errors on the disabled addLoadingUnitButton and gives an error related feedback*/
  newLoadingUnitCapture() {
    if (
      this.hasValidWaggonNumber &&
      !this.showLoadingUnits[this.loadingUnitCounter] &&
      !this.waitForLoadingUnitNumber
    ) {
      this.showLoadingUnits[this.loadingUnitCounter] = true;
      this.waitForLoadingUnitNumber = true;
      this.firstTime = true;
      this.changeTitle('waggon.loadingUnit');
    } else {
      if (this.waitForLoadingUnitNumber && this.hasValidWaggonNumber) {
        if (this.loadingUnitCounter == 4) {
          this.snackService.handleDefaultErrorsWithText(
            'loadingUnit.enoughLoadingUnits',
            'error',
          );
        } else {
          if (this.firstTime) {
            this.firstTime = false;
            return;
          } else {
            this.snackService.handleDefaultErrorsWithText(
              'loadingUnit.captureLoadingUnitWarning',
              'error',
            );
          }
        }
      } else {
        this.snackService.handleDefaultErrorsWithText(
          'loadingUnit.buttonDisabled',
          'error',
        );
      }
    }
  }

  //On Click function, when adding a waggon, it resets the waggon overview interface and the new added waggon could be added.
  addWaggon() {
    if (this.hasValidWaggonNumber === true) {
      this.listOfLoadingUnitNumbers = [];
      this.showLoadingUnits = [false, false, false, false];
      this.hasValidWaggonNumber = false;
      this.loadingUnitCounter = 0;
      this.waggonPositionNumber++;
    } else {
      this.snackService.handleDefaultErrorsWithText(
        'loadingUnit.buttonDisabled',
        'error',
      );
    }
  }

  removeLoadingUnit(loadingUnitNumber) {
    const unitLoad =
      this.trainStorage.getWaggonOfUnitLoadByUnitLoadNumber(loadingUnitNumber);
    if (unitLoad) {
      this.loadingUnitService
        .deleteLoadingUnit(
          this.trainStorage.getUnitloadByUnitloadNumber(loadingUnitNumber)
            .unitLoadNumber,
          this.trainStorage.getWaggon(this.formattedWaggonNumber).waggonNumber,
        )
        .subscribe(
          () => {
            console.log('loading unit deleted successfully');
          },
          () => {
            console.error("loadingUnit couldn't be deleted : $(error)");
          },
        );
    }
    this.trainStorage.removeUnitLoadByName(loadingUnitNumber);
    this.listOfLoadingUnitNumbers = this.listOfLoadingUnitNumbers.filter(
      (loadingUnit) => loadingUnit !== loadingUnitNumber,
    );
    this.buildOverviewWithInformations(
      this.formattedWaggonNumber,
      this.listOfLoadingUnitNumbers,
    );
  }

  resetLoadingUnits() {
    this.listOfLoadingUnitNumbers = [];
    this.showLoadingUnits = [false, false, false, false];
    this.loadingUnitCounter = 0;
  }

  openDeleteWaggonDialog() {
    if (this.loadingUnitCounter > 0) {
      const dialogRef = this.dialog.open(
        DeleteWaggonWithLoadingUnitExceptionDialogComponent,
        {},
      );

      dialogRef.afterClosed().subscribe((status: boolean) => {
        if (status) {
          this.deleteWaggon();
        }
      });
    } else {
      this.deleteWaggon();
    }
  }

  deleteWaggon() {
    this.waggonService
      .deleteWaggon(
        this.trainStorage.getWaggonByPosition(this.waggonPositionNumber)
          .waggonNumber,
      )
      .subscribe(
        () => {
          console.log('Waggon deleted successfully');
        },
        () => {
          console.error("waggon couldn't be deleted : $(error)");
        },
      );
    this.trainStorage.removeWaggonByName(this.formattedWaggonNumber);
    if (this.waggonPositionNumber > 1) {
      this.waggonPositionNumber--;
      const waggon: waggonDTO = this.trainStorage.getWaggonByPosition(
        this.waggonPositionNumber,
      );
      const unitLoads: string[] =
        this.trainStorage.getUnitLoadNumbersOnWaggonWithName(waggon.waggonNumber);
      this.buildOverviewWithInformations(waggon.waggonNumber, unitLoads);
    } else {
      this.hasValidWaggonNumber = false;
      this.formattedWaggonNumber = '';
      this.resetLoadingUnits();
    }
  }

  changeWaggonCheckSumFlag(checkSumIsOkay: boolean) {
    this.waggonCheckSumIsOkay = checkSumIsOkay;
  }

  changeLoadigUnitCheckSumFlag(checkSumIsOkay: boolean) {
    this.actualUnitLoadNumberCheckSumIsOkay = checkSumIsOkay;
  }

  rightClick() {
    this.waggonPositionNumber++;
    this.buildOverviewWithActualWaggonPosition();
  }

  leftClick() {
    this.waggonPositionNumber--;
    this.buildOverviewWithActualWaggonPosition();
  }
}
