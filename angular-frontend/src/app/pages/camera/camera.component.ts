/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { WebcamUtil, WebcamInitError, WebcamImage } from 'ngx-webcam';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.scss'],
})
export class CameraComponent implements OnInit {
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public errors: WebcamInitError[] = [];
  public videoOptions: MediaTrackConstraints = {
    facingMode: { ideal: 'enviroment' },
    width: { ideal: 1024 },
    height: { ideal: 768 },
  };
  public webcamImage: WebcamImage = null; //contains the picture
  trigger: Subject<void> = new Subject<void>(); // webcam snapshot trigger
  nextWebcam: Subject<boolean | string> = new Subject<boolean | string>(); // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  webcamWidth: number;
  webcamHeight: number;
  mode: string; //LE: Loading Unit Mode, WG: Waggon Mode, SE = Plombenmode, DG = Dangergood, null returns ''

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<CameraComponent>,
    iconRegistry: MatIconRegistry,
    private domSanitzer: DomSanitizer,
    private snackService: SnackbarService,
  ) {
    iconRegistry.addSvgIcon(
      'camera_icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/add_a_photo.svg',
      ),
    );
    iconRegistry.addSvgIcon(
      'camera_switch_icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/flip_camera.svg',
      ),
    );
    iconRegistry.addSvgIcon(
      'abort_icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/cross.svg',
      ),
    );
    this.webcamWidth = data.width;
    this.webcamHeight = data.height;
    this.mode = data.mode;
    dialogRef.disableClose = true;
  }

  //Triggers a snapshot and emits the picture to the backend for receiving the detected number
  // or an empty string when the number could not be detected.
  public triggerSnapshot(): void {
    this.trigger.next();
    //this.webcameImage contains the actual picture
    switch (this.mode) {
      case 'WG':
        //ToDo: Call pythonbackend for waggon number detection and receive string to return
        console.log('WG-Detection');
        this.dialogRef.close('12345678910'); //Dummy
        break;
      case 'LE':
        //ToDo: Call pythonbackend for loadingunit detection and receive string to return
        console.log('LE-Detection');
        this.dialogRef.close('ABCD1234561'); // Dummy
        break;
      case 'SE':
        //ToDo: Call pythonbackend for seal detection and receive string to return
        console.log('SE-Detection');
        this.dialogRef.close('123456'); // Dummy
        break;
      case 'DG':
        //ToDo: Call Pythonbackend for danger good detection and receive string to return
        console.log('DG-Detection');
        this.dialogRef.close(''); // Dummy
        break;
      default:
        console.log('Default callback');
        this.dialogRef.close(''); // Dummy
    }
  }

  //Handles initialisation errors, that may occur when no camera device is detected
  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
    this.snackService.handleDefaultErrorsWithText(error.message, 'error');
    this.dialogRef.close('');
  }

  //Switches to the next camera device
  public showNextWebcam(directionOrDeviceId: boolean | string): void {
    // true: move forward through devices
    // false: move backwards through devices
    // string: move to device with given deviceId
    this.nextWebcam.next(directionOrDeviceId);
  }

  //Handles the image
  public handleImage(webcamImage: WebcamImage): void {
    this.webcamImage = webcamImage;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean | string> {
    return this.nextWebcam.asObservable();
  }

  ngOnInit(): void {
    WebcamUtil.getAvailableVideoInputs().then(
      (mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
      },
    );
  }

  abort() {
    this.dialogRef.close('');
  }
}
