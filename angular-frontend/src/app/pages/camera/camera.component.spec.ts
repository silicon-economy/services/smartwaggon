/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CameraComponent } from './camera.component';
import { WebcamImage, WebcamInitError } from 'ngx-webcam';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

describe('CameraComponent', () => {
  let component: CameraComponent;
  let fixture: ComponentFixture<CameraComponent>;
  let snackService: SnackbarService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CameraComponent],
      imports: [TranslateTestingModule.withTranslations('de', {})],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CameraComponent);
    snackService = TestBed.get(SnackbarService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  //ToDo test the python backend calls when there are implemented
  describe('trigger snapshot', () => {
    it('should trigger a snapshot and call the WG detection', () => {
      component.mode = 'WG';
      spyOn(component.trigger, 'next');
      const dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);
      component.dialogRef = dialogRefSpy;
      component.triggerSnapshot();
      expect(component.trigger.next).toHaveBeenCalled();
      expect(dialogRefSpy.close).toHaveBeenCalledWith('12345678910');
    });

    it('should trigger a snapshot and call the LE detection', () => {
      component.mode = 'LE';
      spyOn(component.trigger, 'next');
      const dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);
      component.dialogRef = dialogRefSpy;
      component.triggerSnapshot();
      expect(component.trigger.next).toHaveBeenCalled();
      expect(dialogRefSpy.close).toHaveBeenCalledWith('ABCD1234561');
    });

    it('should trigger a snapshot and call the SE detection', () => {
      component.mode = 'SE';
      spyOn(component.trigger, 'next');
      const dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);
      component.dialogRef = dialogRefSpy;
      component.triggerSnapshot();
      expect(component.trigger.next).toHaveBeenCalled();
      expect(dialogRefSpy.close).toHaveBeenCalledWith('123456');
    });

    it('should trigger a snapshot and call the DG detection', () => {
      component.mode = 'DG';
      spyOn(component.trigger, 'next');
      const dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);
      component.dialogRef = dialogRefSpy;
      component.triggerSnapshot();
      expect(component.trigger.next).toHaveBeenCalled();
      expect(dialogRefSpy.close).toHaveBeenCalledWith('');
    });

    it('should trigger a snapshot and handle the default case', () => {
      component.mode = '';
      spyOn(component.trigger, 'next');
      const dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);
      component.dialogRef = dialogRefSpy;
      component.triggerSnapshot();
      expect(component.trigger.next).toHaveBeenCalled();
      expect(dialogRefSpy.close).toHaveBeenCalledWith('');
    });
  });

  it('should handle init errors', () => {
    spyOn(snackService, 'handleDefaultErrorsWithText');
    const dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);
    component.dialogRef = dialogRefSpy;
    const error = new WebcamInitError();
    component.handleInitError(error);
    expect(component.errors).toContain(error);
    expect(snackService.handleDefaultErrorsWithText).toHaveBeenCalledWith(
      error.message,
      'error',
    );
    expect(dialogRefSpy.close).toHaveBeenCalledWith('');
  });

  it('should show the next webcam', () => {
    spyOn(component.nextWebcam, 'next');
    component.showNextWebcam(true);
    expect(component.nextWebcam.next).toHaveBeenCalledWith(true);
  });

  it('should handle webcam images', () => {
    const webcamImage = new WebcamImage(
      'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',
      'image/jpeg',
      null,
    );
    component.handleImage(webcamImage);
    expect(component.webcamImage).toBe(webcamImage);
  });

  it('should abort', () => {
    const dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);
    component.dialogRef = dialogRefSpy;
    component.abort();
    expect(dialogRefSpy.close).toHaveBeenCalledWith('');
  });
});
