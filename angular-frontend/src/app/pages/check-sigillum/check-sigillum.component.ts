/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { sealDTO } from 'src/app/shared/services/state/DTO/sealDTO';
import { SnackbarService } from 'src/app/shared/services/state/snackbar.service';
import { TrainStorageService } from 'src/app/shared/services/state/train-storage.service';
import { CameraComponent } from '../camera/camera.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-check-sigillum',
  templateUrl: './check-sigillum.component.html',
  styleUrls: ['./check-sigillum.component.scss'],
})
export class CheckSigillumComponent implements OnInit {
  @Input() waggonPositionNumber: number;
  @Input() loadingUnitNumber: string;

  addASealMode = true;
  savable = false;
  sealsArrayExist = false;
  addNewAllowed = false;
  dontSeeMode = true;
  muellMode = false;
  initialValue = '';
  doingModeText = 'sigillumPlumbum.addNewSeal';

  title = 'sigillumPlumbum.title';
  saveIt = 'sigillumPlumbum.saveIt';
  discardIt = 'sigillumPlumbum.discard';
  maxSeals = 10;

  menuItem1 = 'sigillumPlumbum.carrier';
  menuItem2 = 'sigillumPlumbum.terminal';
  menuItem3 = 'sigillumPlumbum.douane';
  menuItem4 = 'sigillumPlumbum.operator';
  menuItem5 = 'sigillumPlumbum.unknown';

  selectedValue = 'sigillumPlumbum.undefined';
  inputFieldText = '';

  seals: sealDTO[] = null;
  seal: sealDTO = null;

  constructor(
    private iconRegistry: MatIconRegistry,
    private domSanitzer: DomSanitizer,
    private snackService: SnackbarService,
    public router: Router,
    public route: ActivatedRoute,
    public trainStorage: TrainStorageService,
    private dialog: MatDialog,
  ) {
    this.registerIcon('loading-Unit-Icon',
      'assets/icons/loading-Unit-Icon.svg');
    this.registerIcon('lock',
      'assets/icons/third-party-material-icons/lock.svg');
    this.registerIcon('lockadd',
      'assets/icons/third-party-material-icons/custom-composed/lockadd.svg');
    this.registerIcon('dontSee',
      'assets/icons/third-party-material-icons/visibility_off_FILL0_wght400_GRAD0_opsz48.svg');
    this.registerIcon('muell',
      'assets/icons/third-party-material-icons/delete.svg');
    this.registerIcon('camera',
      'assets/icons/third-party-material-icons/photo_camera.svg');
    this.registerIcon('keyboard',
      'assets/icons/third-party-material-icons/baseline_keyboard_black_48dp.svg');
    this.registerIcon('photo',
      'assets/icons/Icon_KameraScan.svg');
    this.registerIcon('edit',
      'assets/icons/third-party-material-icons/custom-composed/comment_block.svg');


    this.route.queryParams.subscribe((params) => {
      this.loadingUnitNumber = params['loadingUnitNumber'];
      this.waggonPositionNumber = Number(params['waggonPositionNumber']);
    });
  }

  typeOfSeal: string[] = [
    this.menuItem1,
    this.menuItem2,
    this.menuItem3,
    this.menuItem4,
    this.menuItem5
  ];

  //If there are seals saved before this function will load the seals from the train storage and display the
  ngOnInit(): void {
    this.seals = this.trainStorage.getSigillumsFromUnitLoad(
      this.loadingUnitNumber
    );
    if (this.seals != null && this.seals.length > 0) {
      this.sealsArrayExist = true;
      this.dontSeeMode = false;
      if (this.seals.length >= this.maxSeals) {
        this.addASealMode = false;
        this.savable = false;
        this.snackService.handleDefaultErrorsWithText(
          'sigillumPlumbum.maxSealNumberReached',
          'error'
        );
      }
    }
    this.seal = this.trainStorage.createSigillum('', '');
    if (this.seals != null && this.seals.length > 0 && this.seals[0].sealTypus == 'sigillumPlumbum.unvisible') {
      this.addASealMode = false;
    }
  }

  //Register icons
  private registerIcon(iconName:string, value:string):void{
    this.iconRegistry.addSvgIcon(
      iconName,
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        value,
      ),
    );
  }

  //Sets the focus to the input field
  selectKeyboard() {
    document.getElementById('sealInputField').focus();
  }

  // This function should set the invisible state and commit it
  dontSeeButton() {
    if (this.muellMode == false) {
      this.savable = true;
      this.selectedValue = 'sigillumPlumbum.unvisible';
      this.inputFieldText = '';
      this.commit();
    } else {
      this.discard();
    }
  }

  //This function navigates to the waggon overview
  discard(): void {
    this.router.navigate(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: this.waggonPositionNumber },
    });
  }

  selectChoose(value: string) {
    this.savable = true;
    this.muellMode = true;
    this.dontSeeMode = false;
    this.selectedValue = value;
    this.addNewAllowed = true;
  }

  inputFieldFunction() {
    this.savable = true;
    this.muellMode = true;
    this.dontSeeMode = false;
    this.addNewAllowed = true;
  }

  //This function adds a entered seal to the seal array and will display it in the seal list
  addNewField() {
    this.doingModeText = 'sigillumPlumbum.addNewSeal';
    const seal: sealDTO = this.trainStorage.createSigillum(
      this.selectedValue,
      this.inputFieldText
    );
    this.trainStorage.addSigillumToUnitLoad(this.loadingUnitNumber, seal);
    this.seals = this.trainStorage.getSigillumsFromUnitLoad(
      this.loadingUnitNumber
    );
    if (this.seals != null && this.seals.length > 0) {
      this.sealsArrayExist = true;
    }
    this.seal = this.trainStorage.createSigillum('', '');
    this.selectedValue = null;
    this.inputFieldText = null;
    this.dontSeeMode = false;
    this.savable = false;
    this.muellMode = false;
    this.addNewAllowed = false;
    this.initialValue = '';
    if (this.seals.length >= this.maxSeals) {
      this.addASealMode = false;
      this.savable = false;
      this.snackService.handleMessageWithTranslateVariables(
        'sigillumPlumbum.maxSealNumberReached',
        3210,
      );
    }
  }

  //This function deletes a captured seal
  deleteItem(whichone: number) {
    this.seals.splice(whichone, 1);
    this.trainStorage.setSigillumsToUnitLoad(
      this.loadingUnitNumber,
      this.seals
    );
    if (this.seals.length < this.maxSeals) {
      this.addASealMode = true;
    }
    if (this.seals.length == 0) {
      this.dontSeeMode = true;
    }
  }

  //This function will load a captured seal for editing
  editItem(whichone: number) {
    this.doingModeText = 'sigillumPlumbum.changeSeal';
    const seal: sealDTO = this.seals[whichone];
    this.seals.splice(whichone, 1);
    this.trainStorage.setSigillumsToUnitLoad(
      this.loadingUnitNumber,
      this.seals,
    );
    if (this.seals.length < this.maxSeals) {
      this.addASealMode = true;
    }
    this.savable = true;
    if (this.seals.length == 0) {
      this.dontSeeMode = true;
    }
    this.inputFieldText = seal.sealNumber;
    this.initialValue = seal.sealTypus;
    this.selectedValue = this.initialValue;
    this.addNewAllowed = true;
  }

  notImplementedYet() {
    this.snackService.handleDefaultErrorsWithText(
      'sigillumPlumbum.notImplementedYet',
      'error',
    );
  }

  selectCamera() {
    const width = window.screen.availWidth;
    const height = Math.min(width * 1.3, window.screen.availHeight);
    const dialogRef = this.dialog.open(CameraComponent, {
      height: `${height}px`,
      width: `${width}px`,
      maxWidth: '800px',
      data: { width: Math.min(800, width), height: height, mode: 'SE' },
    });

    dialogRef.afterClosed().subscribe((detectedNumber: string) => {
      this.inputFieldText = detectedNumber;
    });
  }

  commit(): void {
    if (this.selectedValue != 'sigillumPlumbum.undefined') {
      this.seal = this.trainStorage.createSigillum(
        this.selectedValue,
        this.inputFieldText,
      );
      this.trainStorage.addSigillumToUnitLoad(
        this.loadingUnitNumber,
        this.seal,
      );
      this.router.navigate(['/waggon-overview'], {
        queryParams: { waggonPositionNumber: this.waggonPositionNumber },
      });
    }
  }
}
