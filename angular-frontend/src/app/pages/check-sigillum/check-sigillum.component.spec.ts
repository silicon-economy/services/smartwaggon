/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { TrainStorageService } from 'src/app/shared/services/state/train-storage.service';
import { CheckSigillumComponent } from './check-sigillum.component';
import { BehaviorSubject, of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { WaggonOverviewComponent } from '../waggon-overview/waggon-overview.component';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSelectModule } from '@angular/material/select';
import { sealDTO } from '../../shared/services/state/DTO/sealDTO';
import { expect } from '@angular/flex-layout/_private-utils/testing';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { ExceptionDialogComponent } from '../exception-dialog/exception-dialog.component';

describe('CheckSigillumComponent', () => {
  let component: CheckSigillumComponent;
  let fixture: ComponentFixture<CheckSigillumComponent>;
  let router: Router;
  let dialog: MatDialog;
  let snackService: SnackbarService;
  let trainStorageServiceSpy: jasmine.SpyObj<TrainStorageService>;

  const paramsSubject = new BehaviorSubject({
    number: 'ABCD1234',
    waggonPositionNumber: '2',
  });

  beforeEach(async () => {
    trainStorageServiceSpy = jasmine.createSpyObj('TrainStorageService', [
      'addSigillumToUnitLoad',
      'createSigillum',
      'getSigillumsFromUnitLoad',
      'setSigillumsToUnitLoad',
    ]);
    await TestBed.configureTestingModule({
      declarations: [CheckSigillumComponent],
      imports: [
        BrowserModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        HttpClientModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        TranslateTestingModule.withTranslations('de', {}),
        RouterTestingModule.withRoutes([
          { path: 'waggon-overview', component: WaggonOverviewComponent },
        ]),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            queryParams: of(paramsSubject),
          },
        },
        { provide: TrainStorageService, useValue: trainStorageServiceSpy },
        MatDialog,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CheckSigillumComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    snackService = TestBed.get(SnackbarService);
    dialog = TestBed.inject(MatDialog);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test the ngOnInit function', () => {
    const seal1: sealDTO = { sealNumber: '1234', sealTypus: 'typeA' };
    const seal2: sealDTO = { sealNumber: '1111', sealTypus: 'typeB' };
    const sigiliumList = [seal1, seal2];
    trainStorageServiceSpy.getSigillumsFromUnitLoad.and.returnValue(
      sigiliumList,
    );
    component.ngOnInit();
    expect(component.seals).toEqual(sigiliumList);
    expect(component.sealsArrayExist).toBeTrue();
    expect(component.dontSeeMode).toBeFalse();
    expect(component.trainStorage.createSigillum).toHaveBeenCalledWith('', '');
  });

  it('test the ngOnInit function', () => {
    const seal: sealDTO = {
      sealNumber: '1234',
      sealTypus: 'sigillumPlumbum.unvisible',
    };
    const sigiliumList = [seal];
    trainStorageServiceSpy.getSigillumsFromUnitLoad.and.returnValue(
      sigiliumList,
    );
    component.ngOnInit();
    expect(component.seals).toEqual(sigiliumList);
    expect(component.addASealMode).toBeFalse();
  });

  it('test the ngOnInit function when max seals are reached', () => {
    const seal1: sealDTO = { sealNumber: '1234', sealTypus: 'typeA' };
    const seal2: sealDTO = { sealNumber: '1111', sealTypus: 'typeB' };
    const sigiliumList = [seal1, seal2];
    component.maxSeals = 1;
    trainStorageServiceSpy.getSigillumsFromUnitLoad.and.returnValue(
      sigiliumList,
    );
    spyOn(snackService, 'handleDefaultErrorsWithText');
    component.ngOnInit();
    expect(snackService.handleDefaultErrorsWithText).toHaveBeenCalledWith(
      'sigillumPlumbum.maxSealNumberReached',
      'error',
    );
    expect(component.seals).toEqual(sigiliumList);
    expect(component.sealsArrayExist).toBeTrue();
    expect(component.dontSeeMode).toBeFalse();
    expect(component.addASealMode).toBeFalse();
    expect(component.savable).toBeFalse();
    expect(component.trainStorage.createSigillum).toHaveBeenCalledWith('', '');
    component.maxSeals = 10;
  });

  it('test the selectKeyboard function', () => {
    spyOn(document.getElementById('sealInputField'), 'focus');
    component.selectKeyboard();
    expect(document.getElementById('sealInputField').focus).toHaveBeenCalled();
  });

  it('test the dontSeeButton function', () => {
    spyOn(component, 'commit');
    component.muellMode = false;
    component.dontSeeButton();
    expect((component.savable = true));
    expect((component.selectedValue = 'sigillumPlumbum.unvisible'));
    expect((component.inputFieldText = ''));
    component.muellMode = true;
    component.dontSeeButton();
    expect(component.commit).toHaveBeenCalled;
  });

  it('should test the select camera function', () => {
    spyOn(dialog, 'open').and.returnValue({
      afterClosed: () => of(''),
    } as MatDialogRef<ExceptionDialogComponent>);
    component.selectCamera();
    expect(dialog.open).toHaveBeenCalled();
  });

  it('test commit function', () => {
    component.selectedValue = 'blubber';
    const seal1: sealDTO = { sealNumber: '1234', sealTypus: 'typeA' };
    trainStorageServiceSpy.createSigillum.and.returnValue(seal1);
    spyOn(router, 'navigate');
    component.waggonPositionNumber = 1;
    component.loadingUnitNumber = 'sdfs';
    component.commit();
    expect(router.navigate).toHaveBeenCalledWith(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: 1 },
    });
    expect(trainStorageServiceSpy.addSigillumToUnitLoad).toHaveBeenCalledWith(
      'sdfs',
      seal1,
    );
  });

  it('test the aboard function', () => {
    spyOn(router, 'navigate');
    component.waggonPositionNumber = 1;
    component.discard();
    expect(router.navigate).toHaveBeenCalledWith(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: 1 },
    });
  });

  it('test the selectChoose function', () => {
    component.selectedValue = '';
    component.selectChoose('ABCD');
    expect(component.selectedValue).toEqual('ABCD');
    expect(component.savable).toBeTrue();
    expect(component.muellMode).toBeTrue();
    expect(component.dontSeeMode).toBeFalse();
    expect(component.addNewAllowed).toBeTrue();
  });

  it('test the inputField function', () => {
    component.inputFieldFunction();
    expect(component.savable).toBeTrue();
    expect(component.muellMode).toBeTrue();
    expect(component.dontSeeMode).toBeFalse();
    expect(component.addNewAllowed).toBeTrue();
  });

  it('test the deleteItem function', () => {
    const seal1: sealDTO = { sealNumber: '1231', sealTypus: 'typeA' };
    const seal2: sealDTO = { sealNumber: '2341', sealTypus: 'typeB' };
    const seal3: sealDTO = { sealNumber: '3451', sealTypus: 'typeC' };
    component.seals = new Array<sealDTO>();
    component.seals.push(seal1);
    component.seals.push(seal2);
    component.seals.push(seal3);
    component.loadingUnitNumber = 'sdfs';
    component.deleteItem(1);
    expect(trainStorageServiceSpy.setSigillumsToUnitLoad).toHaveBeenCalledWith(
      'sdfs',
      component.seals,
    );
    expect(component.seals[0].sealNumber).toBe('1231');
    expect(component.seals[1].sealNumber).toBe('3451');
    expect(component.seals.length).toBe(2);
    expect(component.addASealMode).toBeTrue();
    component.seals.pop();
    component.deleteItem(0);
    expect(component.seals.length).toBe(0);
    expect(component.dontSeeMode).toBeTrue();
  });

  it('test the editItem function', () => {
    const seal1: sealDTO = { sealNumber: '1231', sealTypus: 'typeA' };
    const seal2: sealDTO = { sealNumber: '2341', sealTypus: 'typeB' };
    const seal3: sealDTO = { sealNumber: '3451', sealTypus: 'typeC' };
    component.seals = new Array<sealDTO>();
    component.seals.push(seal1);
    component.seals.push(seal2);
    component.seals.push(seal3);
    component.loadingUnitNumber = 'sdfs';
    component.editItem(1);
    expect(trainStorageServiceSpy.setSigillumsToUnitLoad).toHaveBeenCalledWith(
      'sdfs',
      component.seals,
    );
    expect(component.addASealMode).toBeTrue();
    component.seals.pop();
    component.editItem(0);
    expect(component.dontSeeMode).toBeTrue();
    expect(component.savable).toBeTrue();
    expect(component.seals.length).toBe(0);
    expect(component.addNewAllowed).toBeTrue();
  });

  it('test the addNewField function', () => {
    const seal1: sealDTO = { sealNumber: '1234', sealTypus: 'typeA' };
    const seal2: sealDTO = { sealNumber: '1111', sealTypus: 'typeB' };
    const sigiliumList = [seal1, seal2];
    trainStorageServiceSpy.createSigillum.and.returnValue(seal1);
    component.loadingUnitNumber = 'sdfs';
    trainStorageServiceSpy.getSigillumsFromUnitLoad.and.returnValue(
      sigiliumList,
    );
    component.addNewField();
    expect(trainStorageServiceSpy.addSigillumToUnitLoad).toHaveBeenCalledWith(
      'sdfs',
      seal1,
    );
    expect(component.sealsArrayExist).toBeTrue();
    expect(component.dontSeeMode).toBeFalse();
    expect(component.savable).toBeFalse();
    expect(component.muellMode).toBeFalse();
    expect(component.addNewAllowed).toBeFalse();
    expect(component.initialValue).toBe('');
    expect(component.selectedValue).toBeNull();
    expect(component.inputFieldText).toBeNull();
    let a: number;
    for (a = 0; a < 10; a++) {
      sigiliumList.push(seal1);
    }
    component.addNewField();
    expect(component.addASealMode).toBeFalse();
    expect(component.savable).toBeFalse();
  });

  it('test the notImplementedYet function', () => {
    component.notImplementedYet();
  });
});
