/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { SnackbarService } from 'src/app/shared/services/state/snackbar.service';
import { LoadingUnitComponent } from './loading-unit.component';
import { expect } from '@angular/flex-layout/_private-utils/testing';
import { SimpleChanges } from '@angular/core';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { of } from 'rxjs';
import { ExceptionDialogComponent } from '../exception-dialog/exception-dialog.component';

describe('LoadingUnitComponent', () => {
  let component: LoadingUnitComponent;
  let fixture: ComponentFixture<LoadingUnitComponent>;
  let event;
  let dialog: MatDialog;
  let snackService: SnackbarService;

  beforeEach(async () => {
    event = {
      target: {
        selectionStart: 0,
        value: 'abc',
      },
    };
    await TestBed.configureTestingModule({
      declarations: [LoadingUnitComponent],
      imports: [
        BrowserModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        FormsModule,
        NoopAnimationsModule,
        MatDialogModule,
        TranslateTestingModule.withTranslations('de', {}),
      ],
      providers: [{ provide: SnackbarService }, MatDialog],
    }).compileComponents();
    fixture = TestBed.createComponent(LoadingUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    dialog = TestBed.inject(MatDialog);
    snackService = TestBed.get(SnackbarService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not submit and call snack service', function () {
    spyOn(snackService, 'handleDefaultErrorsWithText');
    spyOn(
      component.trainStorage,
      'checkIfUnitLoadNumberIsAlreadyUsed',
    ).and.returnValue(true);
    component.loadUnitNumber = 'ABCD1234560';
    component.showButtons = true;
    expect(component.showLUFuctionKeys).toBeFalse();
    component.submit(true);
    expect(
      component.trainStorage.checkIfUnitLoadNumberIsAlreadyUsed,
    ).toHaveBeenCalled();
    expect(component.showButtons).toBeTrue();
    expect(component.showLUFuctionKeys).toBeFalse();
    expect(snackService.handleDefaultErrorsWithText).toHaveBeenCalledWith(
      'loadingUnit.numberAlreadyExists',
      'error',
    );
  });

  it('should submit', function () {
    spyOn(
      component.trainStorage,
      'checkIfUnitLoadNumberIsAlreadyUsed',
    ).and.returnValue(false);
    component.loadUnitNumber = 'ABCD1234560';
    component.showButtons = true;
    expect(component.showLUFuctionKeys).toBeFalse();
    component.submit(true);
    expect(component.showButtons).toBeFalse();
    expect(component.showLUFuctionKeys).toBeTrue();
    expect(
      component.trainStorage.checkIfUnitLoadNumberIsAlreadyUsed,
    ).toHaveBeenCalled();
    expect(component.disableInput).toBeTrue();
  });

  it('should discard', () => {
    component.showIcons = false;
    component.showButtons = true;
    component.inputIsOkayFlag = true;
    component.units = 'SUDU 307007 - 9';
    component.discard();
    expect(component.units).toBe('');
    expect(component.showIcons).toBeTrue();
    expect(component.showButtons).toBeFalsy();
    expect(component.inputIsOkayFlag).toBeFalsy();
  });

  it('should test the select camera function', () => {
    spyOn(dialog, 'open').and.returnValue({
      afterClosed: () => of(''),
    } as MatDialogRef<ExceptionDialogComponent>);
    spyOn(component, 'sendNumber');
    component.selectCamera();
    expect(dialog.open).toHaveBeenCalled();
    expect(component.sendNumber).toHaveBeenCalledWith('');
  });

  describe('dialog on submitting a wrong loading unit number', () => {
    it('should open the dialog and submit on confirmation', () => {
      component.units = 'ABCD123';
      spyOn(dialog, 'open').and.returnValue({
        afterClosed: () => of(true),
      } as MatDialogRef<ExceptionDialogComponent>);
      spyOn(component, 'submit');
      component.openExceptionDialog();
      expect(dialog.open).toHaveBeenCalled();
      expect(component.submit).toHaveBeenCalledWith(false);
    });

    it('should open the dialog and abort submission', () => {
      component.units = 'ABCD123';
      spyOn(dialog, 'open').and.returnValue({
        afterClosed: () => of(false),
      } as MatDialogRef<ExceptionDialogComponent>);
      spyOn(component, 'submit');
      component.openExceptionDialog();
      expect(dialog.open).toHaveBeenCalled();
      expect(component.submit).not.toHaveBeenCalled();
    });
  });

  it('should select Keyboard', () => {
    spyOn(component.inputField.nativeElement, 'focus');
    component.showLUFuctionKeys = false;
    component.selectKeyboard();
    expect(component.inputField.nativeElement.focus).toHaveBeenCalled();
    expect(component.showIcons).toBeFalse();
    expect(component.showButtons).toBeTrue();
  });

  it('should call snackService.handleDefaultErrorsWithText when showLUFuctionKeys is true', () => {
    component.showLUFuctionKeys = true;
    spyOn(snackService, 'handleDefaultErrorsWithText');
    component.selectKeyboard();
    expect(snackService.handleDefaultErrorsWithText).toHaveBeenCalledWith(
      'loadingUnit.LEInputFieldDisabled',
      'error',
    );
  });

  it('should convert the value to uppercase', () => {
    component.onKey(event);
    expect(event.target.value).toBe('ABC');
  });

  it('should set the selectionStart to the same position as before', () => {
    event.target.selectionStart = 2;
    component.onKey(event);
    expect(event.target.selectionStart).toBe(2);
  });

  it('should set the inputIsOkayFlag to the result of testTheNumber', () => {
    spyOn(component, 'testTheNumber').and.returnValue(true);
    component.onKey(event);
    expect(component.inputIsOkayFlag).toBe(true);
  });

  it('should testTheNumber', () => {
    expect(component.testTheNumber('ABCD1234567')).toBeFalse();
    expect(component.testTheNumber('ABCD1234560')).toBeTrue();
    expect(component.testTheNumber('SUDU 307007 - 9')).toBeTrue();
    expect(component.testTheNumber('SUDU3070079')).toBeTrue();
    let a: number;
    let testString: string;
    for (a = 0; a < 10; a++) {
      testString = 'CSQU305438' + a;
      if (a != 3) {
        expect(component.testTheNumber(testString)).toBeFalse();
      } else {
        expect(component.testTheNumber(testString)).toBeTrue();
      }
    }
  });

  it('should test the sendNumber function, when the waggon number is empty', () => {
    component.sendNumber('');
  });

  it('should test the sendNumber function, when the waggon number contains a non valid number', () => {
    component.sendNumber('12345');
    expect(component.showIcons).toBeFalsy();
    expect(component.showButtons).toBeTruthy();
  });

  it('should set the correct properties when loadingUnitNumber changes', () => {
    const changes: SimpleChanges = {
      loadingUnitNumber: {
        currentValue: '1234',
        previousValue: null,
        isFirstChange: () => true,
        firstChange: true,
      },
    };
    component.ngOnChanges(changes);
    expect(component.loadUnitNumber).toEqual('1234');
    expect(component.units).toEqual('1234');
    expect(component.showButtons).toBe(false);
    expect(component.showLUFuctionKeys).toBe(true);
    expect(component.showIcons).toBe(false);
    expect(component.disableInput).toBe(true);
  });

  it('should call the deleteLoadingUnit emitter', function () {
    spyOn(component.deleteLoadingUnit, 'emit');
    component.loadingUnitNumber = 'ABCD123';
    component.delete();
    expect(component.deleteLoadingUnit.emit).toHaveBeenCalledWith('ABCD123');
  });
});
