/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter,
  Input, OnChanges, Output, SimpleChanges, ViewChild,} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { SnackbarService } from 'src/app/shared/services/state/snackbar.service';
import { ExceptionDialogComponent } from '../exception-dialog/exception-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { TrainStorageService } from '../../shared/services/state/train-storage.service';
import { CameraComponent } from '../camera/camera.component';
import { conformToMask } from 'text-mask-core';

@Component({
  selector: 'app-loading-unit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './loading-unit.component.html',
  styleUrls: ['./loading-unit.component.scss'],
})
export class LoadingUnitComponent implements OnChanges {
  @Output() titleChangedEmitter = new EventEmitter<string>();
  @Output() unitAdded = new EventEmitter<string>();
  @Output() deleteLoadingUnit = new EventEmitter<string>();
  @Output() checkSumIsOkay = new EventEmitter<boolean>();
  @Input() loadingUnitNumber: string;
  @Input() waggonPositionNumber;
  @ViewChild('inputField') inputField: ElementRef;

  units;
  date_mask = [/[A-Za-z]/, /[A-Za-z]/, /[A-Za-z]/, /[A-Za-z]/, ' ', /\d/,
    /\d/, /\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, ];
  inputIsOkayFlag = false;
  loadUnitNumber = '';
  loadUnitNumberTmp = '';
  showIcons = true;
  showButtons = false;
  showLUFuctionKeys = false;
  disableInput = false;

  constructor(
    private iconRegistry: MatIconRegistry,
    private domSanitzer: DomSanitizer,
    private snackService: SnackbarService,
    private dialog: MatDialog,
    public trainStorage: TrainStorageService,
  ) {
    this.iconRegistry.addSvgIcon(
      'loading-Unit-Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/loading-Unit-Icon.svg',
      ),
    );
    this.iconRegistry.addSvgIcon(
      'camera_Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/Icon_KameraScan.svg',
      ),
    );
    this.iconRegistry.addSvgIcon(
      'keyboard_Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/baseline_keyboard_black_48dp.svg',
      ),
    );
    this.iconRegistry.addSvgIcon(
      'delete_Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/delete.svg',
      ),
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['loadingUnitNumber'].currentValue) {
      this.loadUnitNumber = changes['loadingUnitNumber'].currentValue;
      this.units = this.loadUnitNumber;
      this.showButtons = false;
      this.showLUFuctionKeys = true;
      this.showIcons = false;
      this.disableInput = true;
      this.inputIsOkayFlag = true;
    }
  }

  /* When the Keyboard button  is clicked
  selectKeyboard method will change title of page to loadUnit
  and change the buttons  to (submit , discard) */
  selectKeyboard() {
    if (!this.showLUFuctionKeys) {
      this.showIcons = false;
      this.showButtons = true;
      this.inputField.nativeElement.focus();
    } else {
      this.snackService.handleDefaultErrorsWithText(
        'loadingUnit.LEInputFieldDisabled',
        'error',
      );
    }
  }

  //Turns on the camera component for taking a picture.
  //The detected number is emitted to the sendNumber function from the camera component.
  selectCamera() {
    const currentWidth = window.screen.availWidth;
    const currentHeight = Math.min(
      currentWidth * 1.3,
      window.screen.availHeight,
    );

    const dialogRef = this.dialog.open(CameraComponent, {
      height: `${currentHeight}px`,
      width: `${currentWidth}px`,
      maxWidth: '800px',
      data: {
        width: Math.min(800, currentWidth),
        height: currentHeight,
        mode: 'LE', //For Seal detection
      },
    });

    dialogRef.afterClosed().subscribe((detectedNumber: string) => {
      this.sendNumber(detectedNumber);
    });
  }

  /* When the discard button is clicked,
   the discard method will work and return the title of the page to overview
   and also change the buttons back to what they were (Keyboard, camera) */
  discard() {
    this.units = '';
    this.titleChangedEmitter.emit('waggon.overview');
    this.showIcons = true;
    this.showButtons = false;
    this.inputIsOkayFlag = false;
  }

  //submits the loading unit number and emits it to the waggon overview if the number doesn't already exist
  submit(okayChecksum: boolean) {
    if (!this.trainStorage.checkIfUnitLoadNumberIsAlreadyUsed(this.units)) {
      this.checkSumIsOkay.emit(okayChecksum);
      this.unitAdded.emit(this.units);
      this.showButtons = false;
      this.showLUFuctionKeys = true;
      this.disableInput = true;
    } else {
      this.snackService.handleDefaultErrorsWithText(
        'loadingUnit.numberAlreadyExists',
        'error',
      );
    }
  }

  //This function handles the submit button if the checksum isn't valid
  openExceptionDialog() {
    const dialogRef = this.dialog.open(ExceptionDialogComponent, {
      data: {
        number: this.units,
      },
    });

    dialogRef.afterClosed().subscribe((status: boolean) => {
      if (status) {
        this.submit(false);
      }
    });
  }

  /*Tests, if checksum is correct. The input must be four letters
  followed by seven digits, the last digit must be the checksum
  of the string. The checksum is calculated with algorithm of
  ISO 6346 */
  testTheNumber(value: string): boolean {
    let retVal = false;
    this.loadUnitNumberTmp = value;
    value = value.replace(/\s/g, '');
    value = value.replace(/-/g, '');
    value = value.replace(/_/g, '');
    if (value.length == 11) {
      let multiplier = 1;
      let checksum = 0;
      let a: number;
      let charValue: number;
      for (a = 0; a < 4; a++) {
        if (value.charAt(a) >= 'A' && value.charAt(a) <= 'Z') {
          charValue = value.charCodeAt(a) - 55;
          if (charValue > 10) {
            //A-Limes
            charValue++;
          }
          if (charValue > 21) {
            //K-Frontiere
            charValue++;
          }
          if (charValue > 32) {
            //U-Border
            charValue++;
          }
          checksum += charValue * multiplier;
          multiplier *= 2;
        } else {
          break;
        }
      }
      for (a = 4; a < 10; a++) {
        if (value.charAt(a) >= '0' && value.charAt(a) <= '9') {
          charValue = value.charCodeAt(a) - '0'.charCodeAt(0);
          checksum += charValue * multiplier;
          multiplier *= 2;
        } else {
          break;
        }
      }
      checksum = checksum % 11;
      if (checksum == 10) {
        checksum = 0;
      }
      if (checksum == value.charCodeAt(10) - '0'.charCodeAt(0)) {
        retVal = true;
        this.loadUnitNumber = value;
      }
    }
    return retVal;
  }

  //onKey(event) is called everytime a input is done in the input field
  onKey(event) {
    const position = event.target.selectionStart;
    event.target.value = event.target.value.toUpperCase();
    event.target.selectionStart = position;
    this.inputIsOkayFlag = this.testTheNumber(event.target.value);
  }

  // receives the detected number from the camera component, handles it and passes it into the input field.
  sendNumber($number: string) {
    this.titleChangedEmitter.emit('waggon.capture');
    if ($number != '') {
      this.showIcons = false;
      this.showButtons = true;
      this.units = conformToMask($number, this.date_mask, {}).conformedValue;
      this.inputIsOkayFlag = this.testTheNumber(this.units); //Tests if the detected number is valid
    }
  }

  delete() {
    this.deleteLoadingUnit.emit(this.loadingUnitNumber);
  }
}
