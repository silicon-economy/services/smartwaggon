/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DangergoodlabelCaptureComponent } from './dangergoodlabel-capture.component';
import { SnackbarService } from 'src/app/shared/services/state/snackbar.service';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, of } from 'rxjs';
import { TrainStorageService } from '../../shared/services/state/train-storage.service';
import { WaggonOverviewComponent } from '../waggon-overview/waggon-overview.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { dangerGoodLabelDTO } from '../../shared/services/state/DTO/dangerGoodLabelDTO';
import { LoadingUnitService } from '../../shared/services/http/loading-unit.service';

describe('DangergoodlabelCaptureComponent', () => {
  let component: DangergoodlabelCaptureComponent;
  let fixture: ComponentFixture<DangergoodlabelCaptureComponent>;
  let trainStorageServiceSpy: jasmine.SpyObj<TrainStorageService>;
  let loadingUnitServiceSpy: jasmine.SpyObj<LoadingUnitService>;

  let router: Router;

  const paramsSubject = new BehaviorSubject({
    number: '23 12 312 3 123 - 5',
    showWaggonNumber: 'false',
    waggonPositionNumber: '2',
  });

  beforeEach(async () => {
    loadingUnitServiceSpy = jasmine.createSpyObj('LoadingUnitService', [
      'saveDangerGoodLabel',
    ]);
    trainStorageServiceSpy = jasmine.createSpyObj('TrainStorageService', [
      'setDangergoodLabelToUnitLoad',
      'getDangergoodStatesFromUnitLoadAsString',
      'getDangerGoodLabelsFromUnitLoad',
    ]);
    await TestBed.configureTestingModule({
      declarations: [DangergoodlabelCaptureComponent],
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'waggon-overview', component: WaggonOverviewComponent },
        ]),
        TranslateTestingModule.withTranslations('de', {}),
        HttpClientTestingModule,
        MatFormFieldModule,
        MatIconModule,
        MatButtonModule,
      ],
      providers: [
        { provide: SnackbarService, useValue: { pushNewStatus: () => ({}) } },
        {
          provide: ActivatedRoute,
          useValue: { queryParams: of({ loadingUnitNumber: 'ABCD12345670' }) },
        },
        {
          provide: ActivatedRoute,
          useValue: { queryParams: of(paramsSubject) },
        },
        { provide: TrainStorageService, useValue: trainStorageServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DangergoodlabelCaptureComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should actionButtons', () => {
    expect(component.actionsButton('o1')).toBe();
    expect(component.actionsButton('o2')).toBe();
    expect(component.actionsButton('o3')).toBe();
    expect(component.actionsButton('l1')).toBe();
    expect(component.actionsButton('l2')).toBe();
    expect(component.actionsButton('l3')).toBe();
    expect(component.actionsButton('r1')).toBe();
    expect(component.actionsButton('r3')).toBe();
    expect(component.actionsButton('r2')).toBe();
    expect(component.actionsButton('u2')).toBe();
    expect(component.actionsButton('u3')).toBe();
    expect(component.actionsButton('u1')).toBe();
  });

  it('should discard', () => {
    spyOn(router, 'navigate');
    component.waggonPositionNumber = 1;
    component.discard();
    expect(router.navigate).toHaveBeenCalledWith(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: 1 },
    });
  });

  it('should generateStateInfo', () => {
    expect(component.generateStateInfo()).toString();
  });

  it('should setStateInfo', () => {
    let a, b, c, d;
    let wort = '0000';
    for (a = 0; a < 4; a++) {
      for (b = 0; b < 4; b++) {
        for (c = 0; c < 4; c++) {
          for (d = 0; d < 4; d++) {
            wort = a + '' + b + '' + c + '' + d;
            expect(component.setStateInfo(wort)).toBe();
          }
        }
      }
    }
  });

  it('should commit', () => {
    spyOn(router, 'navigate');
    trainStorageServiceSpy.setDangergoodLabelToUnitLoad.and.callThrough();
    component.waggonPositionNumber = 1;
    component.commit();
    expect();
    expect(
      trainStorageServiceSpy.setDangergoodLabelToUnitLoad,
    ).toHaveBeenCalledTimes(4);
    expect(router.navigate).toHaveBeenCalledWith(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: 1 },
    });
  });

  it('should save danger goods and log success message', () => {
    // Set up
    const mockLabels: dangerGoodLabelDTO[] = [
      {
        statusNumber: 1,
        statusStr: 'stringtest',
        position: 1,
        creationDate: new Date(),
        modificationDate: new Date(),
      },
      {
        statusNumber: 2,
        statusStr: 'stringtest2',
        position: 2,
        creationDate: new Date(),
        modificationDate: new Date(),
      },
    ];
    loadingUnitServiceSpy.saveDangerGoodLabel.and.returnValue(of(null));
    component.saveDangerGoods(mockLabels, loadingUnitServiceSpy);

    expect(loadingUnitServiceSpy.saveDangerGoodLabel).toHaveBeenCalledTimes(1);
    expect(loadingUnitServiceSpy.saveDangerGoodLabel).toHaveBeenCalledWith(
      component.loadingUnitNumber,
      mockLabels,
    );
  });
});
