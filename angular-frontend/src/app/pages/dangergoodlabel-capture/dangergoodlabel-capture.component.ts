/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { SnackbarService } from 'src/app/shared/services/state/snackbar.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TrainStorageService } from '../../shared/services/state/train-storage.service';
import { dangerGoodLabelDTO } from '../../shared/services/state/DTO/dangerGoodLabelDTO';
import { LoadingUnitService } from '../../shared/services/http/loading-unit.service';

@Component({
  selector: 'app-dangergoodlabel-capture',
  templateUrl: './dangergoodlabel-capture.component.html',
  styleUrls: ['./dangergoodlabel-capture.component.scss'],
})
export class DangergoodlabelCaptureComponent implements OnInit {
  /* to use it:
    set e.g. variable number = "MILK 555555 - 5";
    <app-dangergoodlabel-capture [unitload_Number]="number"
                                 [walking_back]="true | false">
                                 [state]="nnnn"
    </app-dangergoodlabel-capture>
  */

  @Input() walking_back: boolean;
  @Input() state: string;
  @Input() waggonPositionNumber: number;

  title = 'dangergoodlabel.title';
  saveIt = 'dangergoodlabel.saveIt';
  discardIt = 'dangergoodlabel.discard';

  accent = 'accent';
  nonAccent = '';

  bottomChoose = '0';
  topChoose = '0';
  leftChoose = '0';
  rightChoose = '0';

  //definition of status = ["undefined", "readable", "block", "unvisible"];
  status = ['0', '1', '2', '3'];

  farbe_o1: string = this.nonAccent;
  farbe_o2: string = this.nonAccent;
  farbe_o3: string = this.nonAccent;
  farbe_l1: string = this.nonAccent;
  farbe_l2: string = this.nonAccent;
  farbe_l3: string = this.nonAccent;
  farbe_r1: string = this.nonAccent;
  farbe_r2: string = this.nonAccent;
  farbe_r3: string = this.nonAccent;
  farbe_u1: string = this.nonAccent;
  farbe_u2: string = this.nonAccent;
  farbe_u3: string = this.nonAccent;

  actioncanbeperformedFlag = false;
  actioncanbeperformedORCounter = 0;

  loadingUnitNumber: string;

  constructor(
    private loadingUnitService: LoadingUnitService,
    private iconRegistry: MatIconRegistry,
    private domSanitzer: DomSanitizer,
    private snackService: SnackbarService,
    private router: Router,
    private route: ActivatedRoute,
    public trainStorage: TrainStorageService,
  ) {
    this.iconRegistry.addSvgIcon(
      'block_FILL0',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/block_FILL0_wght400_GRAD0_opsz48.svg',
      ),
    );
    this.iconRegistry.addSvgIcon(
      'done_FILL0',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/done_FILL0_wght400_GRAD0_opsz48.svg',
      ),
    );
    this.iconRegistry.addSvgIcon(
      'visoff_FILL0',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/visibility_off_FILL0_wght400_GRAD0_opsz48.svg',
      ),
    );
    this.iconRegistry.addSvgIcon(
      'loading-Unit-Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/loading-Unit-Icon.svg',
      ),
    );

    this.route.queryParams.subscribe((params) => {
      this.loadingUnitNumber = params['loadingUnitNumber'];
      this.waggonPositionNumber = Number(params['waggonPositionNumber']);
    });
  }

  ngOnInit(): void {
    this.state = this.trainStorage.getDangergoodStatesFromUnitLoadAsString(
      this.loadingUnitNumber,
    );
    if (this.state != null && this.state.length == 4) {
      this.setStateInfo(this.state);
    }
  }

  actionsButton(choose: string): void {
    if (choose.charAt(0) == 'o') {
      this.actioncanbeperformedORCounter |= 1;
      this.farbe_o1 = this.nonAccent;
      this.farbe_o2 = this.nonAccent;
      this.farbe_o3 = this.nonAccent;
      this.topChoose = '' + choose.charAt(1);
      if (choose.charAt(1) == '1') {
        this.farbe_o1 = this.accent;
      } else if (choose.charAt(1) == '2') {
        this.farbe_o2 = this.accent;
      } else if (choose.charAt(1) == '3') {
        this.farbe_o3 = this.accent;
      }
    } else if (choose.charAt(0) == 'l') {
      this.actioncanbeperformedORCounter |= 2;
      this.farbe_l1 = this.nonAccent;
      this.farbe_l2 = this.nonAccent;
      this.farbe_l3 = this.nonAccent;
      this.leftChoose = '' + choose.charAt(1);
      if (choose.charAt(1) == '1') {
        this.farbe_l1 = this.accent;
      } else if (choose.charAt(1) == '2') {
        this.farbe_l2 = this.accent;
      } else if (choose.charAt(1) == '3') {
        this.farbe_l3 = this.accent;
      }
    } else if (choose.charAt(0) == 'r') {
      this.actioncanbeperformedORCounter |= 4;
      this.farbe_r1 = this.nonAccent;
      this.farbe_r2 = this.nonAccent;
      this.farbe_r3 = this.nonAccent;
      this.rightChoose = '' + choose.charAt(1);
      if (choose.charAt(1) == '1') {
        this.farbe_r1 = this.accent;
      } else if (choose.charAt(1) == '2') {
        this.farbe_r2 = this.accent;
      } else if (choose.charAt(1) == '3') {
        this.farbe_r3 = this.accent;
      }
    } else if (choose.charAt(0) == 'u') {
      this.actioncanbeperformedORCounter |= 8;
      this.farbe_u1 = this.nonAccent;
      this.farbe_u2 = this.nonAccent;
      this.farbe_u3 = this.nonAccent;
      this.bottomChoose = '' + choose.charAt(1);
      if (choose.charAt(1) == '1') {
        this.farbe_u1 = this.accent;
      } else if (choose.charAt(1) == '2') {
        this.farbe_u2 = this.accent;
      } else if (choose.charAt(1) == '3') {
        this.farbe_u3 = this.accent;
      }
    }
    if (this.walking_back && this.actioncanbeperformedORCounter == 15) {
      this.actioncanbeperformedFlag = true;
    }
    if (!this.walking_back) {
      if (
        (this.actioncanbeperformedORCounter & 2) == 2 ||
        (this.actioncanbeperformedORCounter & 4) == 4
      ) {
        this.actioncanbeperformedFlag = true;
      }
    }
  }

  discard(): void {
    this.router.navigate(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: this.waggonPositionNumber },
    });
  }

  clearOverview() {
    this.farbe_o1 = this.nonAccent;
    this.farbe_o2 = this.nonAccent;
    this.farbe_o3 = this.nonAccent;
    this.farbe_l1 = this.nonAccent;
    this.farbe_l2 = this.nonAccent;
    this.farbe_l3 = this.nonAccent;
    this.farbe_r1 = this.nonAccent;
    this.farbe_r2 = this.nonAccent;
    this.farbe_r3 = this.nonAccent;
    this.farbe_u1 = this.nonAccent;
    this.farbe_u2 = this.nonAccent;
    this.farbe_u3 = this.nonAccent;
    this.actioncanbeperformedFlag = false;
    this.actioncanbeperformedORCounter = 0;
    this.topChoose = '0';
    this.bottomChoose = '0';
    this.leftChoose = '0';
    this.rightChoose = '0';
  }

  generateStateInfo(): string {
    return (
      this.topChoose + this.bottomChoose + this.leftChoose + this.rightChoose
    );
  }

  setStateInfo(was: string) {
    this.clearOverview();
    if (was.length == 4) {
      this.topChoose = was.charAt(0);
      this.bottomChoose = was.charAt(1);
      this.leftChoose = was.charAt(2);
      this.rightChoose = was.charAt(3);
      if (was.charAt(0) != '0') {
        this.actioncanbeperformedORCounter |= 1;
      }
      if (was.charAt(1) != '0') {
        this.actioncanbeperformedORCounter |= 8;
      }
      if (was.charAt(2) != '0') {
        this.actioncanbeperformedORCounter |= 2;
      }
      if (was.charAt(3) != '0') {
        this.actioncanbeperformedORCounter |= 4;
      }
      if (was.charAt(0) == '1') {
        this.farbe_o1 = this.accent;
      } else if (was.charAt(0) == '2') {
        this.farbe_o2 = this.accent;
      } else if (was.charAt(0) == '3') {
        this.farbe_o3 = this.accent;
      }
      if (was.charAt(1) == '1') {
        this.farbe_u1 = this.accent;
      } else if (was.charAt(1) == '2') {
        this.farbe_u2 = this.accent;
      } else if (was.charAt(1) == '3') {
        this.farbe_u3 = this.accent;
      }
      if (was.charAt(2) == '1') {
        this.farbe_l1 = this.accent;
      } else if (was.charAt(2) == '2') {
        this.farbe_l2 = this.accent;
      } else if (was.charAt(2) == '3') {
        this.farbe_l3 = this.accent;
      }
      if (was.charAt(3) == '1') {
        this.farbe_r1 = this.accent;
      } else if (was.charAt(3) == '2') {
        this.farbe_r2 = this.accent;
      } else if (was.charAt(3) == '3') {
        this.farbe_r3 = this.accent;
      }
      if (this.walking_back && this.actioncanbeperformedORCounter == 15) {
        this.actioncanbeperformedFlag = true;
      }
      if (!this.walking_back) {
        if (
          (this.actioncanbeperformedORCounter & 2) == 2 ||
          (this.actioncanbeperformedORCounter & 4) == 4
        ) {
          this.actioncanbeperformedFlag = true;
        }
      }
    }
  }

  commit(): void {
    this.trainStorage.setDangergoodLabelToUnitLoad(
      this.loadingUnitNumber,
      dangerGoodLabelDTO.positionFront,
      this.status[this.topChoose.charAt(0)],
    );
    this.trainStorage.setDangergoodLabelToUnitLoad(
      this.loadingUnitNumber,
      dangerGoodLabelDTO.positionRear,
      this.status[this.bottomChoose.charAt(0)],
    );
    this.trainStorage.setDangergoodLabelToUnitLoad(
      this.loadingUnitNumber,
      dangerGoodLabelDTO.positionLeft,
      this.status[this.leftChoose.charAt(0)],
    );
    this.trainStorage.setDangergoodLabelToUnitLoad(
      this.loadingUnitNumber,
      dangerGoodLabelDTO.positionRight,
      this.status[this.rightChoose.charAt(0)],
    );
    this.saveDangerGoods(
      this.trainStorage.getDangerGoodLabelsFromUnitLoad(this.loadingUnitNumber),
      this.loadingUnitService,
    );
    this.router.navigate(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: this.waggonPositionNumber },
    });
  }
  saveDangerGoods(
    dangerGoods: dangerGoodLabelDTO[],
    loadingunitSerive: LoadingUnitService,
  ) {
    loadingunitSerive
      .saveDangerGoodLabel(this.loadingUnitNumber, dangerGoods)
      .subscribe({
        next: () => {
          console.log('danger Good Labels has been saved successfully');
        },
        error: (error) => {
          console.error('Error saving dangerGoods :', error);
        },
      });
  }
}
