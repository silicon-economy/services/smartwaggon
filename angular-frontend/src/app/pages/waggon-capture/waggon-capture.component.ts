/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Output, EventEmitter } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material/dialog';
import { ExceptionDialogComponent } from '../exception-dialog/exception-dialog.component';
import { TrainStorageService } from '../../shared/services/state/train-storage.service';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { waggonDTO } from '../../shared/services/state/DTO/waggonDTO';
import { WaggonService } from '../../shared/services/http/waggon.service';
import { CameraComponent } from '../camera/camera.component';
import { conformToMask } from 'text-mask-core';

@Component({
  selector: 'app-waggon-capture',
  templateUrl: './waggon-capture.component.html',
  styleUrls: ['./waggon-capture.component.scss'],
})
export class WaggonCaptureComponent {
  @Output() titleChangedEmitter = new EventEmitter<string>();
  @Output() capturedWaggonNumberFormattedEmitter = new EventEmitter<string>();
  @Output() checksumIsOkay = new EventEmitter<boolean>();

  myModel; //Contains the current input of the inputField
  date_mask = [
    /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/,
    /\d/, ' ', /\d/, ' ', /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/,
  ];
  waggonNumber = '';
  formattedWaggonNumber = '';
  waggonNumberIsValid = false;
  showIcons = true;
  showButtons = false;

  constructor(
    public waggonService: WaggonService,
    private ds: DomSanitizer,
    private iconRegistry: MatIconRegistry,
    private dialog: MatDialog,
    public trainStorage: TrainStorageService,
    private snackService: SnackbarService,
  ) {
    this.registerIcon('waggon-icon',
      'assets/icons/waggon_icon.svg');
    this.registerIcon('camera_Icon',
      'assets/icons/Icon_KameraScan.svg');
    this.registerIcon('keyboard_Icon',
      'assets/icons/third-party-material-icons/baseline_keyboard_black_48dp.svg');
  }

  //Register icons
  private registerIcon(iconName:string, value:string):void{
    this.iconRegistry.addSvgIcon(
      iconName,
      this.ds.bypassSecurityTrustResourceUrl(
        value,
      ),
    );
  }

  selectKeyboard() {
    this.titleChangedEmitter.emit('waggon.capture');
    this.showIcons = false;
    this.showButtons = true;
    document.getElementById('WN-InputField').focus();
  }

  //Turns on the camera component for taking a picture.
  //The detected number is emitted to the sendNumber function from the camera component.
  selectCamera() {
    const currentWidth = window.screen.availWidth;
    const currentHeight = Math.min(
      currentWidth * 1.3,
      window.screen.availHeight,
    );

    const dialogRef = this.dialog.open(CameraComponent, {
      height: `${currentHeight}px`,
      width: `${currentWidth}px`,
      maxWidth: '800px',
      data: {
        width: Math.min(800, currentWidth),
        height: currentHeight,
        mode: 'WG',
      },
    });

    dialogRef.afterClosed().subscribe((detectedNumber: string) => {
      this.sendNumber(detectedNumber);
    });
  }

  discard() {
    this.myModel = '';
    this.titleChangedEmitter.emit('waggon.overview');
    this.showIcons = true;
    this.showButtons = false;
  }

  saveWaggon(waggon: waggonDTO) {
    this.waggonService.save(waggon, this.formattedWaggonNumber).subscribe({
      next: () => {
        console.log('Waggon saved successfully');
      },
      error: (error) => {
        console.error('Error saving waggon:', error);
      },
    });
  }
  //submits the waggon number and emits it to the waggon overview if the number doesn't already exist
  submit(okayChecksum: boolean) {
    if (
      !this.trainStorage.checkIfWaggonNumberIsAlreadyUsed(
        this.formattedWaggonNumber,
      )
    ) {
      this.showButtons = false;
      this.titleChangedEmitter.emit('waggon.overview');
      this.checksumIsOkay.emit(okayChecksum);
      this.capturedWaggonNumberFormattedEmitter.emit(this.formattedWaggonNumber);
      this.saveWaggon(this.trainStorage.getWaggon(this.formattedWaggonNumber));
    } else {
      this.snackService.handleDefaultErrorsWithText(
        'waggonCapture.numberAlreadyExists',
        'error',
      );
    }
  }

  //This function handles the submit button if the checksum isn't valid
  openExceptionDialog() {
    const dialogRef = this.dialog.open(ExceptionDialogComponent, {
      data: {
        number: this.myModel.toString(),
      },
    });

    dialogRef.afterClosed().subscribe((status: boolean) => {
      if (status) {
        this.submit(false);
      }
    });
  }

  /* The function testTheNumber takes an input string which contains a waggon number and checks
    if the number is valid. The number is valid, if it contains twelve digits, where the last digit is the checksum.
    For the first eleven digits the cross sum is computed, by using an alternating factor of one and two.
    The checksum is valid, if it is equal to 10 minus the cross sum.   */
  testTheNumber(input: string): boolean {
    let retVal = false;
    let sum = 0,
      tmpSum;

    function testCheckSum(lastDigit: number, checksum: number) {
      return (10 - parseInt(checksum.toString().slice(-1))) % 10 == lastDigit;
    }

    if (input.length == 12) {
      for (let a = 0; a <= 10; a++) {
        if (input.charAt(a) >= '0' && input.charAt(a) <= '9') {
          let factor: number;
          //The alternating factor is computed
          if (a % 2 == 0) {
            factor = 2;
          } else {
            factor = 1;
          }
          tmpSum = parseInt(input.charAt(a)) * factor;
          if (tmpSum >= 10) {
            tmpSum -= 10;
            sum = sum + 1;
          }
          sum = sum + tmpSum;
          retVal = testCheckSum(parseInt(input.charAt(11)), sum);
        } else {
          retVal = false;
          break;
        }
      }
    }
    return retVal;
  }

  /* The function stripAndTrimTheString takes the String and deletes all spaces and
    minus-signs, then gives it to
    this.testTheNumber and knows after if there are only digits in it. If it is so,
    the result is stored in variable waggonNumber and waggonNumberIsValid is set to true
    to enable the submit-button. */
  stripAndTrimTheString(input: string): boolean {
    input = input.replace(/\s/g, '');
    input = input.replace(/-/g, '');
    input = input.replace(/_/g, '');
    this.waggonNumberIsValid = this.testTheNumber(input);
    if (this.waggonNumberIsValid) {
      this.waggonNumber = input;
    }
    return this.waggonNumberIsValid;
  }

  //Is called when a button in the InputField is pressed
  onKey(event) {
    const input: string = event.target.value;
    this.formattedWaggonNumber = input;
    this.stripAndTrimTheString(input);
  }

  // receives the detected number from the camera component, handles it and passes it into the input field.
  sendNumber(number: string) {
    this.titleChangedEmitter.emit('waggon.capture');
    if (number != '') {
      this.showIcons = false;
      this.showButtons = true;
      this.myModel = conformToMask(number, this.date_mask, {}).conformedValue;
      this.stripAndTrimTheString(this.myModel); //Tests if the detected number is valid
      this.formattedWaggonNumber = this.myModel;
    }
  }
}
