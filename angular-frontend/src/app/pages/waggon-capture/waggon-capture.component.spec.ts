/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WaggonCaptureComponent } from './waggon-capture.component';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { expect } from '@angular/flex-layout/_private-utils/testing';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { of } from 'rxjs';
import { ExceptionDialogComponent } from '../exception-dialog/exception-dialog.component';
import { waggonDTO } from '../../shared/services/state/DTO/waggonDTO';
import { WaggonService } from '../../shared/services/http/waggon.service';

// Tests for the WaggonCaptureComponent
describe('WaggonCaptureComponent', () => {
  let component: WaggonCaptureComponent;
  let fixture: ComponentFixture<WaggonCaptureComponent>;
  let dialog: MatDialog;
  let snackService: SnackbarService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WaggonCaptureComponent],
      imports: [
        BrowserModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        MatDialogModule,
        TranslateTestingModule.withTranslations('de', {}),
      ],
      providers: [WaggonService, MatDialog],
    }).compileComponents();
    fixture = TestBed.createComponent(WaggonCaptureComponent);
    component = fixture.componentInstance;
    dialog = TestBed.inject(MatDialog);
    snackService = TestBed.get(SnackbarService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('dialog on submitting a wrong waggon unit number', () => {
    it('should open the dialog and submit on confirmation', () => {
      component.myModel = 'W001';
      spyOn(dialog, 'open').and.returnValue({
        afterClosed: () => of(true),
      } as MatDialogRef<ExceptionDialogComponent>);
      spyOn(component, 'submit');
      component.openExceptionDialog();
      expect(dialog.open).toHaveBeenCalled();
      expect(component.submit).toHaveBeenCalledWith(false);
    });

    it('should open the dialog and abort submission', () => {
      component.myModel = 'W001';
      spyOn(dialog, 'open').and.returnValue({
        afterClosed: () => of(false),
      } as MatDialogRef<ExceptionDialogComponent>);
      spyOn(component, 'submit');
      component.openExceptionDialog();
      expect(dialog.open).toHaveBeenCalled();
      expect(component.submit).not.toHaveBeenCalled();
    });
  });

  it('should test the select camera function', () => {
    component.myModel = '';
    spyOn(dialog, 'open').and.returnValue({
      afterClosed: () => of(''),
    } as MatDialogRef<ExceptionDialogComponent>);
    spyOn(component, 'sendNumber');
    component.selectCamera();
    expect(dialog.open).toHaveBeenCalled();
    expect(component.sendNumber).toHaveBeenCalledWith('');
  });

  // Tests the discard function
  it('should discard', () => {
    component.selectKeyboard();
    component.myModel = '12 39 812 3 785 - 5';
    expect(component.showIcons).toBeFalsy();
    expect(component.showButtons).toBeTruthy();
    component.discard();
    expect(component.myModel).toBe('');
    expect(component.showIcons).toBeTruthy();
    expect(component.showButtons).toBeFalsy();
  });

  // Tests the selection of the Keyboard which should lead to the Wa
  it('should select Keyboard', () => {
    expect(component.showButtons).toBeFalsy();
    expect(component.showIcons).toBeTrue();
    component.selectKeyboard();
    expect(component.showButtons).toBeTruthy();
    expect(component.showIcons).toBeFalsy();
  });

  // Tests the submit function
  it('should not submit and call snack service', () => {
    spyOn(snackService, 'handleDefaultErrorsWithText');
    component.selectKeyboard();
    spyOn(
      component.trainStorage,
      'checkIfWaggonNumberIsAlreadyUsed',
    ).and.returnValue(true);
    expect(component.showButtons).toBeTruthy();
    component.submit(true);
    expect(component.showButtons).toBeTruthy();
    expect(snackService.handleDefaultErrorsWithText).toHaveBeenCalledWith(
      'waggonCapture.numberAlreadyExists',
      'error',
    );
  });

  it('should submit', () => {
    component.selectKeyboard();
    spyOn(
      component.trainStorage,
      'checkIfWaggonNumberIsAlreadyUsed',
    ).and.returnValue(false);
    expect(component.showButtons).toBeTruthy();
    component.submit(true);
    expect(component.showButtons).toBeFalse();
  });

  // Tests the testTheNumber function including zero checksums
  it('should testTheNumber', () => {
    expect(component.testTheNumber('123')).toBeFalse;
    expect(component.testTheNumber('12 34 567 8 901 - 2')).toBeFalse;
    expect(component.testTheNumber('1234567B9012')).toBeFalse;
    expect(component.testTheNumber('12 3A 567 B 901 - 2')).toBeFalse;

    // only 41806650915 with checksum 8 should work
    for (let i = 0; i <= 9; i++) {
      if (i == 6) {
        expect(component.testTheNumber('31814953508' + i.toString())).toBeTrue;
      } else {
        expect(component.testTheNumber('31814953508' + i.toString())).toBeFalse;
      }
    }
  });

  it('should stripAndTrimTheString', () => {
    expect(component.stripAndTrimTheString('123')).toBeFalse;
    expect(component.stripAndTrimTheString('12 34 567 8 901 - 2')).toBeTrue;
    expect(component.stripAndTrimTheString('1__________-_')).toBeFalse();
    expect(component.stripAndTrimTheString('41 80 665 0 915 - 3')).toBeTrue();
    expect(component.stripAndTrimTheString('41 80 665 0 915 - 4')).toBeFalse();
    expect(component.stripAndTrimTheString('41 80 665 0 914 - 6')).toBeTrue();
    expect(component.stripAndTrimTheString('41 80 665 0 914 - 1')).toBeFalse();
  });

  it('should test the sendNumber function, when the waggon number is empty', () => {
    component.sendNumber('');
  });

  it('should test the sendNumber function, when the waggon number contains a valid number', () => {
    component.sendNumber('231231231235');
    expect(component.showIcons).toBeFalsy();
    expect(component.showButtons).toBeTruthy();
    expect(component.waggonNumberIsValid).toBeTruthy();
  });

  it('should test the sendNumber function, when the waggon number contains a non valid number', () => {
    component.sendNumber('12345');
    expect(component.showIcons).toBeFalsy();
    expect(component.showButtons).toBeTruthy();
    expect(component.waggonNumberIsValid).toBeFalsy();
  });

  it('should save waggon successfully', () => {
    const waggon: waggonDTO = {
      waggonNumber: '11',
      waggonPosition: 1,
      comment: 'This is a comment.',
      correctCheckSum: false,
      unitLoads: [],
      creationDate: new Date(),
      modificationDate: new Date(),
      damageComment: 'No damages found.',
    };

    spyOn(component.waggonService, 'save').and.returnValue(of(null));
    const consoleLogSpy = spyOn(console, 'log');
    const consoleErrorSpy = spyOn(console, 'error');

    component.saveWaggon(waggon);

    expect(component.waggonService.save).toHaveBeenCalledWith(
      waggon,
      component.formattedWaggonNumber,
    );
    expect(consoleLogSpy).toHaveBeenCalledWith('Waggon saved successfully');
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
});
