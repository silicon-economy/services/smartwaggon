/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-waggon-with-loading-unit-exception-dialog',
  templateUrl:
    './delete-waggon-with-loading-unit-exception-dialog.component.html',
  styleUrls: [
    './delete-waggon-with-loading-unit-exception-dialog.component.scss',
  ],
})
export class DeleteWaggonWithLoadingUnitExceptionDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<DeleteWaggonWithLoadingUnitExceptionDialogComponent>,
  ) {}

  onYesClick() {
    this.dialogRef.close(true);
  }

  onNoClick() {
    this.dialogRef.close(false);
  }
}
