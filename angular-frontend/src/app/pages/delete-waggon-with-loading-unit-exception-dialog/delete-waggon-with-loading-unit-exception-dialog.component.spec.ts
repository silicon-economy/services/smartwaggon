/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteWaggonWithLoadingUnitExceptionDialogComponent } from './delete-waggon-with-loading-unit-exception-dialog.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TranslateTestingModule } from 'ngx-translate-testing';

describe('DeleteWaggonWithLoadingUnitExceptionDialogComponent', () => {
  let component: DeleteWaggonWithLoadingUnitExceptionDialogComponent;
  let fixture: ComponentFixture<DeleteWaggonWithLoadingUnitExceptionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DeleteWaggonWithLoadingUnitExceptionDialogComponent],
      imports: [TranslateTestingModule.withTranslations('de', {})],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(
      DeleteWaggonWithLoadingUnitExceptionDialogComponent,
    );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close dialog with true value when Yes button is clicked', () => {
    const dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);
    component.dialogRef = dialogRefSpy;
    component.onYesClick();
    expect(dialogRefSpy.close).toHaveBeenCalledWith(true);
  });

  it('should close dialog with false value when No button is clicked', () => {
    const dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);
    component.dialogRef = dialogRefSpy;
    component.onNoClick();
    expect(dialogRefSpy.close).toHaveBeenCalledWith(false);
  });
});
