/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-waggon-capture-function-keys',
  templateUrl: './captured-waggon-keys.component.html',
  styleUrls: ['./captured-waggon-keys.component.scss'],
})
export class CapturedWaggonKeysComponent implements OnInit {
  @Input() waggonNumber: string;
  @Input() waggonPositionNumber: number;

  constructor(
    iconRegistry: MatIconRegistry,
    private domSanitzer: DomSanitizer,
    private snackService: SnackbarService,
    private router: Router,
  ) {
    iconRegistry.addSvgIcon(
      'comment_Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/custom-composed/comment_block.svg',
      ),
    );
    iconRegistry.addSvgIcon(
      'broken_Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/broken_image.svg',
      ),
    );
  }

  ngOnInit(): void {
    return;
  }

  // reportDamage() is called, when clicking on the damage-button
  reportDamage() {
    this.router.navigate(['/damage-capture'], {
      queryParams: {
        number: this.waggonNumber,
        showWaggonNumber: true,
        waggonPositionNumber: this.waggonPositionNumber,
      },
    });
  }

  // comment() is called, when clicking on the comment-button
  comment() {
    this.router.navigate(['/comment'], {
      queryParams: {
        number: this.waggonNumber,
        showWaggonNumber: true,
        waggonPositionNumber: this.waggonPositionNumber,
      },
    });
  }
}
