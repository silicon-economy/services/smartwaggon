/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CapturedWaggonKeysComponent } from './captured-waggon-keys.component';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { CommentPageComponent } from '../comment-page/comment-page.component';

//Tests the waggon capture function keys component
describe('WaggonCaptureFunctionKeysComponent', () => {
  let component: CapturedWaggonKeysComponent;
  let fixture: ComponentFixture<CapturedWaggonKeysComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CapturedWaggonKeysComponent],
      imports: [
        BrowserModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        HttpClientModule,
        MatCardModule,
        RouterTestingModule.withRoutes([
          { path: 'comment', component: CommentPageComponent },
        ]),
        TranslateTestingModule.withTranslations('de', {}),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CapturedWaggonKeysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('commit() should navigate to the comment page with the right query parameters', () => {
    spyOn(router, 'navigate');
    component.waggonNumber = '1234567890';
    component.waggonPositionNumber = 1;
    component.comment();
    expect(router.navigate).toHaveBeenCalledWith(['/comment'], {
      queryParams: {
        number: component.waggonNumber,
        showWaggonNumber: true,
        waggonPositionNumber: 1,
      },
    });
  });

  it('should route to the report broken part page', () => {
    spyOn(router, 'navigate');
    component.waggonNumber = '1234567890';
    component.waggonPositionNumber = 1;
    component.reportDamage();
    expect(router.navigate).toHaveBeenCalledWith(['/damage-capture'], {
      queryParams: {
        number: component.waggonNumber,
        showWaggonNumber: true,
        waggonPositionNumber: 1,
      },
    });
  });
});
