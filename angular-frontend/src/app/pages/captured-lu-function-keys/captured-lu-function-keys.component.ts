/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { SnackbarService } from '../../shared/services/state/snackbar.service';

@Component({
  selector: 'app-captured-lu-function-keys',
  templateUrl: './captured-lu-function-keys.component.html',
  styleUrls: ['./captured-lu-function-keys.component.scss'],
})
export class CapturedLUFunctionKeysComponent implements OnInit {
  @Input() loadingUnitNumber: string;
  @Input() waggonPositionNumber: number;

  constructor(
    iconRegistry: MatIconRegistry,
    private domSanitzer: DomSanitizer,
    private snackService: SnackbarService,
    private router: Router,
  ) {
    iconRegistry.addSvgIcon(
      'comment_Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/custom-composed/comment_block.svg',
      ),
    );
    iconRegistry.addSvgIcon(
      'broken_Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/broken_image.svg',
      ),
    );
    iconRegistry.addSvgIcon(
      'lock_Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/lock.svg',
      ),
    );
    iconRegistry.addSvgIcon(
      'warning_Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/warning.svg',
      ),
    );
  }

  // reportDamage() is called, when clicking on the damage-button
  reportDamage() {
    this.router.navigate(['/damage-capture'], {
      queryParams: {
        number: this.loadingUnitNumber,
        showWaggonNumber: false,
        waggonPositionNumber: this.waggonPositionNumber,
      },
    });
  }

  // comment() is called, when clicking on the comment-button
  comment() {
    this.router.navigate(['/comment'], {
      queryParams: {
        number: this.loadingUnitNumber,
        showWaggonNumber: false,
        waggonPositionNumber: this.waggonPositionNumber,
      },
    });
  }

  // dangerGood() is called , when clicking on the dangerGood-Button
  // it passes the loading unit Number to the  dangerGoodlabel component
  dangerGood() {
    this.router.navigate(['/dangergoodlabel-capture'], {
      queryParams: {
        loadingUnitNumber: this.loadingUnitNumber,
        waggonPositionNumber: this.waggonPositionNumber,
      },
    });
  }

  // captureSeal() is called, when clicking on the captureSeal-button
  captureSeal() {
    this.router.navigate(['/check-sigillum'], {
      queryParams: {
        loadingUnitNumber: this.loadingUnitNumber,
        waggonPositionNumber: this.waggonPositionNumber,
      },
    });
  }

  ngOnInit(): void {
    return;
  }
}
