/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CapturedLUFunctionKeysComponent } from './captured-lu-function-keys.component';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DangergoodlabelCaptureComponent } from '../dangergoodlabel-capture/dangergoodlabel-capture.component';
import { routes } from '../../app-routing.module';
import { CommentPageComponent } from '../comment-page/comment-page.component';
import { Router } from '@angular/router';
import { DamageCapturePageComponent } from '../damage-capture-page/damage-capture-page.component';

describe('CapturedLUFunctionKeysComponent', () => {
  let component: CapturedLUFunctionKeysComponent;
  let fixture: ComponentFixture<CapturedLUFunctionKeysComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CapturedLUFunctionKeysComponent],
      imports: [
        BrowserModule,
        MatIconModule,
        MatFormFieldModule,
        HttpClientModule,
        MatCardModule,
        RouterTestingModule.withRoutes([
          {
            path: 'dangergoodlabel-capture',
            component: DangergoodlabelCaptureComponent,
          },
          { path: 'comment', component: CommentPageComponent },
          { path: 'damage-capture', component: DamageCapturePageComponent },
        ]),
        TranslateTestingModule.withTranslations('de', {}),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CapturedLUFunctionKeysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  //ToDo When implemented: Test for the comment function
  it('should take me to the comment page', () => {
    const expectedLURoute = {
      path: 'comment',
      component: CommentPageComponent,
    };
    expect(routes).toContain(expectedLURoute);
    component.comment();
  });

  it('commit() should navigate to the comment page with the right query parameters', () => {
    spyOn(router, 'navigate');
    component.loadingUnitNumber = 'ABCD1234560';
    component.waggonPositionNumber = 1;
    component.comment();
    expect(router.navigate).toHaveBeenCalledWith(['/comment'], {
      queryParams: {
        number: component.loadingUnitNumber,
        showWaggonNumber: false,
        waggonPositionNumber: 1,
      },
    });
  });

  it('should report broken part', () => {
    const expectedLURoute = {
      path: 'damage-capture',
      component: DamageCapturePageComponent,
    };
    expect(routes).toContain(expectedLURoute);
    component.reportDamage();
  });

  it('should take me to the dangergood page ', () => {
    const expectedLURoute = {
      path: 'dangergoodlabel-capture',
      component: DangergoodlabelCaptureComponent,
    };
    expect(routes).toContain(expectedLURoute);
    component.dangerGood();
  });

  it('should capture Seal part', () => {
    spyOn(router, 'navigate');
    component.loadingUnitNumber = 'ABCD1234560';
    component.waggonPositionNumber = 1;
    component.captureSeal();
    expect(router.navigate).toHaveBeenCalledWith(['/check-sigillum'], {
      queryParams: {
        loadingUnitNumber: component.loadingUnitNumber,
        waggonPositionNumber: 1,
      },
    });
  });
});
