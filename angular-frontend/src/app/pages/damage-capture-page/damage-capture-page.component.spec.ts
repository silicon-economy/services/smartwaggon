/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageCapturePageComponent } from './damage-capture-page.component';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { WaggonOverviewComponent } from '../waggon-overview/waggon-overview.component';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { expect } from '@angular/flex-layout/_private-utils/testing';
import { TrainStorageService } from '../../shared/services/state/train-storage.service';
import { LoadingUnitService } from '../../shared/services/http/loading-unit.service';
import { WaggonService } from '../../shared/services/http/waggon.service';

describe('DangerCapturePageComponent', () => {
  let component: DamageCapturePageComponent;
  let fixture: ComponentFixture<DamageCapturePageComponent>;
  let router: Router;
  let trainStorageServiceSpy: jasmine.SpyObj<TrainStorageService>;
  let loadingUnitServiceSpy: jasmine.SpyObj<LoadingUnitService>;
  let waggonServiceSpy: jasmine.SpyObj<WaggonService>;

  const paramsSubject = new BehaviorSubject({
    number: '23 12 312 3 123 - 5',
    showWaggonNumber: 'false',
    waggonPositionNumber: '2',
  });

  beforeEach(async () => {
    loadingUnitServiceSpy = jasmine.createSpyObj('LoadingUnitService', [
      'addDamageComment',
    ]);
    loadingUnitServiceSpy.addDamageComment.and.returnValue(of(null));
    waggonServiceSpy = jasmine.createSpyObj('WaggonService', [
      'addDamageComment',
    ]);
    waggonServiceSpy.addDamageComment.and.returnValue(of(null));
    trainStorageServiceSpy = jasmine.createSpyObj('TrainStorageService', [
      'getDamageCommentByWaggonName',
      'getDamageCommentByUnitLoadNumber',
      'addDamageCommentToUnitLoad',
      'addDamageCommentToWaggon',
    ]);
    await TestBed.configureTestingModule({
      declarations: [DamageCapturePageComponent],
      imports: [
        BrowserModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        HttpClientModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes([
          { path: 'waggon-overview', component: WaggonOverviewComponent },
        ]),
        TranslateTestingModule.withTranslations('de', {}),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            queryParams: of(paramsSubject),
          },
        },
        { provide: TrainStorageService, useValue: trainStorageServiceSpy },
        { provide: LoadingUnitService, useValue: loadingUnitServiceSpy },
        { provide: WaggonService, useValue: waggonServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DamageCapturePageComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should discard', function () {
    spyOn(router, 'navigate');
    spyOn(component, 'setNewDamageCommentToTrainStorage');
    component.waggonPositionNumber = 1;
    component.commentInput = '123';
    component.discard();
    expect(component.setNewDamageCommentToTrainStorage).toHaveBeenCalledWith(
      '',
    );
    expect(router.navigate).toHaveBeenCalledWith(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: 1 },
    });
  });

  it('should commit', function () {
    spyOn(router, 'navigate');
    spyOn(component, 'setNewDamageCommentToTrainStorage');
    component.waggonPositionNumber = 1;
    component.commentInput = '123';
    component.commit();
    expect(component.setNewDamageCommentToTrainStorage).toHaveBeenCalledWith(
      '123',
    );
    expect(router.navigate).toHaveBeenCalledWith(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: 1 },
    });
  });

  describe('setNewDamageCommentToTrainStorage', () => {
    it('should add the damage comment to the waggon if showWaggonNumber is true', () => {
      trainStorageServiceSpy.addDamageCommentToWaggon.and.callThrough();
      component.number = '23 12 312 3 123 - 5';
      component.showWaggonNumber = true;
      component.setNewDamageCommentToTrainStorage('Test comment');
      expect(
        trainStorageServiceSpy.addDamageCommentToWaggon,
      ).toHaveBeenCalledWith(component.number, 'Test comment');
    });

    it('should add the damage comment to the unit load if showWaggonNumber is false', () => {
      trainStorageServiceSpy.addDamageCommentToUnitLoad.and.callThrough();
      component.number = 'ABCD1234560';
      component.showWaggonNumber = false;
      component.setNewDamageCommentToTrainStorage('Test comment');
      expect(
        trainStorageServiceSpy.addDamageCommentToUnitLoad,
      ).toHaveBeenCalledWith(component.number, 'Test comment');
    });
  });

  it('should toggle the comment box', function () {
    component.showComment = false;
    component.toggleCommentBox();
    expect(component.showComment).toBeTruthy();
    component.toggleCommentBox();
    expect(component.showComment).toBeFalsy();
  });

  it('should allow the rendering of the camera', function () {
    component.showCamera = false;
    component.selectCamera();
    expect(component.showCamera).toBeTruthy();
  });

  it('should call addDamageComment for loading units', () => {
    const loadingUnitNumber = '123';
    const comment = 'test comment';

    component.number = loadingUnitNumber;
    component.showWaggonNumber = false;

    component.setNewDamageCommentToTrainStorage(comment);

    expect(loadingUnitServiceSpy.addDamageComment).toHaveBeenCalledWith(
      loadingUnitNumber,
      comment,
    );
  });

  it('should call addDamageComment for Waggon', () => {
    const waggonNumber = '456';
    const comment = 'test comment';

    component.saveWaggonDamageComment(waggonNumber, comment);

    expect(waggonServiceSpy.addDamageComment).toHaveBeenCalledWith(
      waggonNumber,
      comment,
    );
  });
});
