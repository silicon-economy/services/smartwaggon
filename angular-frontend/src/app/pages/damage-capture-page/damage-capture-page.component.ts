/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TrainStorageService } from '../../shared/services/state/train-storage.service';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { LoadingUnitService } from '../../shared/services/http/loading-unit.service';
import { WaggonService } from '../../shared/services/http/waggon.service';

@Component({
  selector: 'app-damage-capture-page',
  templateUrl: './damage-capture-page.component.html',
  styleUrls: ['./damage-capture-page.component.scss'],
})
export class DamageCapturePageComponent {
  @Input() number: string;
  @Input() showWaggonNumber: boolean; // True if the waggon-icon should be shown. False if the loading-unit-icon should be shown
  @Input() waggonPositionNumber: number;

  commentInput: string;
  showComment = false;
  showCamera = false;

  constructor(
    private loadingUnitService: LoadingUnitService,
    private waggonService: WaggonService,
    private iconRegistry: MatIconRegistry,
    private domSanitzer: DomSanitizer,
    private snackService: SnackbarService,
    private router: Router,
    private route: ActivatedRoute,
    private trainStorage: TrainStorageService,
  ) {
    this.iconRegistry.addSvgIcon(
      'loading-Unit-Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/loading-Unit-Icon.svg',
      ),
    );
    this.iconRegistry.addSvgIcon(
      'waggon-Icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/waggon_icon.svg',
      ),
    );
    this.iconRegistry.addSvgIcon(
      'camera-icon',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/photo_camera.svg',
      ),
    );
    this.iconRegistry.addSvgIcon(
      'comment-block',
      this.domSanitzer.bypassSecurityTrustResourceUrl(
        'assets/icons/third-party-material-icons/custom-composed/comment_block.svg',
      ),
    );

    this.route.queryParams.subscribe((params) => {
      this.number = params['number'];
      this.showWaggonNumber = coerceBooleanProperty(params['showWaggonNumber']);
      this.waggonPositionNumber = Number(params['waggonPositionNumber']);
    });
    if (this.showWaggonNumber) {
      this.commentInput = this.trainStorage.getDamageCommentByWaggonName(
        this.number,
      );
    } else {
      this.commentInput = this.trainStorage.getDamageCommentByUnitLoadNumber(
        this.number,
      );
    }
  }

  commit() {
    this.setNewDamageCommentToTrainStorage(this.commentInput);
    this.router.navigate(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: this.waggonPositionNumber },
    });
  }

  discard() {
    this.setNewDamageCommentToTrainStorage('');
    this.router.navigate(['/waggon-overview'], {
      queryParams: { waggonPositionNumber: this.waggonPositionNumber },
    });
  }

  // Sets the text of the InputField depending on whether it is a loading unit comment or a waggon comment.
  setNewDamageCommentToTrainStorage(text: string) {
    if (this.showWaggonNumber) {
      this.trainStorage.addDamageCommentToWaggon(this.number, text);
      this.saveWaggonDamageComment(this.number, text);
    } else {
      this.trainStorage.addDamageCommentToUnitLoad(this.number, text);
      this.saveLoadingUnitDamageComment(this.number, text);
    }
  }

  toggleCommentBox() {
    this.showComment = !this.showComment;
  }

  selectCamera() {
    this.showCamera = !this.showCamera;
  }
  saveLoadingUnitDamageComment(loadingUnitNumber: string, comment: string) {
    this.loadingUnitService
      .addDamageComment(loadingUnitNumber, comment)
      .subscribe({
        next: () => {
          console.log('Waggon comment saved successfully');
        },
        error: (error) => {
          console.error('Error saving waggon comment:', error);
        },
      });
  }
  saveWaggonDamageComment(waggonNumber: string, comment: string) {
    this.waggonService.addDamageComment(waggonNumber, comment).subscribe({
      next: () => {
        console.log('Waggon comment saved successfully');
      },
      error: (error) => {
        console.error('Error saving waggon comment:', error);
      },
    });
  }
}
