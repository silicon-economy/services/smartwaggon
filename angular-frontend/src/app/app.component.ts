/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatDrawerMode } from '@angular/material/sidenav';
import { MatDialog } from '@angular/material/dialog';
import { FooterService } from './shared/services/state/footer.service';
import { ThemeService } from './shared/services/state/theme.service';
import { SidenavService } from './shared/services/state/sidenav.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { CookieDialogComponent } from './shared/dialogs/cookie-dialog/cookie-dialog.component';
import { BlockchainModeService } from './shared/services/state/blockchain-mode.service';
import { CookieConsentService } from './shared/services/state/cookie-consent.service';
import { ModeIcon } from './shared/models/mode';
import { ScrollRestorationService } from './shared/services/state/scroll-restoration.service';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from './shared/services/state/snackbar.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('indicatorRotation', [
      state('collapsed, void', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition(
        'expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)'),
      ),
    ]),
    /** Animation that expands and collapses the panel content. */
    trigger('listHeight', [
      state('collapsed, void', style({ height: '0px', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition(
        'expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)'),
      ),
    ]),
  ],
})
export class AppComponent {
  title = 'Smart Waggon Inspector';
  breakpointStateLtMd = false;
  modeIcon: ModeIcon;
  footerState: boolean;
  sidenavMode: MatDrawerMode = 'side';
  sidenavState;
  desktopSidenavState: boolean;
  exAppState = false;
  currentClassName: string;
  actualLanguage = 'DE';

  constructor(
    public breakpointObserver: BreakpointObserver,
    public dialog: MatDialog,
    public cookieService: CookieConsentService,
    public themeService: ThemeService,
    public footerService: FooterService,
    public sidenavService: SidenavService,
    public blockchainService: BlockchainModeService,
    private scrollStateService: ScrollRestorationService,
    public snack: SnackbarService,
    public snackBar: MatSnackBar,
    private translate: TranslateService,
  ) {
    // Sidenav State
    sidenavService.getSidenavState$().subscribe((side) => {
      this.desktopSidenavState = side;
    });

    // Breakpoints
    breakpointObserver
      .observe([Breakpoints.Small, Breakpoints.XSmall])
      .subscribe((result) => {
        if (result.matches) {
          this.breakpointStateLtMd = true;
          this.activateHandsetLayout();
        } else {
          this.breakpointStateLtMd = false;
          this.deactivateHandsetLayout();
        }
      });
    // Cookies
    cookieService.getCookieConsent().subscribe((consent) => {
      if (consent === false) {
        const cookieDialogRef = this.dialog.open(CookieDialogComponent, {
          disableClose: true,
        });
        cookieDialogRef.afterClosed().subscribe(() => {
          cookieService.setCookieConsent(true);
        });
      }
    });

    // Themes and Modes
    themeService.getModeName$().subscribe(() => {
      this.modeIcon = themeService.getNextModeIcon();
      this.applyClass(themeService.getClassName());
    });
    themeService.getThemeName$().subscribe(() => {
      this.applyClass(themeService.getClassName());
    });

    // Snackbar for Error Handling
    this.snack
      .getStatusMessage()
      .subscribe((r) => this.openSnackBar(r, this.snack.getNextDuration()));
  }

  private openSnackBar(message: string, duration: number): void {
    if (duration > 0) {
      this.snackBar.open(message, this.translate.instant('close'), {
        duration: duration,
      });
    } else {
      this.snackBar.open(message, this.translate.instant('close'));
    }
  }

  changeLanguage() {
    if (this.actualLanguage === 'EN') {
      this.translate.use('de');
      this.actualLanguage = 'DE';
    } else if (this.actualLanguage === 'DE') {
      this.translate.use('en');
      this.actualLanguage = 'EN';
    }
  }
  private activateHandsetLayout() {
    this.sidenavMode = 'over';
    this.sidenavState = false;
  }

  private deactivateHandsetLayout() {
    this.sidenavMode = 'side';
    this.sidenavState = this.desktopSidenavState;
  }

  sidenavStateToggle() {
    if (!this.breakpointStateLtMd) {
      this.sidenavService.toggleSidenavState();
    }
  }

  applyClass(className: string): void {
    if (this.currentClassName !== className) {
      document.body.classList.add(className);
      document.body.classList.remove(this.currentClassName);
      this.currentClassName = className;
    }
  }
}
