/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ErrorPageComponent } from './pages/error-page/error-page.component';
import { FocusBlurDirective } from './directives/focus-blur.directive';
import { ImprintLegalPageComponent } from './pages/imprint-legal-page/imprint-legal-page.component';
import { PrivacyPageComponent } from './pages/privacy-page/privacy-page.component';
import { SettingsPageComponent } from './pages/settings-page/settings-page.component';
import { SharedModule } from './shared/shared.module';
import { FlexModule } from '@angular/flex-layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';

import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { MatChipsModule } from '@angular/material/chips';

import { MatCheckboxModule } from '@angular/material/checkbox';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ScrollRestorationService } from './shared/services/state/scroll-restoration.service';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatStepperModule } from '@angular/material/stepper';

import { MatSliderModule } from '@angular/material/slider';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatDividerModule } from '@angular/material/divider';
import { WaggonOverviewComponent } from './pages/waggon-overview/waggon-overview.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { WaggonCaptureComponent } from './pages/waggon-capture/waggon-capture.component';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { TextMaskModule } from 'angular2-text-mask';
import { SnackbarService } from './shared/services/state/snackbar.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LoadingUnitComponent } from './pages/loadingUnit-capture/loading-unit.component';
import { CapturedWaggonKeysComponent } from './pages/captured-waggon-keys/captured-waggon-keys.component';

import { CapturedLUFunctionKeysComponent } from './pages/captured-lu-function-keys/captured-lu-function-keys.component';
import { DangergoodlabelCaptureComponent } from './pages/dangergoodlabel-capture/dangergoodlabel-capture.component';
import { WebcamModule } from 'ngx-webcam';
import { CameraComponent } from './pages/camera/camera.component';
import { CommentPageComponent } from './pages/comment-page/comment-page.component';
import { DamageCapturePageComponent } from './pages/damage-capture-page/damage-capture-page.component';
import { CheckSigillumComponent } from './pages/check-sigillum/check-sigillum.component';
import { ExceptionDialogComponent } from './pages/exception-dialog/exception-dialog.component';
import { DeleteWaggonWithLoadingUnitExceptionDialogComponent } from './pages/delete-waggon-with-loading-unit-exception-dialog/delete-waggon-with-loading-unit-exception-dialog.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

export const options: Partial<null | IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    AppComponent,
    ErrorPageComponent,
    FocusBlurDirective,
    ImprintLegalPageComponent,
    PrivacyPageComponent,
    SettingsPageComponent,
    LandingPageComponent,
    WaggonOverviewComponent,
    WaggonCaptureComponent,
    LoadingUnitComponent,
    CapturedWaggonKeysComponent,
    CapturedLUFunctionKeysComponent,
    DangergoodlabelCaptureComponent,
    CameraComponent,
    CommentPageComponent,
    DamageCapturePageComponent,
    CheckSigillumComponent,
    DamageCapturePageComponent,
    ExceptionDialogComponent,
    DeleteWaggonWithLoadingUnitExceptionDialogComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,

    // Custom Modules:
    SharedModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FlexModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    CommonModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatSelectModule,
    FormsModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDialogModule,
    MatChipsModule,
    MatSliderModule,
    MatProgressBarModule,
    MatRadioModule,
    MatDividerModule,
    MatCheckboxModule,
    HttpClientModule,
    WebcamModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      defaultLanguage: 'de',
    }),
    MatTableModule,
    MatPaginatorModule,
    MatStepperModule,
    MatSnackBarModule,
    NgxMaskModule.forRoot(),
    TextMaskModule,
  ],

  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    ScrollRestorationService,
    { provide: 'scrollContainerSelector', useValue: '#sidenav-content-area' },
    SnackbarService,
  ],
})
export class AppModule {}
