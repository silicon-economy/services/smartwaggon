/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[blurInput]',
})
export class FocusBlurDirective {
  @Input('blurInput') multiply: string;

  constructor(private element: ElementRef) {}

  @HostListener('click')
  onClick() {
    this.element.nativeElement.blur();
  }
}
