/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Loads environment variables.
 * Needs to be done in assets folder since environment.ts is barely accessible after build.
 * Attention: New environment variables also need to be added to assets/env.template.js
 * Detailed Information: https://pumpingco.de/blog/environment-variables-angular-docker/
 */

(function (window) {
  window["env"] = window["env"] || {};
  window["env"]["production"] = false;
  window["env"]["SMWI_BACKEND_URL"] = "http://localhost:8080";
})(this);
