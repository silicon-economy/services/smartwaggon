/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Template to replace environment variables during runtime.
 * Needs to be done in assets folder since environment.ts is barely accessible after build.
 * Attention: New environment variables also need to be added to assets/env.js.
 * Detailed Information: https://pumpingco.de/blog/environment-variables-angular-docker/
 */

(function (window) {
  window["env"] = window["env"] || {};
  window["env"]["production"] = "${production}";
  window["env"]["SMWI_BACKEND_URL"] = "${SMWI_BACKEND_URL}";
})(this);
