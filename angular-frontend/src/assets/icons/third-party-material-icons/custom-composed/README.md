## Third Party Icons which are used as composed images

Throughout the application, [material icons](https://fonts.google.com/icons) are used and combined into new images:

> ### comment_block.svg
>
> * [Note Icon](https://fonts.google.com/icons?selected=Material%20Symbols%20Outlined%3Anotes%3AFILL%400%3Bwght%40400%3BGRAD%400%3Bopsz%4024)
> 
> * [Edit Icon](https://fonts.google.com/icons?selected=Material%20Symbols%20Outlined%3Aedit%3AFILL%401%3Bwght%40400%3BGRAD%400%3Bopsz%4024)

> ### lockadd.svg
>
> * [Lock Icon](https://fonts.google.com/icons?selected=Material%20Symbols%20Outlined%3Alock%3AFILL%401%3Bwght%40400%3BGRAD%400%3Bopsz%4024)
>
> * [Add Icon](https://fonts.google.com/icons?selected=Material%20Symbols%20Outlined%3Aadd%3AFILL%400%3Bwght%40400%3BGRAD%400%3Bopsz%4024)
