## Third Party Icons which are used as Images

Throughout the Application the Material Icons is used as the default font and is included in the png & svg-format.

> https://fonts.google.com/icons?selected=Material%20Icons%3Ahome%3A
