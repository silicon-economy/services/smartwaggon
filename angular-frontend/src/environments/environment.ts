/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production:
    window['env'] && window['env']['production']
      ? window['env']['production']
      : false,
  // SMWI_BACKEND_URL has to include the protocol since we can not rely
  // on servers redirecting us to https when hardcoding http
  smwiBackendUrl:
    window['env'] && window['env']['SMWI_BACKEND_URL']
      ? window['env']['SMWI_BACKEND_URL']
      : 'http://localhost:8080',

  // The following environment variables are independent of deployment
  api: '/v1',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/plugins/zone-error'; // Included with Angular CLI.
