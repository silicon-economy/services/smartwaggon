/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  // SMWI_BACKEND_URL has to include the protocol since we can not rely
  // on servers redirecting us to https when hardcoding http
  smwiBackendUrl:
    window['env'] && window['env']['SMWI_BACKEND_URL']
      ? window['env']['SMWI_BACKEND_URL']
      : 'http://localhost:8080',

  // The following environment variables are independent of deployment
  api: '/v1',
  endPoints: {
    hello: '/hello',
  },
};
