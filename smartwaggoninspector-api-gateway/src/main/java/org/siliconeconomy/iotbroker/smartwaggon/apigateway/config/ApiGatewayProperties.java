/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.smartwaggon.apigateway.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Defines API gateway-specific (configuration) properties (that are bound via environment
 * variables).
 *
 * @author M. Grzenia
 */
@ConfigurationProperties(prefix = "api-gateway")
@ConstructorBinding
@Getter
public class ApiGatewayProperties {

    private final String javaBackendApiUrl;
    private final String pythonBackendApiUrl;
    private final List<String> corsAllowedOriginPatterns;

    /**
     * @param javaBackendApiUrl         The URL to the Smart Waggon Inspector's Java backend.
     * @param pythonBackendApiUrl       The URL to the Smart Waggon Inspector's Python backend.
     * @param corsAllowedOriginPatterns The patterns of origins for which cross-origin requests to
     *                                  the API gateway are allowed from a browser.
     */
    public ApiGatewayProperties(String javaBackendApiUrl,
                                String pythonBackendApiUrl,
                                List<String> corsAllowedOriginPatterns) {
        this.javaBackendApiUrl = requireNonNull(javaBackendApiUrl, "javaBackendApiUrl");
        this.pythonBackendApiUrl = requireNonNull(pythonBackendApiUrl, "pythonBackendApiUrl");
        this.corsAllowedOriginPatterns
            = requireNonNull(corsAllowedOriginPatterns, "corsAllowedOriginPatterns");
    }
}
